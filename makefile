###############################################################################################
#
#
#	Simple Makefile for BLITz
#
#
###############################################################################################

#---------------------------------------------------------------------------
#	build configuration
#---------------------------------------------------------------------------

export CC=clang++

PRODUCTNAME:=blitz
PRODUCTDIR:=bin
BUILDDIR:=$(PRODUCTNAME).build
LIB_PRODUCT:=libblitz.a
DYLIB_PRODUCT:=libblitz.dylib
SERVER_PRODUCT:=blitzd

DOCDIR:=doc

INCLUDE:=-Isrc -Isrc/time
#LIBS := put libraries and frameworks here

ifeq ($(BUILD_TYPE), debug)
	FLAGS=-DDEBUG
	RFLAGS=-g -O0 -msse4
	DIST:=$(DIST)_Debug
else
	RFLAGS=-O2 -msse4
	STRIP=strip $(BUILDDIR)/top
endif

FLAGS = -std=c++98
#FLAGS += -std=c++11

#---------------------------------------------------------------------------
#	Discover source files and infer obj and dep files
#---------------------------------------------------------------------------

LIB_SOURCES := $(addprefix src/, blitz.cpp posix_thread.cpp posix_socket.cpp osc.cpp)  $(wildcard src/time/*.cpp)
LIB_OBJS:=$(addprefix $(BUILDDIR)/,$(patsubst %.cpp,%.o,$(patsubst %.mm,%.o,$(notdir $(LIB_SOURCES)))))
LIB_DEPS:=$(LIB_OBJS:%.o=%.d)

SERVER_SOURCES := $(addprefix src/, directory.cpp directory_server.cpp posix_socket.cpp posix_thread.cpp osc.cpp) $(wildcard src/time/*.cpp)
SERVER_OBJS:=$(addprefix $(BUILDDIR)/,$(patsubst %.cpp,%.o,$(patsubst %.mm,%.o,$(notdir $(SERVER_SOURCES)))))
SERVER_DEPS:=$(SERVER_OBJS:%.o=%.d)

EXAMPLES:=$(shell find examples -type d -and -depth 1) $(shell find max -type d -and -depth 1)

TEXFILES:=$(shell find $(DOCDIR)/latex -name '*.tex')
PDFFILES:=$(addprefix $(DOCDIR)/,$(patsubst %.tex,%.pdf,$(notdir $(TEXFILES))))
#---------------------------------------------------------------------------
#	Main rules
#---------------------------------------------------------------------------

all : $(PRODUCTDIR)/$(LIB_PRODUCT)  $(PRODUCTDIR)/$(DYLIB_PRODUCT) $(PRODUCTDIR)/$(SERVER_PRODUCT)

debug:
	BUILD_TYPE=debug make all

test:
	@ $(MAKE) -C tests

examples: debug
	for subdir in $(EXAMPLES); do \
		echo making $$subdir ; \
		$(MAKE) -C $$subdir debug ; \
	done

.PHONY: doc

doc:
	$(MAKE) doclatex ; \
	cd $(DOCDIR)/doxygen ; \
	doxygen doxygen.conf ;

doclatex: $(PDFFILES)

clean :
	if [ -e $(BUILDDIR) ] ; then rm -r $(BUILDDIR) ; fi
	if [ -e $(PRODUCTDIR) ] ; then rm -r $(PRODUCTDIR) ; fi
	for subdir in $(EXAMPLES); do \
		echo making $$subdir ; \
		$(MAKE) -C $$subdir clean ; \
	done

$(PRODUCTDIR)/$(LIB_PRODUCT) : $(LIB_OBJS) | $(PRODUCTDIR)
	libtool -static -o $(PRODUCTDIR)/$(LIB_PRODUCT) $(LIB_OBJS) $(LIBS)

$(PRODUCTDIR)/$(DYLIB_PRODUCT) : $(LIB_OBJS) | $(PRODUCTDIR)
	$(CC) -shared -o $(PRODUCTDIR)/$(DYLIB_PRODUCT) $(LIB_OBJS) $(LIBS)

$(PRODUCTDIR)/$(SERVER_PRODUCT) : $(SERVER_OBJS) | $(PRODUCTDIR)
	$(CC) -o $(PRODUCTDIR)/$(SERVER_PRODUCT) $(SERVER_OBJS) $(LIBS)

#---------------------------------------------------------------------------
#	Rules to make build and product directories
#---------------------------------------------------------------------------

$(BUILDDIR) :
	mkdir $(BUILDDIR)

$(PRODUCTDIR) :
	mkdir $(PRODUCTDIR)

#---------------------------------------------------------------------------
#	Pattern Rules to make obj files
#---------------------------------------------------------------------------

vpath %.cpp $(dir $(wildcard src/*/))
vpath %.mm $(dir $(wildcard src/*/))

$(BUILDDIR)/%.o : %.cpp | $(BUILDDIR)
	clang++ $(RFLAGS) $(FLAGS) -o $@ $(INCLUDE) -c $<

$(BUILDDIR)/%.o : %.mm | $(BUILDDIR)
	clang++ $(RFLAGS) $(FLAGS) -o $@ $(INCLUDE) -c $<

#---------------------------------------------------------------------------
#	Pattern Rules to make pdf files
#---------------------------------------------------------------------------

$(DOCDIR)/%.pdf : $(DOCDIR)/latex/%.tex
	pdflatex -output-directory=$(DOCDIR)/latex $< ; \
	mv $(DOCDIR)/latex/$(@F) $(DOCDIR)

#---------------------------------------------------------------------------
#	Automatic dependency requisites
#---------------------------------------------------------------------------

df = $(BUILDDIR)/$(*F)

MAKEDEPEND = clang -M $(CPPFLAGS) $(INCLUDE) $(SDKDIRS) $(FLAGS) -o $(df).Td $<

include $(LIB_DEPS)
include $(SERVER_DEPS)

$(BUILDDIR)/%.d : %.cpp | $(BUILDDIR)
	$(MAKEDEPEND)
	sed 's,\($*\)\.o[ :]*,$(BUILDDIR)/\1.o $@ : ,g' < $(df).Td > $(df).d;
	rm -f $(df).Td

$(BUILDDIR)/%.d : %.mm | $(BUILDDIR)
	$(MAKEDEPEND)
	sed 's,\($*\)\.o[ :]*,$(BUILDDIR)/\1.o $@ : ,g' < $(df).Td > $(df).d;
	rm -f $(df).Td
