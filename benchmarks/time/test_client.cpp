/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: test_client.cpp
*	@author: Martin Fouilleul
*	@date: 28/06/2019
*	@revision:
*
*****************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<getopt.h>

#include"time_client.h"

//--------------------------------------------------------------------------------
// main
//--------------------------------------------------------------------------------

#define DEFAULT_ADDRESS "127.0.0.1"
#define DEFAULT_PORT 5559		//TODO(martin): pass it as a command line argument...


volatile bool _globalRun = true;

void SigIntHandler(int dummy)
{
	_globalRun = false;
}

int main(int argc, char** argv)
{
	//NOTE(martin): parse command line arguments
	//TODO(martin): well, at least check something...

	#ifndef TIME_SERVER_SIMULATOR
		const char* serverAddress =  DEFAULT_ADDRESS ;
		unsigned short serverPort = DEFAULT_PORT ;

		time_client_options options;
		options.enableDiscipline = true;
		options.enableHuffPuff = true;

		struct option longopts[] = {
			{"enable-discipline", required_argument, 0, 'd'},
			{"enable-huffpuff", required_argument, 0, 'h'},
			{"server", required_argument, 0, 's'},
			{"port", required_argument, 0, 'p'}
		};
		int ch;
		while((ch = getopt_long(argc, argv, "d:h:s:p:", longopts, 0)) != -1)
		{
			switch(ch)
			{
				case 'd':
					options.enableDiscipline = (atoi(optarg) != 0);
 					break;
				case 'h':
					options.enableHuffPuff = (atoi(optarg) != 0);
 					break;
				case 's':
					serverAddress = optarg;
					break;
				case 'p':
					serverPort = atoi(optarg);
					break;
				default:
					//TODO(martin): usage();
					printf("error in arguments\n");
					break;
			}
		}

		time_client timeClient;
		TimeClientStart(&timeClient, StringToHostIP(serverAddress), serverPort, &options);

	#else

		time_client timeClient;
		time_simulator_options options;
		options.offset = 0;
		options.randomSeed = 5678;
		options.upScale = 0.0002;
		options.upShape = 1.3;
		options.downScale = 0.0002;
		options.downShape = 1.3;
		options.enableDiscipline = true;
		options.enableHuffPuff = true;

		struct option longopts[] = {
			{"enable-discipline", required_argument, 0, 'd'},
			{"enable-huffpuff", required_argument, 0, 'h'},
			{"up-scale", required_argument, 0, 'M'},
			{"up-shape", required_argument, 0, 'S'},
			{"down-scale", required_argument, 0, 'm'},
			{"down-shape", required_argument, 0, 's'},
			{"offset", required_argument, 0, 'o'}
		};
		int ch;
		while((ch = getopt_long(argc, argv, "d:h:m:s:M:S:o:", longopts, 0)) != -1)
		{
			switch(ch)
			{
				case 'd':
					options.enableDiscipline = (atoi(optarg) != 0);
 					break;
				case 'h':
					options.enableHuffPuff = (atoi(optarg) != 0);
 					break;
				case 'M':
					options.upScale = atof(optarg);
 					break;
				case 'S':
					options.upShape = atof(optarg);
 					break;
				case 'm':
					options.downScale = atof(optarg);
 					break;
				case 's':
					options.downShape = atof(optarg);
 					break;
				case 'o':
					options.offset = SecondsToTimestamp(atof(optarg));
 					break;
				default:
					//TODO(martin): usage();
					printf("error in arguments\n");
					break;
			}
		}

		TimeClientStartSimulation(&timeClient, &options);
	#endif // undef TIME_SERVER_SIMULATOR


	signal(SIGINT, SigIntHandler);
	while(_globalRun)
	{
		//NOTE(martin): run until SIGINT
		sleep(1);
	}

	#ifdef TIME_SERVER_SIMULATOR
		TimeClientStopSimulation(&timeClient);
	#else
		TimeClientStop(&timeClient);
	#endif

	return(0);
}
