#!/usr/local/bin/python3

import sys
import re
import numpy as np
import csv
from matplotlib import pyplot as plt

# parse arguments

if len(sys.argv) < 2 :
	print("usage: plot_time_sim simfile")
	exit(0)

infile = sys.argv[1]

start = 1
if len(sys.argv) > 2 :
	start = int(sys.argv[2])

plotfile = re.sub('\.csv$', '_' + str(start) + '.eps', infile)
scatterfile = re.sub('\.csv$', '_' + str(start) + '_scatter.eps', infile)

# read the input file

csvfile = open(infile)
reader = csv.reader(csvfile, delimiter = ';')
contents = np.transpose(np.asarray(list(reader), dtype=np.float))

# extract the columns

clockError = contents[0]
sampleDelay = contents[1]
sampleOffset = contents[2]
#offsetAdjust = contents[3]
#frequencyAdjust = contents[4]
#totalAdjust = contents[5]
#disciplineState = contents[6]
#pollInterval = contents[7]

seconds = np.arange(0, len(clockError))


# compute statistics

tail = 500
rms = np.sqrt(np.mean(np.square(clockError[tail:])))
mean = np.mean(clockError[tail:])
rms_around_mean = np.sqrt(np.mean(np.square(clockError[tail:] - mean)))

print('rms = ' + np.format_float_scientific(rms, precision=3) + 's')
print('mean = ' + np.format_float_scientific(mean, precision=3) + 's')
print('rms around mean = ' + np.format_float_scientific(rms_around_mean, precision=3) + 's')

# plot the clock error and sample offset

fig, ax1 = plt.subplots()

color='tab:red'
ax1.set_xlabel('time (s)')
ax1.set_ylabel('clock error (s)', color=color)
ax1.tick_params(axis='y', labelcolor=color)
ax1.ticklabel_format(axis='y', style='sci', scilimits=(0,0))

ax1.set_ylim([-0.1, 0.03])

plt.plot(seconds[start:], clockError[start:], color=color)
plt.text(1000, 0.02, 'Mean error after convergence = '+ np.format_float_scientific(mean, precision=2) + ' s', color=color)
plt.text(1000, 0.014, 'RMS error after convergence = '+ np.format_float_scientific(rms, precision=2) + ' s', color=color)
plt.text(1000, 0.008, '(Bias-corrected RMS = '+ np.format_float_scientific(rms_around_mean, precision=2) + ' s)', color=color)


color = 'tab:blue'

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.plot(seconds[start:], sampleDelay[start:], color=color)

ax2.set_ylim([0, 0.3])
ax2.set_ylabel('sample round-trip time', color=color)  # we already handled the x-label with ax1
ax2.tick_params(axis='y', labelcolor=color)
ax2.ticklabel_format(axis='y', style='sci', scilimits=(0,0))

fig.tight_layout()  # otherwise the right y-label is slightly clipped


plt.savefig(plotfile, format='eps')

# plot the wedge scattergram

plt.figure()
plt.scatter(sampleDelay[start:], sampleOffset[start:], s=5)
axes = plt.gca()
axes.set_xlim([0, 0.02])
axes.set_ylim([-0.01, 0.01])
axes.set_xlabel('round-trip time (s)')
axes.set_ylabel('offset (s)')
axes.ticklabel_format(axis='both', style='sci', scilimits=(0,0))

plt.savefig(scatterfile, format='eps')


plt.show()
