/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: test_server.cpp
*	@author: Martin Fouilleul
*	@date: 28/06/2019
*	@revision:
*
*****************************************************************/

#include<unistd.h>

#include"time_server.h"

#define PORT 5559	//TODO(martin): pass it as a command line argument...

volatile bool _globalRun = true;

void SigIntHandler(int dummy)
{
	_globalRun = false;
}

int main(int argc, char** argv)
{
	time_server server;
	TimeServerStart(&server, PORT);

	signal(SIGINT, SigIntHandler);
	while(_globalRun)
	{
		sleep(1);
	}

	TimeServerStop(&server);
	return(0);
}
