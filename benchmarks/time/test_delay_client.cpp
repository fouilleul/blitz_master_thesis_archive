/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: client.cpp
*	@author: Martin Fouilleul
*	@date: 07/03/2019
*	@revision:
*
*****************************************************************/

/*
	Synchronizes on time server
*/

#include<sys/socket.h>
#include<netinet/ip.h>
#include<arpa/inet.h>	// inet_addr()
#include<unistd.h>	// close()
#include<errno.h>
#include<string.h>	// strerror()

#include<pthread.h>	// pthread, maybe use std::thread instead
#include<signal.h>	// signal(), to handle Ctrl-C

//DEBUG
#include<stdio.h>	// printf()
#include<assert.h>	// assert()

#include"packet.h"
#include"platform_time.h"
#include"user_clock.h"
#include"clock_filter.h"

//------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------

#define DEFAULT_ADDRESS "127.0.0.1"
#define DEFAULT_PORT 5559		//TODO(martin): pass it as a command line argument...

//--------------------------------------------------------------------------------
// main
//--------------------------------------------------------------------------------

static bool _mainLoopRun = true;

void SigIntHandler(int dummy)
{
	_mainLoopRun = false;
}

int main(int argc, char** argv)
{
	//NOTE(martin): parse command line arguments
	//TODO(martin): well, at least check something...

	const char* serverAddress = argc > 1 ? argv[1] : DEFAULT_ADDRESS ;
	const unsigned short serverPort = argc > 2 ? atoi(argv[2]) : DEFAULT_PORT ;

	//NOTE(martin): create and bind a udp socket for reception

	int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	assert(sock);

	sockaddr_in to = {0};
	to.sin_addr.s_addr = inet_addr(serverAddress);
	to.sin_port = htons(serverPort);
	to.sin_family = AF_INET;

	struct sigaction sa = {};
	sa.sa_handler = SigIntHandler;
	sa.sa_flags =  0;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGINT, &sa, 0);

	while(_mainLoopRun)
	{
		sync_packet packet = {0};
		packet.type = SYNC_REQUEST;
		packet.t1 = GetSystemTimestamp();

		ssize_t size = 0;
		if((size = sendto(sock, &packet, sizeof(packet), 0, (sockaddr*)&(to), sizeof(to))) == -1)
		{
			printf("Error %i : %s\n", errno, strerror(errno));
			exit(0);
		}

		sockaddr_in from = {0};
		socklen_t fromSize = sizeof(from);
		size = recvfrom(sock, &packet, sizeof(sync_packet), 0, (sockaddr*)&from, &fromSize);
		if(size <= 0 )
		{
			break;
		}

		u64 t4 = GetSystemTimestamp();

		//NOTE(martin): Compute the packet offset, delay and dispersion values, according to the on-wire protocol
		//		(See RFC5905 p29, p37)
		//		First order differences are computed on fixed point values. Then the remaining computations are
		//		carried on floating point (seconds) values
		//
		//		The convention used here is that a positive offset means the server clock is into the future
		//		relative to the client clock.

		//TODO(martin): t4 should always be > t1, but we should check for incorrect packets !!

		f64 t21 = (packet.t2 > packet.t1) ?
				(TimestampToSeconds(packet.t2 - packet.t1)) :
				-(TimestampToSeconds(packet.t1 - packet.t2));

		f64 t34 = (packet.t3 > t4) ? (TimestampToSeconds(packet.t3 - t4)) : -(TimestampToSeconds(t4 - packet.t3));
		f64 delay = t21 - t34;
		f64 offset = 0.5*(t21 + t34);

		f64 upEstimate = t21;
		f64 downEstimate = -t34;

		//DEBUG(martin): print delay, offset and up/down estimates in csv format

		printf("%.12f ; %.12f ; %.12f ; %.12f ;\n", delay, offset, upEstimate, downEstimate);

		timespec tp;
		tp.tv_sec = 1;
		tp.tv_nsec = 0;
		nanosleep(&tp, 0);
	}

	close(sock);
//	printf("clean exit\n");
	return(0);
}
