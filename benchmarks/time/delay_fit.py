import numpy as np
from scipy.stats import pareto
import matplotlib.pyplot as plt
import csv

csvfile = open('data/delay_localhost.csv')
reader = csv.reader(csvfile, delimiter = ';')
lines = list(reader)

rtt = [sublist[0] for sublist in lines]
offs = [sublist[1] for sublist in lines]
up = [sublist[2] for sublist in lines]
down = [sublist[3] for sublist in lines]


array = np.array(up, dtype=np.float)


hist = np.histogram(array, 100)

est = pareto.fit(array, loc=0.000048)

print(est)

shape = est[0]
loc = est[1]
scale = est[2]
x = np.linspace(np.min(array), np.max(array), 100)
plt.plot(x, pareto.pdf(x, shape, loc, scale))

plt.hist(array, 100, density=True)
plt.show()
