/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include<stdio.h>
#include<sys/time.h>
#include<limits.h>

int main(int argc, char** argv)
{
	timeval lastTv;
	gettimeofday(&lastTv, 0);

	timeval minDiff;
	minDiff.tv_sec = INT_MAX;
	minDiff.tv_usec = INT_MAX;

	const int GRANULARITY_NUM_ITERATION = 1000000;
	for(int i=0; i<GRANULARITY_NUM_ITERATION; i++)
	{
		timeval tv;
		gettimeofday(&tv, 0);

		timeval diff;
		timersub(&tv, &lastTv, &diff);

		if(!timercmp(&tv, &lastTv, ==) && timercmp(&diff, &minDiff, <))
		{
			minDiff = diff;
		}
		lastTv = tv;
	}

	gettimeofday(&lastTv, 0);
	timeval tv;
	timeval duration;
	const int PRECISION_NUM_ITERATION = 100000000;
	for(int i=0; i<PRECISION_NUM_ITERATION; i++)
	{
		gettimeofday(&tv, 0);
	}
	gettimeofday(&tv, 0);
	timersub(&tv, &lastTv, &duration);

	long long nsec = (duration.tv_usec * 10e3) / PRECISION_NUM_ITERATION;
	long long sec = duration.tv_sec / PRECISION_NUM_ITERATION;
	long long rem = duration.tv_sec % PRECISION_NUM_ITERATION;
	nsec += rem * (long long)10e9 / PRECISION_NUM_ITERATION;

	printf("clock granularity estimate : %li sec, %i usec\n", minDiff.tv_sec, minDiff.tv_usec);
	printf("clock query duration estimate : %lli sec, %lli nsec\n", sec, nsec);
	return(0);
}
