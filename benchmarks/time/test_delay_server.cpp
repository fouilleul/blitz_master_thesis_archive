/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: server.cpp
*	@author: Martin Fouilleul
*	@date: 07/03/2019
*	@revision:
*
*****************************************************************/

/*
	Respond to synchronization requests from clients
*/

#include<sys/socket.h>
#include<netinet/ip.h>
#include<unistd.h>	// close()
#include<arpa/inet.h>	// inet_addr()

//DEBUG
#include<stdio.h>
#include<assert.h>

#include"packet.h"
#include"platform_time.h"

#define PORT 4559	//TODO(martin): pass it as a command line argument...

int main(int argc, char** argv)
{
	//NOTE(martin): create a socket and bind it to the server port

	int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	assert(sock);

	sockaddr_in addr;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(PORT);
	addr.sin_family = AF_INET;

	int res = bind(sock, (sockaddr*)&addr, sizeof(addr));
	if(res)
	{
		perror("Failed binding socket");
		assert(0);
	}


	sync_packet packet;
	sockaddr_in from;

	//TODO(martin): daemonize, or at least catch sigint
	while(1)
	{
		//TODO(martin): use select instead of recvfrom ?

		socklen_t fromSize = sizeof(from);
		ssize_t size = recvfrom(sock, &packet, sizeof(sync_packet), 0, (sockaddr*)&from, &fromSize);

		u64 t2 = GetSystemTimestamp();

		packet.type = SYNC_REPLY;
		packet.t2 = t2;
		packet.t3 = GetSystemTimestamp();

		if(sendto(sock, &packet, sizeof(sync_packet), 0, (sockaddr*)&from, sizeof(from)) == -1)
		{
			perror("Server sendto()");
		}
	}

	close(sock);

	return(0);
}
