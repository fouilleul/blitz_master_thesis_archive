import numpy as np
from scipy.stats import pareto
import matplotlib.pyplot as plt
import csv

csvfile = open('rtt_localhost.csv')
reader = csv.reader(csvfile, delimiter = ';')
lines = list(reader)
flat_list = [item for sublist in lines for item in sublist]
array = np.array(flat_list, dtype=np.float)


hist = np.histogram(array, 100)

est = pareto.fit(array, loc=0.04)

print(est)

shape = est[0]
loc = est[1]
scale = est[2]
x = np.linspace(np.min(array), np.max(array), 100)
plt.plot(x, pareto.pdf(x, shape, loc, scale))

plt.hist(array, 100)
plt.show()
