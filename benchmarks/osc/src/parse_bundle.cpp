/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include<stdio.h>

#if USE_OSCPACK
	#include"OscReceivedElements.h" // oscpack
#elif USE_LIBLO
	#include"lo/lo_cpp.h"		// liblo
#elif USE_RTOSC
	#include"rtosc.h"		// rtosc
#elif USE_LIBO

	#include"osc_bundle_s.h"
	#include"osc_bundle_iterator_s.h"
	#include"osc_message_s.h"	// libo
	#include"osc_atom_s.h"
	#include"osc_message_iterator_s.h"
#endif

#include"osc.h"			// blitz osc
#include"platform_time.h"
#include"user_clock.h"

#ifndef PACKET_COUNT
	#define PACKET_COUNT 10000000
#endif

int main()
{
	printf("parsing bundles ");
	#ifdef USE_OSCPACK
		printf("using oscpack");
	#elif USE_RTOSC
		printf("using rtosc");
	#elif USE_BLITZ_ITERATOR
		printf("using blitz osc_iterator");
	#elif USE_BLITZ_SCAN
		printf("using blitz OscScan");
	#elif USE_LIBO
		printf("using libo");
	#elif USE_LIBLO
		#error "liblo does not provide a public API for deserializing/parsing bundles..."
	#else
		#error "No OSC API defined !"
	#endif

	printf(" ; %i ; ", PACKET_COUNT);

	const char* address1 = "/test/message/1";
	const char* address2 = "/test/message/2";
	const char* s1 = "This is test 1 !";
	const char* s2 = "This is test 2 !";
	int64 h1 = 1234;
	int64 h2 = 5678;
	double d1 = 3.14159;
	double d2 = 1.6180;

	const int buffSize = 2048;
	char buffer[buffSize];
	int packetSize;
	{
		osc_element elt;
		OscBeginPacket(&elt, buffSize, buffer);
			OscBeginBundle(&elt, 0);
				OscFormatMessage(&elt, address1, "shd", s1, h1, d1);
				OscFormatMessage(&elt, address2, "shd", s2, h2, d2);
			OscEndBundle(&elt);
		OscEndPacket(&elt);
		packetSize = OscElementSize(&elt);
	}

	volatile const char* pa; // parsed address
	volatile const char* ps; // parsed string
	volatile int64 ph;	// parsed int
	volatile double pd;	// parsed double

	u64 start = GetSystemTimestamp();

	for(int i=0; i<PACKET_COUNT; i++)
	{
	#if USE_LIBO

		// iterate over messages in an unserialized bundle
		t_osc_bndl_it_s *b_it = osc_bndl_it_s_get(packetSize, buffer);

		while(osc_bndl_it_s_hasNext(b_it))
		{
			t_osc_msg_s *msg_s = osc_bndl_it_s_next(b_it);

			if(!strcmp(osc_message_s_getTypetags(msg_s)+1, "shd"))
			{
				t_osc_msg_it_s *m_it_s = osc_msg_it_s_get(msg_s);
				t_osc_atom_s *atom = osc_msg_it_s_next(m_it_s);
				ps = osc_atom_s_getData(atom);
				atom = osc_msg_it_s_next(m_it_s);
				ph = osc_atom_s_getInt64(atom);
				atom = osc_msg_it_s_next(m_it_s);
				pd = osc_atom_s_getDouble(atom);

				osc_msg_it_s_destroy(m_it_s);
			}
			else
			{
				printf("typetags = %s\n", osc_message_s_getTypetags(msg_s));
				ASSERT(0);
			}
		}

	#elif USE_OSCPACK

		try
		{
			osc::ReceivedPacket packet(buffer, packetSize);

			ASSERT(packet.IsBundle());
			osc::ReceivedBundle bundle(packet);
			for(osc::ReceivedBundle::const_iterator it = bundle.ElementsBegin();
			    it != bundle.ElementsEnd();
			    it++)
			{
				ASSERT(it->IsMessage());
				osc::ReceivedMessage msg(*it);

				pa = msg.AddressPattern();
				osc::ReceivedMessageArgumentStream args = msg.ArgumentStream();

				if(!strcmp(msg.TypeTags(), "shd"))
				{
					const char* ops;
					osc::int64 oph;
					double opd;
					args >> ops >> oph >> opd >> osc::EndMessage;
				}
				else
				{
					ASSERT(0);
				}
			}
		}
		catch(...)
		{
			ASSERT(0);
		}

	#elif USE_RTOSC

		ASSERT(rtosc_bundle_p(buffer));

		int count = rtosc_bundle_elements(buffer, buffSize);

		for(int i=0; i<count; i++)
		{
			const char* msg = rtosc_bundle_fetch(buffer, i);
			size_t eltSize = rtosc_bundle_size(buffer, i);

			if(rtosc_valid_message_p(msg, eltSize))
			{
				if(!strcmp(rtosc_argument_string(msg), "shd"))
				{
					rtosc_arg_itr_t it = rtosc_itr_begin(msg);
					pa = msg;
					ps = rtosc_itr_next(&it).val.s;
					ph = rtosc_itr_next(&it).val.h;
					pd = rtosc_itr_next(&it).val.d;
				}
				else
				{
					ASSERT(0);
				}
			}
			else
			{
				ASSERT(0);
			}
		}

	#elif USE_BLITZ_ITERATOR
		osc_element elt;

		if(OscParsePacket(&elt, packetSize, buffer) != OSC_OK)
		{
			ASSERT(0);
		}
		ASSERT(OscElementIsBundle(&elt));
		for(osc_element elt_it = OscElementsBegin(&elt);
		    !OscIsAtEndOfBundle(&elt, &elt_it);
		    OscElementNext(&elt_it))
		{
			ASSERT(OscElementIsMessage(&elt_it));
			osc_msg* msg = OscElementAsMessageUnchecked(&elt_it);

			pa = OscAddressPattern(msg);
			if(!strcmp(OscTypeTags(msg), "shd"))
			{
				osc_arg_iterator it = OscArgumentsBegin(msg);
				ps = OscAsStringUnchecked(&it); OscArgumentNext(&it);
				ph = OscAsInt64Unchecked(&it); OscArgumentNext(&it);
				pd = OscAsDoubleUnchecked(&it);
			}
			else
			{
				ASSERT(0);
			}
		}

	#elif USE_BLITZ_SCAN

		osc_element elt;

		if(OscParsePacket(&elt, packetSize, buffer) != OSC_OK)
		{
			ASSERT(0);
		}
		ASSERT(OscElementIsBundle(&elt));
		for(osc_element elt_it = OscElementsBegin(&elt);
		    !OscIsAtEndOfBundle(&elt, &elt_it);
		    OscElementNext(&elt_it))
		{
			ASSERT(OscElementIsMessage(&elt_it));
			osc_msg* msg = OscElementAsMessageUnchecked(&elt_it);
			ASSERT(msg);

			pa = OscAddressPattern(msg);

			//NOTE(martin): here we do not check the type string because it is checked inside OscScan()
			//              the typical use case is to check the return value of OscScan() for a type mismatch
			if(OscScanArguments(msg, "shd", &ps, &ph, &pd) != OSC_OK)
			{
				ASSERT(0);
			}
		}
	#endif
	}

	u64 delta = GetSystemTimestamp() - start;
	double length = TimestampToSeconds(delta);
	printf("%.12f\n", length);

	return(0);
}
