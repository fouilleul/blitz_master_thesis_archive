/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include<stdio.h>


#include"OscOutboundPacketStream.h" // oscpack
#include"lo/lo_cpp.h"		    // liblo
#include"rtosc.h"		    // rtosc
#include"osc_bundle_u.h"	    // libo
#include"osc.h"			    // blitz osc
#include"platform_time.h"
#include"user_clock.h"

#ifndef PACKET_COUNT
	#define PACKET_COUNT 10000000
#endif
//#define PACKET_COUNT 1

int main()
{
	printf("compose bundles ");
	#if USE_LIBLO
		printf("using liblo");
	#elif USE_LIBO
		printf("using libo");
	#elif USE_OSCPACK
		printf("using oscpack");
	#elif USE_RTOSC
		printf("using rtosc");
	#elif USE_BLITZ_PUSH
		printf("using blitz osc (push API)");
	#elif USE_BLITZ_FORMAT
		printf("using blitz osc (format API)");
	#else
		#error No OSC API defined !
	#endif

	printf(" ; %i ; ", PACKET_COUNT);

	const char* address1 = "/test/message/1";
	const char* address2 = "/test/message/2";
	const char* s1 = "This is test 1 !";
	const char* s2 = "This is test 2 !";
	int64 h1 = 1234;
	int64 h2 = 5678;
	double d1 = 3.14159;
	double d2 = 1.6180;

	const int buffSize = 2048;
	char buffer[buffSize];

	u64 start = GetSystemTimestamp();

	for(int i=0; i<PACKET_COUNT; i++)
	{
	#if USE_LIBLO
		lo::Message msg1("shd", s1, h1, d1);
		lo::Message msg2("shd", s2, h2, d2);
		lo::Bundle bundle;
		bundle.add(address1, msg1);
		bundle.add(address2, msg2);

		size_t size = buffSize;
		bundle.serialise(buffer, &size);

	#elif USE_LIBO
		t_osc_bndl_u *bundle = osc_bundle_u_alloc();

		t_osc_msg_u* msg1 = osc_message_u_alloc();
		osc_message_u_setAddress(msg1, address1),
		osc_message_u_appendString(msg1, s1);
		osc_message_u_appendInt64(msg1, h1);
		osc_message_u_appendDouble(msg1, d1);

		t_osc_msg_u* msg2 = osc_message_u_alloc();
		osc_message_u_setAddress(msg1, address2),
		osc_message_u_appendString(msg1, s2);
		osc_message_u_appendInt64(msg1, h2);
		osc_message_u_appendDouble(msg1, d2);

		osc_bundle_u_addMsg(bundle, msg1);
		osc_bundle_u_addMsg(bundle, msg2);

		long size = osc_bundle_u_nserialize(NULL, 0, bundle);
		osc_bundle_u_nserialize(buffer, size, bundle);

		//NOTE(martin): apparently osc_bundle_u_addMsg() frees the message on our behalf...
		osc_bundle_u_free(bundle);

	#elif USE_OSCPACK
		osc::OutboundPacketStream packet(buffer, buffSize);

		//NOTE(martin): we have to explicitly cast to (osc::int64) otherwise calls to << are ambiguous
		//		even if its exactly the same type...

		packet << osc::BeginBundle() <<
			osc::BeginMessage(address1)
				<< s1
				<< (osc::int64)h1
				<< d1
			<< osc::EndMessage
			<< osc::BeginMessage(address2)
				<< s2
				<< (osc::int64)h2
				<< d2
			<< osc::EndMessage
		<< osc::EndBundle;
		size_t size = packet.Size();

	#elif USE_RTOSC

		char buffer1[buffSize];
		char buffer2[buffSize];
		rtosc_message(buffer1, buffSize, address1, "shd", s1, h1, d1);
		rtosc_message(buffer2, buffSize, address2, "shd", s2, h2, d2);
		size_t size = rtosc_bundle(buffer, buffSize, 0, 2, buffer1, buffer2);

	#elif USE_BLITZ_PUSH

		osc_element elt;
		OscBeginPacket(&elt, buffSize, buffer);
		OscBeginBundle(&elt);
			OscBeginMessage(&elt, address1);
				OscPushString(&elt, s1);
				OscPushInt64(&elt, h1);
				OscPushDouble(&elt, d1);
			OscEndMessage(&elt);
			OscBeginMessage(&elt, address2);
				OscPushString(&elt, s2);
				OscPushInt64(&elt, h2);
				OscPushDouble(&elt, d2);
			OscEndMessage(&elt);
		OscEndBundle(&elt);
		OscEndPacket(&elt);
		size_t size = OscElementSize(&elt);

	#elif USE_BLITZ_FORMAT
		osc_element elt;
		OscBeginPacket(&elt, buffSize, buffer);
			OscBeginBundle(&elt);
				OscFormatMessage(&elt, address1, "shd", s1, h1, d1);
				OscFormatMessage(&elt, address2, "shd", s2, h2, d2);
			OscEndBundle(&elt);
		OscEndPacket(&elt);
		size_t size = OscElementSize(&elt);
	#endif
	}

	u64 delta = GetSystemTimestamp() - start;
	double length = TimestampToSeconds(delta);
	printf("%.12f\n", length);

	return(0);
}
