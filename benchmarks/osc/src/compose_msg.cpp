/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include<stdio.h>


#include"OscOutboundPacketStream.h" // oscpack
#include"lo/lo_cpp.h"		    // liblo
#include"rtosc.h"		    // rtosc
#include"osc_message_u.h"	    // libo

#include"osc.h"			    // blitz osc
#include"platform_time.h"
#include"user_clock.h"

#ifndef PACKET_COUNT
	#define PACKET_COUNT 10000000
#endif

int main()
{
	printf("compose messages ");
	#if USE_LIBLO
		printf("using liblo");
	#elif USE_LIBO
		printf("using libo");
	#elif USE_OSCPACK
		printf("using oscpack");
	#elif USE_RTOSC
		printf("using rtosc");
	#elif USE_BLITZ_PUSH
		printf("using blitz osc (push API)");
	#elif USE_BLITZ_FORMAT
		printf("using blitz osc (format API)");
	#else
		#error No OSC API defined !
	#endif

	printf(" ; %i ; ", PACKET_COUNT);

	const char* address = "/foo/bar";
	const char* s = "Hello, world !";
	int64 h = 4000;
	double d = 6.28218;

	const int buffSize = 2048;
	char buffer[buffSize];

	u64 start = GetSystemTimestamp();

	for(int i=0; i<PACKET_COUNT; i++)
	{
	#if USE_LIBLO
		size_t size = buffSize;

		lo_message lo = lo_message_new();
		lo_message_add_string(lo, s);
		lo_message_add_int64(lo, h);
		lo_message_add_double(lo, d);

		lo_message_serialise(lo, address, buffer, &size);
		lo_message_free(lo);

	#elif USE_LIBO

		//NOTE(martin): this seems to be the only API to create a message ??
		t_osc_msg_u* msg = osc_message_u_alloc();
		osc_message_u_setAddress(msg, address),
		osc_message_u_appendString(msg, s);
		osc_message_u_appendInt64(msg, h);
		osc_message_u_appendDouble(msg, d);

		t_osc_msg_s* serialized = osc_message_u_serialize(msg);

		osc_message_u_free(msg);

	#elif USE_OSCPACK
		osc::OutboundPacketStream packet(buffer, buffSize);

		//NOTE(martin): we have to explicitly cast to (osc::int64) otherwise calls to << are ambiguous
		//		even if its exactly the same type...

		packet << osc::BeginMessage(address)
				<< s
				<< (osc::int64)h
				<< d
			<< osc::EndMessage;

		size_t size = packet.Size();
	#elif USE_RTOSC

		size_t size = rtosc_message(buffer, buffSize, address, "shd", s, h, d);

	#elif USE_BLITZ_PUSH

		osc_element elt;
		OscBeginPacket(&elt, buffSize, buffer);
			OscBeginMessage(&elt, address);
				OscPushString(&elt, s);
				OscPushInt64(&elt, h);
				OscPushDouble(&elt, d);
			OscEndMessage(&elt);
		OscEndPacket(&elt);
		size_t size = OscElementSize(&elt);

	#elif USE_BLITZ_FORMAT
		osc_element elt;
		OscFormat(&elt, buffSize, buffer, address, "shd", s, h, d);
		size_t size = OscElementSize(&elt);
	#endif
	}

	u64 delta = GetSystemTimestamp() - start;
	double length = TimestampToSeconds(delta);
	printf("%.12f\n", length);

	return(0);
}
