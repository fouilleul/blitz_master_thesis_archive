/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include<stdio.h>

#if USE_OSCPACK
	#include"OscReceivedElements.h" // oscpack
#elif USE_LIBLO
	#include"lo/lo_cpp.h"		// liblo
#elif USE_RTOSC
	#include"rtosc.h"		// rtosc
#elif USE_LIBO
	#include"osc_message_s.h"	// libo
	#include"osc_atom_s.h"
	#include"osc_message_iterator_s.h"
#endif

#include"osc.h"			// blitz osc
#include"platform_time.h"
#include"user_clock.h"

#ifndef PACKET_COUNT
	#define PACKET_COUNT 10000000
#endif

int main()
{
	printf("parsing messages ");
	#if USE_LIBLO
		printf("using liblo");
	#elif USE_LIBO
		printf("using libo");
	#elif USE_OSCPACK
		printf("using oscpack");
	#elif USE_RTOSC
		printf("using rtosc");
	#elif USE_BLITZ_ITERATOR
		printf("using blitz osc_iterator");
	#elif USE_BLITZ_SCAN
		printf("using blitz OscScan");
	#else
		#error No OSC API defined !
	#endif

	printf(" ; %i ; ", PACKET_COUNT);

	const char* address = "/foo/bar";
	const char* s = "Hello, world !";
	int64 h = 4000;
	double d = 6.28218;

	const int buffSize = 2048;
	char buffer[buffSize];
	int packetSize;
	{
		osc_element elt;
		OscFormat(&elt, buffSize, buffer, address, "shd", s, h, d);
		packetSize = OscElementSize(&elt);
	}

	volatile const char* pa; // parsed address
	volatile const char* ps; // parsed string
	volatile int64 ph;	// parsed int
	volatile double pd;	// parsed double

	u64 start = GetSystemTimestamp();

	for(int i=0; i<PACKET_COUNT; i++)
	{
	#if USE_LIBLO

		//NOTE(martin): a little bit unfair, because liblo does not seem to expose a simple way to get
		//		the address pattern of a deserialized (but not outgoing) message ??

		lo::Message::maybe maybe = lo::Message::deserialise(buffer, packetSize);
		if(maybe.first)
		{
			ASSERT(0);
		}
		lo::Message msg = maybe.second;

		//NOTE(martin): to measure the real use case and not be unfair with safer implementations,
		//		we introduce the typestring check here

		if(!strcmp(msg.types().c_str(), "shd"))
		{
			lo_arg** argv = msg.argv();
			ps = &argv[0]->s;
			ph = argv[1]->h;
			pd = argv[2]->d;
		}
		else
		{
			ASSERT(0);
		}

	#elif USE_LIBO
		t_osc_msg_s* msg_s = osc_message_s_alloc();
		osc_message_s_wrap(msg_s, buffer);
		pa = osc_message_s_getAddress(msg_s);

		if(!strcmp(osc_message_s_getTypetags(msg_s)+1, "shd"))
		{
			t_osc_msg_it_s *m_it_s = osc_msg_it_s_get(msg_s);
			t_osc_atom_s *atom = osc_msg_it_s_next(m_it_s);
			ps = osc_atom_s_getData(atom);
			atom = osc_msg_it_s_next(m_it_s);
			ph = osc_atom_s_getInt64(atom);
			atom = osc_msg_it_s_next(m_it_s);
			pd = osc_atom_s_getDouble(atom);

			osc_msg_it_s_destroy(m_it_s);
		}
		else
		{
			printf("typetags = %s\n", osc_message_s_getTypetags(msg_s));
			ASSERT(0);
		}

		osc_message_s_free(msg_s);

	#elif USE_OSCPACK

		try
		{
			osc::ReceivedPacket packet(buffer, packetSize);
			osc::ReceivedMessage msg(packet);

			pa = msg.AddressPattern();
			osc::ReceivedMessageArgumentStream args = msg.ArgumentStream();

			if(!strcmp(msg.TypeTags(), "shd"))
			{
				const char* ops;
				osc::int64 oph;
				double opd;
				args >> ops >> oph >> opd >> osc::EndMessage;
			}
			else
			{
				ASSERT(0);
			}
		}
		catch(...)
		{
			ASSERT(0);
		}

	#elif USE_RTOSC

		if(rtosc_valid_message_p(buffer, packetSize))
		{
			if(!strcmp(rtosc_argument_string(buffer), "shd"))
			{
				rtosc_arg_itr_t it = rtosc_itr_begin(buffer);
				pa = buffer;
				ps = rtosc_itr_next(&it).val.s;
				ph = rtosc_itr_next(&it).val.h;
				pd = rtosc_itr_next(&it).val.d;
			}
			else
			{
				ASSERT(0);
			}
		}
		else
		{
			ASSERT(0);
		}

	#elif USE_BLITZ_ITERATOR
		osc_msg msg;
		if(OscParseMessage(&msg, packetSize, buffer) != OSC_OK)
		{
			ASSERT(0);
		}
		pa = OscAddressPattern(&msg);
		if(!strcmp(OscTypeTags(&msg), "shd"))
		{
			osc_arg_iterator it = OscArgumentsBegin(&msg);
			ps = OscAsStringUnchecked(&it); OscArgumentNext(&it);
			ph = OscAsInt64Unchecked(&it); OscArgumentNext(&it);
			pd = OscAsDoubleUnchecked(&it);
		}
		else
		{
			ASSERT(0);
		}

	#elif USE_BLITZ_SCAN
		osc_msg msg;
		if(OscParseMessage(&msg, packetSize, buffer) != OSC_OK)
		{
			ASSERT(0);
		}
		pa = OscAddressPattern(&msg);

		//NOTE(martin): here we do not check the type string because it is checked inside OscScan()
		//              the typical use case is to check the return value of OscScan() for a type mismatch
		if(OscScanArguments(&msg, "shd", &ps, &ph, &pd) != OSC_OK)
		{
			ASSERT(0);
		}

		//printf("%s : %s %lli %f\n", spa, sps, sph, spd);
	#endif
	}

	u64 delta = GetSystemTimestamp() - start;
	double length = TimestampToSeconds(delta);
	printf("%.12f\n", length);

	return(0);
}
