/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#ifndef RUN_COUNT
	#define RUN_COUNT 100000
#endif

int main(int argc, char** argv)
{
	double scale = argc > 1 ? atof(argv[1]) : 0.001;
	double shape = argc > 2 ? atof(argv[2]) : 50;

	sranddev();

	for(int i=0; i<RUN_COUNT; i++)
	{
		int irand = rand();
		double uniform = irand/(double)(RAND_MAX);
		double pareto = scale / exp(1./shape * log(1-uniform));

		printf("%f ; %f ; %i\n", pareto, uniform, rand());
	}
}
