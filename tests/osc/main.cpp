/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include<stdio.h>
#include"osc.h"


int main()
{
	const int buffSize = 1024;
	char buffer[buffSize];
	osc_element elt;

	printf("test 1 :\n");
/*
	OscFormat(&elt, 128, buffer, "/getrpc", "s", "/greetings");

	int size = OscElementSize(&elt);

	ASSERT(OscParsePacket(&elt, size, buffer) == OSC_OK);
	ASSERT(OscElementIsMessage(&elt));
	osc_msg msg;
	ASSERT(OscParseMessage(&msg, size, buffer) == OSC_OK);

	OscElementPrint(&elt);
*/

	OscBeginPacket(&elt, buffSize, buffer);
		OscBeginBundle(&elt, 1);

			OscBeginMessage(&elt, "/foo/bar");
				OscPushInt32(&elt, 32);
				OscPushString(&elt, "Hello, world !");
			OscEndMessage(&elt);

			OscBeginBundle(&elt, 2);
				OscBeginBundle(&elt, 3);
					OscBeginMessage(&elt, "/xxx");
						OscPushFloat(&elt, 3.14);
					OscEndMessage(&elt);
				OscEndBundle(&elt);

				OscBeginMessage(&elt, "/nest/bar");
					OscPushBool(&elt, true);
				OscEndMessage(&elt);

				OscBeginMessage(&elt, "/nest/baz");
					OscPushInfinity(&elt);
				OscEndMessage(&elt);

			OscEndBundle(&elt);

			OscBeginMessage(&elt, "/foo/baz");
				OscPushInt64(&elt, 1000000000);
				OscPushDouble(&elt, 6.3);
			OscEndMessage(&elt);

		OscEndBundle(&elt);
	OscEndPacket(&elt);

	int size = OscElementSize(&elt);
	OscParsePacket(&elt, size, buffer);

	OscElementPrint(&elt);

	printf("test 2 : \n");

	OscBeginPacket(&elt, buffSize, buffer);
		OscBeginMessage(&elt, "/foo/bar");
			OscBeginBundle(&elt);
				OscBeginMessage(&elt, "/nest/1");
					OscPushString(&elt, "Nest 1 !");
				OscEndMessage(&elt);
				OscBeginMessage(&elt, "/nest/2");
					OscPushString(&elt, "Nest 2 !");
				OscEndMessage(&elt);
			OscEndBundle(&elt);

			OscBeginMessage(&elt, "/nest/3");
				OscPushString(&elt, "Nest 3 !");
			OscEndMessage(&elt);
		OscEndMessage(&elt);
	OscEndPacket(&elt);

	OscElementPrint(&elt);

	printf("test 3 : \n");

	OscFormat(&elt, buffSize, buffer, "/foo/bar", "hsf", 4321, "Hello world !", 3.14159);
	OscElementPrint(&elt);

	return(0);
}
