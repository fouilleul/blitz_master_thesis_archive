/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: main.cpp
*	@author: Martin Fouilleul
*	@date: 19/04/2019
*	@revision:
*
*****************************************************************/

#include<unistd.h>
#include<stdio.h>

#include"debug_log.h"
#include"platform_socket.h"
#include"platform_thread.h"
#include"osc.h"
#include"directory.cpp"


int GetReply(platform_socket* retSock, osc_msg* msg, char* buffer, int bufferSize)
{
	uint32 msgSize;
	char msgSizeBuff[4];
	if(SocketReceive(retSock, msgSizeBuff, sizeof(uint32), 0) != sizeof(uint32))
	{
		ERROR_PRINTF("Can't read packet size\n");
		return(-1);
	}
	msgSize = OscToInt32(msgSizeBuff);
	if(msgSize > bufferSize)
	{
		ERROR_PRINTF("Packet size exceeds receive buffer\n");
		return(-1);
	}
	if(SocketReceive(retSock, (void*)buffer, msgSize, 0) != msgSize)
	{
		ERROR_PRINTF("Couldn't read packet\n");
		return(-1);
	}

	OscErrCode err = OSC_OK;
	if((err = OscMessageLayoutInit(msg, buffer, msgSize)) != OSC_OK)
	{
		ERROR_PRINTF("Bad OSC packet : %s", OscGetErrorMessage(err));
		return(-1);
	}
	return(0);
}

int CheckReply(platform_socket* retSock, int buffSize, char* buffer, osc_msg* msg)
{
	if(GetReply(retSock, msg, buffer, buffSize) != 0)
	{
		return(-1);
	}

	if(!strcmp(msg->addressPattern, "/OK!"))
	{
		DEBUG_PRINTF("service directory reply : %s\n", msg->addressPattern);
		return(0);
	}
	else if(!strcmp(msg->addressPattern, "/MULTI"))
	{
		osc_arg_iterator arg = OscArgumentsBegin(msg);
		if(OscArgumentType(&arg) != 'i')
		{
			ERROR_PRINTF("bad argument type for /MULTI reply\n");
			return(-1);
		}
		int count = OscAsInt32Unchecked(&arg);
		int ret = 0;
		for(int i=0; i<count; i++)
		{
			ret |= CheckReply(retSock, buffSize, buffer, msg);
		}
		return(ret);
	}
	else
	{
		ERROR_PRINTF("service directory reply : %s\n", msg->addressPattern);
		return(-1);
	}
}


int UnRef(service_directory* directory, platform_socket* client, platform_socket* retSock, uint64 ID)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, "/&", buffer, bufferSize);
		OscPushInt64(&msg, ID);
		OscPushString(&msg, "unref");
	OscMessageEnd(&msg);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	return(CheckReply(retSock, bufferSize, buffer, &msg));
}

uint64 GetRef(service_directory* directory, platform_socket* client, platform_socket* retSock, const char* path , uint64 baseID = 0)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	if(baseID)
	{
		OscMessageBegin(&msg, "/&", buffer, bufferSize);
			OscPushInt64(&msg, baseID);
			OscPushString(&msg, "getref");
			OscPushString(&msg, path);
		OscMessageEnd(&msg);
	}
	else
	{
		OscMessageBegin(&msg, "/", buffer, bufferSize);
			OscPushString(&msg, "getref");
			OscPushString(&msg, path);
		OscMessageEnd(&msg);
	}
	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	// todo : get the reply on retSock

	if(CheckReply(retSock, bufferSize, buffer, &msg) != 0)
	{
		return(0);
	}
	else
	{
		if(strcmp(OscTypeTags(&msg), "h"))
		{
			ERROR_PRINTF("Bad type string in the directory reply : %s\n", OscTypeTags(&msg));
			return(0);
		}
		osc_arg_iterator arg = OscArgumentsBegin(&msg);
		return(OscAsInt64Unchecked(&arg));
	}
}

uint64 InsertRef(service_directory* directory, platform_socket* client, platform_socket* retSock, const char* inPath)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, inPath, buffer, bufferSize);
		OscPushString(&msg, "insert");
	OscMessageEnd(&msg);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	// todo : get the reply on retSock

	if(CheckReply(retSock, bufferSize, buffer, &msg) != 0)
	{
		return(0);
	}
	else
	{
		if(strcmp(OscTypeTags(&msg), "h"))
		{
			ERROR_PRINTF("Bad type string in the directory reply : %s\n", OscTypeTags(&msg));
			return(0);
		}
		osc_arg_iterator arg = OscArgumentsBegin(&msg);
		return(OscAsInt64Unchecked(&arg));
	}
}

int WriteInt(service_directory* directory, platform_socket* client, platform_socket* retSock, const char* path, int i)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, path, buffer, bufferSize);
		OscPushString(&msg, "write");
		OscPushInt32(&msg, i);
	OscMessageEnd(&msg);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));


	// todo : get the reply on retSock

	return(CheckReply(retSock, bufferSize, buffer, &msg));
}

int WriteString(service_directory* directory, platform_socket* client, platform_socket* retSock, const char* path, const char* string)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, path, buffer, bufferSize);
		OscPushString(&msg, "write");
		OscPushString(&msg, string);
	OscMessageEnd(&msg);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	// todo : get the reply on retSock

	return(CheckReply(retSock, bufferSize, buffer, &msg));
}

int Register(service_directory* directory, platform_socket* client, platform_socket* retSock, const char* path, host_port port)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, path, buffer, bufferSize);
		OscPushString(&msg, "register");
		OscPushInt16(&msg, port);
	OscMessageEnd(&msg);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	// todo : get the reply on retSock

	return(CheckReply(retSock, bufferSize, buffer, &msg));
}


int Read(service_directory* directory, platform_socket* client, platform_socket* retSock, const char* path)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, path, buffer, bufferSize);
		OscPushString(&msg, "read");
	OscMessageEnd(&msg);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	if(CheckReply(retSock, bufferSize, buffer, &msg) != 0)
	{
		return(-1);
	}
	else
	{
		printf("Directory response : ");
		OscMessagePrint(&msg);
		return(0);
	}
}

void* Accept(void* p)
{
	platform_socket* sock = (platform_socket*)p;
	socket_address from;
	platform_socket* client = SocketAccept(sock, &from);
	return(client);
}

void* Listener(void* p)
{
	host_port port = *(host_port*)p;

	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;


	platform_socket* sock = SocketOpen(SOCK_TCP);
	socket_address addr;
	addr.ip = SOCK_IP_ANY;
	addr.port = HostToNetPort(port);
	SocketBind(sock, &addr);
	SocketListen(sock, 10);

	socket_address from;
	platform_socket* client = SocketAccept(sock, &from);

	uint32 msgSize;
	char msgSizeBuff[4];
	if(SocketReceive(client, msgSizeBuff, sizeof(uint32), 0) != sizeof(uint32))
	{
		ERROR_PRINTF("Can't read packet size\n");
		return(0);
	}
	msgSize = OscToInt32(msgSizeBuff);
	if(msgSize > bufferSize)
	{
		ERROR_PRINTF("Packet size exceeds receive buffer\n");
		return(0);
	}
	if(SocketReceive(client, (void*)buffer, msgSize, 0) != msgSize)
	{
		ERROR_PRINTF("Couldn't read packet\n");
		return(0);
	}

	OscMessageLayoutInit(&msg,  buffer, msgSize);
	printf("Received notification : ");
	OscMessagePrint(&msg);
	printf("\n");

	SocketClose(client);
	SocketClose(sock);

	return(0);
}

int Notify(service_directory* directory, platform_socket* client, platform_socket* retSock, const char* path, host_port port)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, path, buffer, bufferSize);
		OscPushString(&msg, "notify");
		OscPushString(&msg, "/test");
		OscPushInt32(&msg, 46);
		OscPushString(&msg, "yolo");
	OscMessageEnd(&msg);

	platform_thread* listener = ThreadCreate(Listener, (void*)&port);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	if(CheckReply(retSock, bufferSize, buffer, &msg) != 0)
	{
		return(-1);
	}
	else
	{
		printf("Directory response : ");
		OscMessagePrint(&msg);
		return(0);
	}
}

int SendTo(service_directory* directory, platform_socket* client, platform_socket* retSock, const char* path, const char* prefix, host_port port)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, path, buffer, bufferSize);
		OscPushString(&msg, "sendto");
		OscPushString(&msg, prefix);
		OscPushInt16(&msg, port);
	OscMessageEnd(&msg);

	platform_thread* listener = ThreadCreate(Listener, (void*)&port);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	if(CheckReply(retSock, bufferSize, buffer, &msg) != 0)
	{
		return(-1);
	}
	else
	{
		printf("Directory response : ");
		OscMessagePrint(&msg);
		return(0);
	}
}

int SendToID(service_directory* directory, platform_socket* client, platform_socket* retSock, uint64 ID, const char* prefix, host_port port)
{
	const int bufferSize = 1024;
	char buffer[bufferSize];
	osc_msg msg;

	OscMessageBegin(&msg, "/&", buffer, bufferSize);
		OscPushInt64(&msg, ID);
		OscPushString(&msg, "sendto");
		OscPushString(&msg, prefix);
		OscPushInt16(&msg, port);
	OscMessageEnd(&msg);

	platform_thread* listener = ThreadCreate(Listener, (void*)&port);

	ServiceDirectoryReceiveOscPacket(directory, client, HostToNetIP(SOCK_IP_LOOPBACK), OscPacket(&msg), OscMessageSize(&msg));

	if(CheckReply(retSock, bufferSize, buffer, &msg) != 0)
	{
		return(-1);
	}
	else
	{
		printf("Directory response : ");
		OscMessagePrint(&msg);
		return(0);
	}
}



int main(int argc, char** argv)
{
	service_directory directory;

	// create a triple of fake sockets to check the messages returned by the directory

	socket_address dirAddr;
	dirAddr.ip = StringToNetIP("127.0.0.1");
	dirAddr.port = 5000;

	platform_socket* dirSock = SocketOpen(SOCK_TCP);
	int r = 0;
	ASSERT((r = SocketBind(dirSock, &dirAddr)) == 0);
	ASSERT(SocketListen(dirSock, 10) == 0);

	platform_thread* acceptThread = ThreadCreate(Accept, dirSock);

	platform_socket* retSock = SocketOpen(SOCK_TCP);
	ASSERT(SocketConnect(retSock, &dirAddr) == 0);

	platform_socket* client = 0;
	ThreadJoin(acceptThread, (void**)&client);
	ThreadDestroy(acceptThread);

	ASSERT(client);

	int ret = 0;
	// begin tests

	printf("Create some nodes\n");

	uint64 foo_bar = GetRef(&directory, client, retSock, "/foo/bar");
	uint64 foo_baz = GetRef(&directory, client, retSock, "/foo/baz");

	uint64 foo_bar_xxx = GetRef(&directory, client, retSock, "/foo/bar/xxx");
	uint64 foo_bar_yyy = GetRef(&directory, client, retSock, "yyy", foo_bar);
	uint64 foo_bar_zzz = GetRef(&directory, client, retSock, "/foo/bar/zzz");

	uint64 gen_1 = InsertRef(&directory, client, retSock, "/foo/baz");
	uint64 gen_2 = InsertRef(&directory, client, retSock, "/foo/baz");
	uint64 gen_3 = InsertRef(&directory, client, retSock, "/foo/baz");

	printf("\n*** Write values ***\n\n");

	WriteInt(&directory, client, retSock, "/foo", 42);
	WriteString(&directory, client, retSock, "/foo/bar/yyy", "hello");
	Register(&directory, client, retSock, "/foo/bar/zzz", 7000);

	WriteInt(&directory, client, retSock, "/foo/baz/*", 5);

	printf("\n*** Show directory tree ***\n\n");
	ShowDirectoryTree(&directory, &directory.nodes[0], 0);

	printf("\n*** Unref some nodes ***\n\n");

	UnRef(&directory, client, retSock, foo_baz);
	UnRef(&directory, client, retSock, gen_1);
	UnRef(&directory, client, retSock, gen_2);

	printf("\n*** Show directory tree ***\n\n");
	ShowDirectoryTree(&directory, &directory.nodes[0], 0);

	printf("\n*** Read /foo/bar/yyy ***\n\n");
	Read(&directory, client, retSock, "/foo/bar/yyy");


	printf("\n*** Notify /foo/bar/zzz ***\n\n");
	Notify(&directory, client, retSock, "/foo/bar/zzz", 7000);

	printf("\n*** Send back /foo/bar/yyy to us on port 8000 ***\n\n");
	SendTo(&directory, client, retSock, "/foo/bar/yyy", "/test2", 8000);

	printf("\n*** Send back /foo/bar/yyy to us on port 9000, using node ID ***\n\n");
	SendToID(&directory, client, retSock, foo_bar_yyy, "/test3", 9000);


	printf("\n*** Unref last child of /foo/baz ***\n\n");

	UnRef(&directory, client, retSock, gen_3);

	printf("\n*** Show directory tree ***\n\n");
	ShowDirectoryTree(&directory, &directory.nodes[0], 0);

	SocketClose(retSock);

	sleep(1); //NOTE(martin): we need to wait in order to properly shut down the connection
		  //		  and not have an "Address already in use" error next time

	SocketClose(client);
	SocketClose(dirSock);

	return(0);
}
