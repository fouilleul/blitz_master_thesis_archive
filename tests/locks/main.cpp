/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: main.cpp
*	@author: Martin Fouilleul
*	@date: 19/04/2019
*	@revision:
*
*****************************************************************/

#include<unistd.h>
#include<stdio.h>

#include"blitz.h"

const char* DEFAULT_SERVER = "127.0.0.1";

int main(int argc, char** argv)
{
	const int buffSize = 1024;
	char buffer[buffSize];

	const char* address = (argc > 1) ? argv[1] : DEFAULT_SERVER;

	blitz_context* context = BlitzContextCreate(StringToNetIP(address));

	BlitzConnect(context);
	uint64 lock = BlitzLock(context, 0, "/node");

	printf("took lock\n");
	char c;
	scanf("%c", &c);

	BlitzUnlock(context, lock);

	BlitzDisconnect(context);
	BlitzContextDestroy(context);
	return(0);
}
