###############################################################################################
#
#	Simple Makefile for an SWLib project
#
###############################################################################################

#---------------------------------------------------------------------------
#	build configuration
#---------------------------------------------------------------------------

PROJECTNAME:=chan_example
PRODUCTDIR:=bin
BUILDDIR:=$(PROJECTNAME).build
SERVICE_PRODUCT:=example_publisher
CLIENT_PRODUCT:=example_subscriber

INCLUDE:=$(addprefix -I, $(shell find ../../src -type d))
LIBS := ../../bin/libblitz.a

ifeq ($(BUILD_TYPE), debug)
	FLAGS=-DDEBUG
	RFLAGS=-g -O0 -msse4
else
	RFLAGS=-O2 -msse4
endif
#---------------------------------------------------------------------------
#	Discover source files and infer obj and dep files
#---------------------------------------------------------------------------

SERVICE_SOURCES := src/publisher_example.cpp
SERVICE_OBJS:=$(addprefix $(BUILDDIR)/,$(patsubst %.cpp,%.o,$(patsubst %.mm,%.o,$(notdir $(SERVICE_SOURCES)))))
SERVICE_DEPS:=$(SERVICE_OBJS:%.o=%.d)

CLIENT_SOURCES := src/subscriber_example.cpp
CLIENT_OBJS:=$(addprefix $(BUILDDIR)/,$(patsubst %.cpp,%.o,$(patsubst %.mm,%.o,$(notdir $(CLIENT_SOURCES)))))
CLIENT_DEPS:=$(CLIENT_OBJS:%.o=%.d)


#BEGIN_USER_SECTION
#---------------------------------------------------------------------------
#	User persistent section
#	Here you can add commands that will not be modified by swupdate
#---------------------------------------------------------------------------

#END_USER_SECTION

#---------------------------------------------------------------------------
#	Main rules
#---------------------------------------------------------------------------

all : $(PRODUCTDIR)/$(SERVICE_PRODUCT) $(PRODUCTDIR)/$(CLIENT_PRODUCT)

debug:
	BUILD_TYPE=debug make all

$(PRODUCTDIR)/$(SERVICE_PRODUCT) : $(SERVICE_OBJS) | $(PRODUCTDIR)
	clang++ $(RFLAGS) -o $(PRODUCTDIR)/$(SERVICE_PRODUCT) $(SERVICE_OBJS)  $(LIBS)

$(PRODUCTDIR)/$(CLIENT_PRODUCT) : $(CLIENT_OBJS) | $(PRODUCTDIR)
	clang++ $(RFLAGS) -o $(PRODUCTDIR)/$(CLIENT_PRODUCT) $(CLIENT_OBJS)  $(LIBS)

clean :
	rm -r $(BUILDDIR)
	rm -r $(PRODUCTDIR)

run : all
	$(PRODUCTDIR)/$(PRODUCT)

#---------------------------------------------------------------------------
#	Rules to make build and product directories
#---------------------------------------------------------------------------

$(BUILDDIR) :
	mkdir $(BUILDDIR)

$(PRODUCTDIR) :
	mkdir $(PRODUCTDIR)

#---------------------------------------------------------------------------
#	Pattern Rules to make obj files
#---------------------------------------------------------------------------

vpath %.cpp $(dir $(wildcard src/*/))
vpath %.mm $(dir $(wildcard src/*/))

$(BUILDDIR)/%.o : %.cpp
	clang++ $(RFLAGS) -arch x86_64 $(FLAGS) -o $@ $(INCLUDE) -c $<

$(BUILDDIR)/%.o : %.mm
	clang++ $(RFLAGS) -arch x86_64 $(FLAGS) -o $@ $(INCLUDE) -c $<

#---------------------------------------------------------------------------
#	Automatic dependency requisites
#---------------------------------------------------------------------------

df = $(BUILDDIR)/$(*F)

MAKEDEPEND = clang -M $(CPPFLAGS) $(INCLUDE) $(SDKDIRS) $(FLAGS) -o $(df).Td $<

include $(CLIENT_DEPS)
include $(SERVICE_DEPS)

$(BUILDDIR)/%.d : %.cpp | $(BUILDDIR)
	$(MAKEDEPEND)
	sed 's,\($*\)\.o[ :]*,$(BUILDDIR)/\1.o $@ : ,g' < $(df).Td > $(df).d;
	rm -f $(df).Td

$(BUILDDIR)/%.d : %.mm | $(BUILDDIR)
	$(MAKEDEPEND)
	sed 's,\($*\)\.o[ :]*,$(BUILDDIR)/\1.o $@ : ,g' < $(df).Td > $(df).d;
	rm -f $(df).Td
