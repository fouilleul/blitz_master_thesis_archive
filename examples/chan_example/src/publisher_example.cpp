/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: test_directory.cpp
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*****************************************************************/

#include<unistd.h>	// sleep()
#include<signal.h>
#include<string.h>
#include<stdio.h>
#include<assert.h>

#include"blitz.h"

bool _mainLoopRun = true;

void SignalHandler(int sig)
{
	_mainLoopRun = false;
}

const char* DEFAULT_PATH = "/channel/greeting";

int main(int argc, char** argv)
{
	//NOTE(martin): install a dummy signal handler, so that we can break blocking io when receiving SIGINT

	struct sigaction sa = {};
	sa.sa_handler = SignalHandler;
	sa.sa_flags =  0;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGINT, &sa, 0);

	const char* path = argc > 1 ? argv[1] : DEFAULT_PATH;

	//NOTE(martin): init the blitz context and register our service

	blitz_context* context = BlitzContextCreate();
	publisher_link* link = BlitzOpenPublisher(context, path);

	while(_mainLoopRun)
	{
		BlitzPublish(link, BlitzTimestamp(context), "/hello", "s", "Hello, world !");
		sleep(1);
	}

	BlitzClosePublisher(context, link);
	BlitzContextDestroy(context);

	return(0);
}
