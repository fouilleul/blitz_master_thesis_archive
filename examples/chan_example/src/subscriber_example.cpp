/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: test_client.cpp
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*****************************************************************/

#include<unistd.h>	// sleep()
#include<signal.h>
#include<string.h>
#include<stdio.h>
#include<assert.h>

#include"directory.h"
#include"blitz.h"

void MyCallback(const char* address,
		uint64 timetag,
		osc_element* element,
		void* userPointer)
{
	printf("time = %f : ", TimestampToSeconds(timetag));
	OscElementPrint(element);
}

bool _mainLoopRun = true;

void SignalHandler(int sig)
{
	_mainLoopRun = false;
}

const char* DEFAULT_CHANNEL = "/channel/greeting";

int main(int argc, char** argv)
{
	//NOTE(martin): install a dummy signal handler, so that we can break blocking io when receiving SIGINT

	struct sigaction sa = {};
	sa.sa_handler = SignalHandler;
	sa.sa_flags =  0;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGINT, &sa, 0);

	const char* channel = argc > 1 ? argv[1] : DEFAULT_CHANNEL;

	blitz_context* context = BlitzContextCreate();

	subscriber_link* link = BlitzSubscribe(context, channel, MyCallback, 0);
	if(!link)
	{
		printf("Could not find the channel link at \"%s\"\n", channel);
		return(-1);
	}

	while(_mainLoopRun)
	{
		sleep(1);
	}
	BlitzUnsubscribe(context, link);
	BlitzContextDestroy(context);
	return(0);
}
