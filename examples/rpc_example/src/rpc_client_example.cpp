/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: test_client.cpp
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*****************************************************************/

#include<unistd.h>	// sleep()
#include<string.h>
#include<stdio.h>
#include<assert.h>

#include"debug_log.h"
#include"directory.h"
#include"blitz.h"

int MyCallback(rpc_handle* handle, rpc_status status, osc_element* reply, void* userPointer)
{
	switch(status)
	{
		case RPC_STATUS_OK:
			printf("received asynchronous RPC reply : \n");
			OscElementPrint(reply);
		break;

		case RPC_STATUS_FAIL:
			printf("MyCallback : RPC call failed...\n");
		break;

		case RPC_STATUS_PENDING:
			printf("Error : Callback called on pending RPC...\n");
		break;
	}

	BlitzRPCCloseHandle(handle);
	return(0);
}

int main(int argc, char** argv)
{

	blitz_context* context = BlitzContextCreate();

	rpc_link* link = BlitzRPCOpenLink(context, "/hello/world");
	if(!link)
	{
		printf("Could not find the RPC link at \"/hello/world\"\n");
		return(-1);
	}

	// BlitzRPCLinkSetCallback(link, MyCallback, 0);

	const int buffSize = 1024;
	char buffer[buffSize];
	char reqBuffer[buffSize];
	osc_element elt;
	OscFormat(&elt, buffSize, reqBuffer, "/get", "sf", "test msg", 3.14159);

	printf("request is : ");
	OscElementPrint(&elt);

	for(int i=0; i<100; i++)
	{
	#if 0
		rpc_handle* handle = BlitzRPCCallAsync(link, &elt, buffSize, buffer);

		if(!handle)
		{
			printf("BlitzRPCLinkCallAsync couldn't create call handle\n");
		}

		//NOTE(martin): actively poll result
		osc_element reply;
		rpc_status status;
		while((status = BlitzRPCPollResult(handle, &reply)) == RPC_STATUS_PENDING)
		{}
	#else
		osc_element reply;
		rpc_status status = BlitzRPCCall(link, &elt, &reply, buffSize, buffer);
	#endif

		if(status == RPC_STATUS_FAIL)
		{
			printf("RPC call failed\n");
		}
		else
		{
			printf("RPC reply : ");
			OscElementPrint(&reply);
		}
		sleep(1);
	}
	BlitzRPCCloseLink(context, link);
	BlitzContextDestroy(context);
	return(0);
}
