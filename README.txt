This repository is a support repository for my master thesis:

A middleware for handling temporal interactions in performing arts (2019)

It contains prototype implementations of the ideas proposed in this thesis. Since it should stay on sync with the document,
IT WILL LIKELY NOT BE UPDATED. Development of the BLITz middleware will continue, but not in this repository.


Building
--------

cd /path/to/blitz/directory
make				# build libblitz and blitzd
make examples			# build the examples and the max external


Debug build
-----------

make debug	# build the project with all debug messages and asserts on


Building the osc benchmarks
---------------------------

cd ./benchmarks/osc

You will need to download:

- liblo version 0.30 from http://liblo.sourceforge.net into ./benchmarks/osc/liblo-0.30
- libo (commit 71655d8ed36f55f2814adade619ec7ba4b39bec2) from https://github.com/CNMAT/libo into libo-master
- oscpack version 1.1.0 from https://github.com/CNMAT/libo into oscpack_1_1_0
- rtosc (commit d54496f13c08ee511995516bdd10ce85da453f1a) from https://github.com/fundamental/rtosc into rtosc-master

and run 'make'


Building the ltp benchmarks
---------------------------

cd ./benchmarks/osc
make
