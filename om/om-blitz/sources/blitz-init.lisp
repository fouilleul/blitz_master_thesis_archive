;============================================================================
; OM-BLITZ
;============================================================================
;
;   This program is free software. For information on usage
;   and redistribution, see the "LICENSE" file in this distribution.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;
;============================================================================


(in-package :cl-user)

(defpackage :blitz
  (:use :common-lisp :cl-user))

(defun load-libblitz ()
  (let* ((omblitz-path (om::mypathname (om::find-library "om-blitz")))
         (dylib-path (merge-pathnames
                  "bin/libblitz.dylib"
                  (make-pathname :directory (butlast (pathname-directory omblitz-path) 2)))))
    (om-fi::om-load-foreign-library
           "LIBBLITZ"
           `((:macosx ,dylib-path)
             (t (:default "libblitz"))))
    ))


;; load now
(load-libblitz)

;; load at OM startup
;; #+macosx(om-fi::add-foreign-loader 'load-libblitz)

;;; to see Blitz outputs on Terminal:
; (listen *terminal-io*)

(push :blitz *features*)
