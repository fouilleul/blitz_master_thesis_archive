

(in-package :om)


(defmethod! blitz-service (host service)
  :indoc '("host (IP address)" "service path")
  :outdoc '("received message" "client")
  :initvals '("127.0.0.1" "/")
  :doc "A Blitz service.

This box presents a <service> to the Blitz framework running on <host>.

When the service is running, the box value is updated with decoded OSC messages by Blitz calls and requests, and a reactive notification is triggered."
  :numouts 2
  t)


(defmethod! blitz-reply (client reply)
  :icon 'osc
  :indoc '("a Blitz client (as produced by blitz-service's 2nd output)" "OSC messages")
  :initvals '(nil nil)
  :doc "A Blitz service reply to a client.

The Reply must be a list of OSC messages formatted as lists (\"/address\" arg1 arg2 ...)
"
  (blitz::blitz-rpc-reply client reply))


;;; The behaviour of Blitz servioce requires a special type of box:
(defclass BlitzBox (OMReceiveBox)
  ((context :accessor context :initform nil)
   (service :accessor service :initform nil)
   (id-cstr :accessor id-cstr :initform nil)))

(defmethod box-draw ((self BlitzBox) (frame OMBoxFrame))
  (let ((y (/ (h frame) 2)))
    (om-with-line-size 2
      (om-draw-circle 12 y 6 :fill nil)
      (when (state self)
        (om-with-fg-color (om-def-color :dark-red)
          (om-draw-circle 12 y 4 :fill t)))
      t)))

(defmethod set-foreign-id ((self BlitzBox) str)
  (free-foreign-id self)
  (setf (id-cstr self) (cffi:foreign-string-alloc str)))

(defmethod free-foreign-id ((self BlitzBox))
  (when (and (id-cstr self)
             (not (cffi-sys:null-pointer-p (id-cstr self))))
    (cffi:foreign-string-free (id-cstr self)))
  (setf (id-cstr self) nil))

(defmethod omng-delete :before ((self BlitzBox))
  (stop-box self)
  (free-foreign-id self))


;;; Attach box to theblitz-service object
(defmethod boxclass-from-function-name ((self (eql 'blitz-service))) 'BlitzBox)



;;; Triggered when the box is activated
(defmethod start-box ((self BlitzBox))

  (when (state self) (stop-box self))

  (let* ((box-args (mapcar 'omng-box-value (inputs self)))
         (service (second box-args))
         (host (first box-args)))

    (om-print-format "Starting Blitz service ~A on ~A" (list service host) "OM-BLITZ")

    (setf (context self) (blitz::BlitzContextCreate))

    (unless (cffi-sys:null-pointer-p (context self))
      (set-foreign-id self service)
      (setf (service self)
            (blitz::blitz-open-service (context self) service (id-cstr self))))

    (om-print-format "=> ~A / ~A" (list (context self) (service self)) "OM-BLITZ")

    ))

;;; Triggered when the box is stopped
(defmethod stop-box ((self BlitzBox))

  (when (and (context self) (not (cffi-sys:null-pointer-p (context self))))

    (when (and (service self) (not (cffi-sys:null-pointer-p (service self))))
      (om-print-format "Blitz stopping service ~A" (list (service self)) "OM-BLITZ")
      (blitz::BlitzRPCcloseservice (context self) (service self))
      (setf (service self) nil))

    (om-print-format "Blitz destroying context ~A" (list (context self)) "OM-BLITZ")
    (blitz::BlitzContextDestroy (context self))
    (setf (context self) nil))
  )



;;;===================================
;;; REPLY TO CLIENT REQUEST
;;;===================================

;;; Finds all Blitz-service boxes with a given path
(defun find-blitz-boxes (path)
  (let ((boxes (find-boxes 'blitz-service)))
    (remove-if-not
     #'(lambda (b)
         (string-equal (omng-box-value (second (inputs b))) path))
     boxes)))


;;; Redefine the default Blitz callback
(defun blitz::blitz-process-callback (client message userpointer)
  (let ((id (cffi:foreign-string-to-lisp userpointer)))
    (om-print-format "Received from ~A for ~A : ~A" (list client id message) "OM-BLITZ")
    (let ((boxes (find-blitz-boxes id)))
      (loop for b in boxes do
            (set-delivered-value b message client)))
    message))



;;;===================================
;;; Subscriber
;;;===================================

(defmethod! blitz-subscriber (path)
  :indoc '("channel path")
  :outdoc '("received message")
  :initvals '("/")
  :doc "A Blitz service.

This box subscribes to a Blitz channel.

"
  :numouts 1
  t)

;;; The behaviour of Blitz servioce requires a special type of box:
(defclass BlitzSubscriberBox (OMReceiveBox)
  ((context :accessor context :initform nil)
   (path :accessor path :initform nil)
   (subscriber :accessor subscriber :initform nil)
   (id-cstr :accessor id-cstr :initform nil)))

(defmethod box-draw ((self BlitzSubscriberBox) (frame OMBoxFrame))
  (let ((y (/ (h frame) 2)))
    (om-with-line-size 2
      (om-draw-circle 12 y 6 :fill nil)
      (when (state self)
        (om-with-fg-color (om-def-color :dark-red)
          (om-draw-circle 12 y 4 :fill t)))
      t)))

(defmethod set-foreign-id ((self BlitzSubscriberBox) str)
  (free-foreign-id self)
  (setf (id-cstr self) (cffi:foreign-string-alloc str)))

(defmethod free-foreign-id ((self BlitzSubscriberBox))
  (when (and (id-cstr self)
             (not (cffi-sys:null-pointer-p (id-cstr self))))
    (cffi:foreign-string-free (id-cstr self)))
  (setf (id-cstr self) nil))

(defmethod omng-delete :before ((self BlitzSubscriberBox))
  (stop-box self)
  (free-foreign-id self))


;;; Attach box to theblitz-service object
(defmethod boxclass-from-function-name ((self (eql 'blitz-subscriber))) 'BlitzSubscriberBox)


;;; Find blitz subscriber boxes with a given path
(defun find-blitz-subscriber-boxes (path)
  (let ((boxes (find-boxes 'blitz-subscriber)))
    (remove-if-not
     #'(lambda (b)
         (string-equal (omng-box-value (first (inputs b))) path))
     boxes)))


;;; Triggered when the box is activated
(defmethod start-box ((self BlitzSubscriberBox))

  (when (state self) (stop-box self))

  (let* ((box-args (mapcar 'omng-box-value (inputs self)))
         (path (first box-args)))

    (om-print-format "Starting Blitz Subscriber ~A" (list path) "OM-BLITZ")

    (setf (context self) (blitz::BlitzContextCreate))

    (unless (cffi-sys:null-pointer-p (context self))
      (set-foreign-id self path)
      (setf (subscriber self)
            (blitz::blitz-subscribe (context self) path (id-cstr self))))

    (om-print-format "=> ~A / ~A" (list (context self) (subscriber self)) "OM-BLITZ")

    ))

;;; Triggered when the box is stopped
(defmethod stop-box ((self BlitzSubscriberBox))

  (when (and (context self) (not (cffi-sys:null-pointer-p (context self))))

    (when (and (subscriber self) (not (cffi-sys:null-pointer-p (subscriber self))))
      (om-print-format "Blitz unsubscribe from channel ~A" (list (subscriber self)) "OM-BLITZ")
      (blitz::BlitzUnsubscribe (context self) (subscriber self))
      (setf (subscriber self) nil))

    (om-print-format "Blitz destroying context ~A" (list (context self)) "OM-BLITZ")
    (blitz::BlitzContextDestroy (context self))
    (setf (context self) nil))
  )


;;; Redefine the default Blitz subscriber callback
(defun blitz::blitz-subscriber-callback (address timetag elt userpointer)
  (let ((id (cffi:foreign-string-to-lisp userpointer)))
    (om-print-format "Received from ~A for ~A : ~A" (list address id elt) "OM-BLITZ")
    (let ((boxes (find-blitz-subscriber-boxes id)))
      (loop for b in boxes do
            (set-delivered-value b elt))
      )
    elt)
)


;;;===================================
;;; Publisher
;;;===================================

(defmethod! blitz-publisher (path msg)
  :indoc '("channel path" "message to publish")
  :initvals '("/" "/")
  :numouts 0
  :doc "A Blitz publisher.
  

This box publishes to a Blitz channel.

"
  t
)

;;; The behaviour of Blitz servioce requires a special type of box:
(defclass BlitzPublisherBox (OMGFBoxcall)
  ((state :initform nil :initarg :state :accessor state)
   (context :accessor context :initform nil)
   (path :accessor path :initform nil)
   (publisher :accessor publisher :initform nil))
)

(defmethod omng-box-value ((self BlitzPublisherBox) &optional numouts)
  (let* ((box-args (mapcar 'omng-box-value (inputs self)))
         (element (second box-args)))
    (om-print-format "test omng-box-value : ~A" (list element) "OM-BLITZ")
    (when (publisher self) (blitz::blitz-publish (publisher self) 0 element))
    )
)

(defmethod set-reactive ((box BlitzPublisherBox) val)
  (call-next-method)
  (if val (start-box box) (stop-box box))
  (setf (state box) val))

(defmethod box-draw ((self BlitzPublisherBox) (frame OMBoxFrame))
  (let ((y (/ (h frame) 2)))
    (om-with-line-size 2
      (om-draw-circle 12 y 6 :fill nil)
      (when (state self)
        (om-with-fg-color (om-def-color :dark-red)
          (om-draw-circle 12 y 4 :fill t)))
      t)))

(defmethod omng-delete :before ((self BlitzPublisherBox))
  (stop-box self))

;;; Attach box to theblitz-service object
(defmethod boxclass-from-function-name ((self (eql 'blitz-publisher))) 'BlitzPublisherBox)

;;; Triggered when the box is activated
(defmethod start-box ((self BlitzPublisherBox))

  (when (state self) (stop-box self))

  (let* ((box-args (mapcar 'omng-box-value (inputs self)))
         (path (first box-args)))

    (om-print-format "Starting Blitz Publisher ~A" (list path) "OM-BLITZ")

    (setf (context self) (blitz::BlitzContextCreate))

    (unless (cffi-sys:null-pointer-p (context self))
      (setf (publisher self) (blitz::BlitzOpenPublisher (context self) path))
      (om-print-format "=> ~A / ~A" (list (context self) (publisher self)) "OM-BLITZ"))
    )
)


;;; Triggered when the box is stopped
(defmethod stop-box ((self BlitzPublisherBox))

  (when (and (context self) (not (cffi-sys:null-pointer-p (context self))))

    (when (and (publisher self) (not (cffi-sys:null-pointer-p (publisher self))))
      (om-print-format "Blitz close publisher to channel ~A" (list (path self)) "OM-BLITZ")
      (blitz::BlitzClosePublisher (context self) (publisher self))
      (setf (publisher self) nil))

    (om-print-format "Blitz destroying context ~A" (list (context self)) "OM-BLITZ")
    (blitz::BlitzContextDestroy (context self))
    (setf (context self) nil))
)

