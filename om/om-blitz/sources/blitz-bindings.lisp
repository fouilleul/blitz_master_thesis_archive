;============================================================================
; OM-BLITZ
;============================================================================
;
;   This program is free software. For information on usage
;   and redistribution, see the "LICENSE" file in this distribution.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;
;============================================================================


(in-package :blitz)

;-------------------------------------------------------------------------------------
; foreign structs
;-------------------------------------------------------------------------------------

(cffi::defcstruct osc_msg
  (msgSize :int)
  (addressPatternSize :int)
  (typeStringSize :int)
  (argsSize :int)
  (addressPattern :string)
  (typeString :string)
  (typeStringEnd :string)
  (args :string))

(cffi::defcstruct osc_arg_iterator
  (tag :string)
  (data :string))

(cffi::defcstruct osc_element
  (size :int)
  (data :string)
  (parent :pointer)
  (msgCache :int)
  (msg (:struct osc_msg))
  (composing :int)
  (composer :pointer))

(cffi::defctype osc_msg_p (:pointer (:struct osc_msg)))
(cffi::defctype osc_arg_iterator_p (:pointer (:struct osc_arg_iterator)))
(cffi::defctype osc_element_p (:pointer (:struct osc_element)))

;-------------------------------------------------------------------------------------
; Osc Util
;-------------------------------------------------------------------------------------

(cffi::defcfun ("OscAddressPattern" OscAddressPattern) :string (msg osc_msg_p))

(cffi::defcfun ("OscBeginPacket" OscBeginPacket) :int (elt osc_element_p) (size :int) (buffer :string))
(cffi::defcfun ("OscEndPacket" OscEndPacket) :int (elt osc_element_p))

(cffi::defcfun ("OscBeginMessage" OscBeginMessage) :int (elt osc_element_p) (addressPattern :string))
(cffi::defcfun ("OscEndMessage" OscEndMessage) :int (elt osc_element_p))

(cffi::defcfun ("OscBeginBundle" OscBeginBundle) :int (elt osc_element_p) (timetag :unsigned-long-long))
(cffi::defcfun ("OscEndBundle" OscEndBundle) :int (elt osc_element_p))

; TODO : add all OscPushXXX() functions
(cffi::defcfun ("OscPushInt32" OscPushInt32) :int (msg osc_msg_p) (i :int))
(cffi::defcfun ("OscPushFloat" OscPushFloat) :int (msg osc_msg_p) (f :float))
(cffi::defcfun ("OscPushString" OscPushString) :string (msg osc_msg_p) (s :string))

; NOTE : osc_arg_iterator functions
(cffi::defcfun ("OscArgumentsCount" OscArgumentsCount) :unsigned-int (msg osc_msg_p))
(cffi::defcfun ("OscArgumentCreate" OscArgumentCreate) osc_arg_iterator_p (msg osc_msg_p))
(cffi::defcfun ("OscArgumentDestroy" OscArgumentDestroy) :void (arg osc_arg_iterator_p))
(cffi::defcfun ("OscArgumentIsEqual" OscArgumentIsEqual) :int (lhs osc_arg_iterator_p) (rhs osc_arg_iterator_p))
(cffi::defcfun ("OscArgumentType" OscArgumentType) :char (arg osc_arg_iterator_p))
(cffi::defcfun ("OscArgumentNext" OscArgumentNext) osc_arg_iterator_p (arg osc_arg_iterator_p))

; TODO : add all OscAsXXXUnchecked() functions
(cffi::defcfun ("OscAsInt32Unchecked" OscAsInt32Unchecked) :int (arg osc_arg_iterator_p))
(cffi::defcfun ("OscAsFloatUnchecked" OscAsFloatUnchecked) :float (arg osc_arg_iterator_p))
(cffi::defcfun ("OscAsStringUnchecked" OscAsStringUnchecked) :string (arg osc_arg_iterator_p))

(cffi::defcfun ("OscMessageData" OscMessageData) :pointer (msg osc_msg_p))
(cffi::defcfun ("OscMessageSize" OscMessageSize) :int (msg osc_msg_p))

(cffi::defcfun ("OscElementData" OscElementData) :pointer (elt osc_element_p))
(cffi::defcfun ("OscElementSize" OscElementSize) :unsigned-int (elt osc_element_p))

(cffi::defcfun ("OscParsePacket" OscParsePacket) :int (element osc_element_p) (size :int) (buffer :string))
(cffi::defcfun ("OscParseMessage" OscParseMessage) :int (msg osc_msg_p) (size :int) (buffer :string))

(cffi::defcfun ("OscElementIteratorCreate" OscElementIteratorCreate) osc_element_p (bundle osc_element_p))
(cffi::defcfun ("OscElementIteratorDestroy" OscElementIteratorDestroy) :void (element osc_element_p))

(cffi::defcfun ("OscIsAtEndOfBundle" OscIsAtEndOfBundle) :int (bundle osc_element_p) (elt osc_element_p))
(cffi::defcfun ("OscElementNext" OscElementNext) osc_element_p (elt osc_element_p))
(cffi::defcfun ("OscElementAsMessage" OscElementAsMessage) :int (element osc_element_p) (msg (:pointer osc_msg_p)))
(cffi::defcfun ("OscElementAsMessageUnchecked" OscElementAsMessageUnchecked) osc_msg_p (element osc_element_p))
(cffi::defcfun ("OscElementIsBundle" OscElementIsBundle) :int (elt osc_element_p))

; Osc/Lisp conversion


(defun list-to-blitz-msg (lisp-msg blitz-elt buffer size)
  (OscBeginPacket blitz-elt size buffer)
  (OscBeginMessage blitz-elt (first lisp-msg))
  (loop for arg in (rest lisp-msg) do
        (cond ((integerp arg)
               (OscPushInt32 blitz-elt arg))
                ((floatp arg)
                 (oscpushfloat blitz-elt arg))
                ((stringp arg)
                 (OscPushString blitz-elt arg))
                (t (print (format nil "unknow type: ~A" arg)))))
    (OscEndMessage blitz-elt)
    (OscEndPacket blitz-elt)
    blitz-elt
)


(defun list-to-blitz-bundle-recursive (lisp-elt blitz-elt)
  (if (typep (first lisp-elt) 'string) ;;; lisp-elt is a single OSC-message
      (let* ((lisp-msg lisp-elt))
         (OscBeginMessage blitz-elt (first lisp-msg))    
         (loop for arg in (rest lisp-msg) do
               (cond ((integerp arg)
                      (OscPushInt32 blitz-elt arg))
                     ((floatp arg)
                      (oscpushfloat blitz-elt arg))
                     ((stringp arg)
                      (OscPushString blitz-elt arg))
                     (t (print (format nil "unknow type: ~A" arg)))))
         (OscEndMessage blitz-elt)
         blitz-elt)

    (let ()  ;;; else: lisp-elt is a list of messages   
      (OscBeginBundle blitz-elt 0)   ; we set timetag to zero for now    
      (loop for nested in lisp-elt do
            (list-to-blitz-bundle-recursive nested blitz-elt))
      (OscEndBundle blitz-elt)
      blitz-elt)
    )
)

(defun list-to-blitz-elt (lisp-elt blitz-elt buffer size)
 
  (if (typep (first lisp-elt) 'string) ;;; lisp-elt is an OSC-message
      
      (let ((lisp-msg lisp-elt))
        (OscBeginPacket blitz-elt size buffer)
         (OscBeginMessage blitz-elt (first lisp-msg))
         (loop for arg in (rest lisp-msg) do
               (cond ((integerp arg)
                      (OscPushInt32 blitz-elt arg))
                     ((floatp arg)
                      (oscpushfloat blitz-elt arg))
                     ((stringp arg)
                      (OscPushString blitz-elt arg))
                     (t (print (format nil "unknow type: ~A" arg)))))
         (OscEndMessage blitz-elt)
         (OscEndPacket blitz-elt)
         blitz-elt
         )
    
    (progn  ;;; else: messages is a list of messages
      
      (OscBeginPacket blitz-elt buffer size)
      (OscBeginBundle blitz-elt 0)    ;; timetag at 0 for now...
      (loop for nested in lisp-elt do
           (list-to-blitz-bundle-recursive nested blitz-elt))
      (OscEndBundle blitz-elt)
      (OscEndPacket blitz-elt)
      blitz-elt)
    )
)


(defun decode-argument (arg)
  (case (code-char (OscArgumentType arg))
    (#\f (OscAsFloatUnchecked arg))
    (#\d (OscAsDoubleUnchecked arg))
    (#\i (OscAsInt32Unchecked arg))
    (#\s (OscAsStringUnchecked arg))
    (#\T t) (#\F nil)
    ; TODO(martin): complete types
    (otherwise :unknown-osc-type))
)

(defun blitz-msg-to-list (blitz-msg)
  (let  ((arg (OscArgumentCreate blitz-msg))
        (argCount (OscArgumentsCount blitz-msg)))

    (let ((ret (cons (OscAddressPattern blitz-msg)
          (loop for i from 0 to (- argCount 1) collect
               (let ((value (decode-argument arg)))
                   (OscArgumentNext arg)
                   value)
          )
          )))
    (OscArgumentDestroy arg)
    ret
    )
  )
)

(defun blitz-elt-to-list (blitz-elt)
  (if (eq (OscElementIsBundle blitz-elt) 1)

      (let* ((it (OscElementIteratorCreate blitz-elt))
            (elts (loop while (eq (OscIsAtEndOfBundle blitz-elt it) 0) collect
              (let  ((elt (blitz-elt-to-list it)))
                    (OscElementNext it)
                    elt)
              )))
        (OscElementIteratorDestroy it)
        elts)
      
    (let ((blitz-msg (OscElementAsMessageUnchecked blitz-elt)))
       (let ((l (blitz-msg-to-list blitz-msg)))
         l)))
)

;; Test blitz to/from list conversion
;(cffi::with-foreign-objects ((bundle '(:struct osc_element)) (buffer '(:char) 1024)) (blitz-elt-to-list (list-to-blitz-elt '(("/abc" 1 2)(("/def" 3 4)("/ghi" 5 6))("/jkl" 7 8)) bundle 1024 buffer))) 

;-------------------------------------------------------------------------------------
; Socket Utilities
;-------------------------------------------------------------------------------------
(cffi::defcfun ("StringToNetIP" StringToNetIP) :int (address :string))

;-------------------------------------------------------------------------------------
; Context functions
;-------------------------------------------------------------------------------------
(cffi::defcfun ("BlitzContextCreate" BlitzContextCreate) :pointer)
(cffi::defcfun ("BlitzContextDestroy" BlitzContextDestroy) :int (context :pointer))

;-------------------------------------------------------------------------------------
; RPC service functions
;-------------------------------------------------------------------------------------

(cffi::defcfun ("BlitzRPCOpenService" BlitzRPCOpenService) :pointer (context :pointer) (path :string) (callback :pointer) (userPointer :pointer))
(cffi:defcfun ("BlitzRPCCloseService" BlitzRPCCloseService) :int (context :pointer) (service :pointer))
(cffi:defcfun ("BlitzRPCReply" BlitzRPCReply) :int (client :pointer) (msg osc_msg_p))


(cffi::defcallback RPCServiceCallback :int ((client :pointer) (bundle osc_element_p) (userPointer :pointer))
  (if (fboundp 'blitz-process-callback)
      (blitz-process-callback client (blitz-elt-to-list bundle) userPointer)
    (print "warning: blitz-process-callback function undefined!!"))
  0)

;;; redefined this !
; (defun blitz-process-callback (client bundle userPointer) nil)

(defun blitz-open-service (context service pointer)
  (blitz::BlitzRPCOpenService context service
                              (cffi:get-callback 'RPCServiceCallback)
                              (or pointer (cffi-sys:null-pointer))
                              ))


(defun blitz-rpc-reply (client reply)

  (let* ((blitz-reply (cffi:foreign-alloc '(:struct osc_element)))
         (buffer-size 4096)
         (buffer (cffi::foreign-alloc :char :count buffer-size)))

    (unwind-protect
        (BlitzRPCReply client (list-to-blitz-elt reply blitz-reply buffer buffer-size))

      (cffi::foreign-free buffer)
      (cffi::foreign-free blitz-reply))

    ))

; TODO : close service !!

;-------------------------------------------------------------------------------------
; RPC link functions
;-------------------------------------------------------------------------------------

(cffi::defcfun ("BlitzRPCOpenLink" BlitzRPCOpenLink) :pointer (context :pointer) (path :string))
(cffi::defcfun ("BlitzRPCCloseLink" BlitzRPCCloseLink) :int (context :pointer) (link :pointer))

(cffi::defcfun ("BlitzRPCCall" BlitzRPCCall) :int (link :pointer) (request osc_msg_p) (reply-max-size :pointer) (reply-buffer :pointer))

;TODO: async call functions


;-------------------------------------------------------------------------------------
; Publisher functions
;-------------------------------------------------------------------------------------

(cffi::defcfun ("BlitzOpenPublisher" BlitzOpenPublisher) :pointer (context :pointer) (path :string))
(cffi::defcfun ("BlitzClosePublisher" BlitzClosePublisher) :int (context :pointer) (publisher :pointer))
(cffi::defcfun ("BlitzPublishElement" BlitzPublishElement) :int (publisher :pointer) (timetag :unsigned-long-long) (elt osc_element_p))

(defun blitz-publish (publisher timetag osc-list)
  (let* ((size 4096)
         (buffer (cffi::foreign-alloc :char :count size))
         (blitz-elt (cffi::foreign-alloc '(:struct osc_element))))

    (list-to-blitz-elt osc-list blitz-elt buffer size)
    (BlitzPublishElement publisher timetag blitz-elt)
    (cffi::foreign-free buffer)
    (cffi::foreign-free blitz-elt))
)

;-------------------------------------------------------------------------------------
; Subscriber functions
;-------------------------------------------------------------------------------------

(cffi::defcfun ("BlitzSubscribe" BlitzSubscribe) :pointer (context :pointer) (path :string) (callback :pointer) (userPointer :pointer))
(cffi::defcfun ("BlitzUnsubscribe" BlitzUnsubscribe) :int (context :pointer) (subscriber :pointer))

(cffi::defcallback ChannelSubscriberCallback :void ((address :string) (timetag :unsigned-long-long) (elt osc_element_p) (userPointer :pointer))
  (if (fboundp 'blitz-subscriber-callback)
      (blitz-subscriber-callback address timetag (blitz-elt-to-list elt) userPointer)
    (print "warning: blitz-subscriber-callback function undefined!!"))
  0)

;;; redefined this !
; (defun blitz-subscriber-callback (address timetag elt userPointer) nil)

(defun blitz-subscribe (context path pointer)
  (blitz::BlitzSubscribe context path
                              (cffi:get-callback 'ChannelSubscriberCallback)
                              (or pointer (cffi-sys:null-pointer))
                              ))

