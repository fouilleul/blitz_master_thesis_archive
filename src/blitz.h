/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: blitz.h
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*****************************************************************/
#ifndef __BLITZ_H_
#define __BLITZ_H_

#include"typedefs.h"		// sized types
#include"platform_socket.h"	// net_ip, etc...
#include"platform_thread.h"
#include"platform_time.h"
#include"user_clock.h"
#include"osc.h"			// osc_msg


#ifdef __cplusplus
extern "C" {
#endif	// __cplusplus

//------------------------------------------------------------------------------------------
// Blitz context functions & low-level API
//------------------------------------------------------------------------------------------

struct blitz_context;

blitz_context* BlitzContextCreate();
int BlitzContextDestroy(blitz_context* context);
int BlitzConnect(blitz_context* context);
int BlitzDisconnect(blitz_context* context);
int BlitzDirectoryRequest(blitz_context* context, osc_element* request, osc_msg* reply);

u64 BlitzTimestamp(blitz_context* context);

//------------------------------------------------------------------------------------------
// RPC API
//------------------------------------------------------------------------------------------

struct rpc_service;
struct rpc_client;

struct rpc_link;
struct rpc_handle;

typedef int8 rpc_status;
const rpc_status RPC_STATUS_OK	     = 0,  // The call was dispatched an returned no error
		 RPC_STATUS_PENDING  = 1,  // The call is still pending
		 RPC_STATUS_BLOCKING = 2,  // Used in blocking calls (reserved)
		 RPC_STATUS_MEM	     = 3,  // The call was dispatched but the return value does not fit in the resultBuffer
		 RPC_STATUS_FAIL     = -1; // We failed to dispatch the call

typedef int (*RPCServiceCallback)(rpc_client*, osc_element*, void*);
typedef void (*RPCLinkCallback)(rpc_handle*, rpc_status status, osc_element*, void*);

//------------------------------------------------------------------------------------------
// RPC Service API (service side)
//------------------------------------------------------------------------------------------

rpc_service* BlitzRPCOpenService(blitz_context* context, const char* path, RPCServiceCallback callback, void* userPointer);
int BlitzRPCCloseService(blitz_context* context, rpc_service* service);

int BlitzRPCReply(rpc_client* client, osc_element* msg);

//------------------------------------------------------------------------------------------
// RPC Link API (client side)
//------------------------------------------------------------------------------------------

rpc_link* BlitzRPCOpenLink(blitz_context* context, const char* path);
int BlitzRPCCloseLink(blitz_context* context, rpc_link* link);
int BlitzRPCLinkSetCallback(rpc_link* link, RPCLinkCallback callback, void* userPointer);
int BlitzRPCCloseHandle(rpc_handle* handle);

//TODO(martin): we could pass a preinitialized osc_msg* reply & no replySize/replyBuffer

rpc_status BlitzRPCCall(rpc_link* link, osc_element* request, osc_element* reply, uint32 maxSize, char* replyBuffer);
rpc_handle* BlitzRPCCallAsync(rpc_link* link, osc_element* request, uint32 replyMaxSize, char* replyBuffer);
rpc_status BlitzRPCPollResult(rpc_handle* handle, osc_element* reply);

//------------------------------------------------------------------------------------------
// Channel API structs
//------------------------------------------------------------------------------------------

struct publisher_link;
struct subscriber_link;

typedef void (*ChannelSubscriberCallback)(const char* address,
                                          uint64 timetag,
					  osc_element* elt,
					  void* userPointer);

//------------------------------------------------------------------------------------------
// Channel Service API (publisher side)
//------------------------------------------------------------------------------------------

publisher_link* BlitzOpenPublisher(blitz_context* context, const char* path);
int BlitzClosePublisher(blitz_context* context, publisher_link* publisher);

int BlitzPublish(publisher_link* link, uint64 timetag, const char* pattern, const char* fmt, ...);
int BlitzPublishArguments(publisher_link* link, uint64 timetag, const char* pattern, int argc, osc_argument* argv);
int BlitzPublishMessage(publisher_link* link, uint64 timetag, osc_msg* msg);
int BlitzPublishElement(publisher_link* link, uint64 timetag, osc_element* element);

//------------------------------------------------------------------------------------------
// Channel Link API (subscriber side)
//------------------------------------------------------------------------------------------

subscriber_link* BlitzSubscribe(blitz_context* context, const char* path, ChannelSubscriberCallback callback, void* userPointer);
int BlitzUnsubscribe(blitz_context* context, subscriber_link* subscriber);


#ifdef __cplusplus
} // extern "C"
#endif	// __cplusplus

#endif //__BLITZ_H_
