/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: posix_thread.cpp
*	@author: Martin Fouilleul
*	@date: 21/03/2019
*	@revision:
*
*****************************************************************/
#include<stdlib.h>
#include<pthread.h>

#include"platform_thread.h"

extern "C" {

struct platform_thread
{
	bool valid;
	pthread_t pthread;
};

platform_thread* ThreadCreate(ThreadStartFunction start, void* userPointer)
{
	platform_thread* thread = (platform_thread*)malloc(sizeof(platform_thread));
	if(!thread)
	{
		return(0);
	}
	if(pthread_create(&thread->pthread, 0, start, userPointer) != 0)
	{
		free(thread);
		return(0);
	}
	else
	{
		thread->valid = true;
		return(thread);
	}
}
int ThreadDestroy(platform_thread* thread)
{
	if(thread->valid)
	{
		void* res;
		pthread_cancel(thread->pthread);
		pthread_join(thread->pthread, &res);
	}
	free(thread);
	return(0);
}

int ThreadSignal(platform_thread* thread, int sig)
{
	return(pthread_kill(thread->pthread, sig));
}

int ThreadJoin(platform_thread* thread, void** ret)
{
	if(pthread_join(thread->pthread, ret))
	{
		return(-1);
	}
	thread->valid = false;
	return(0);
}


struct platform_mutex
{
	pthread_mutex_t pmutex;
};

platform_mutex* MutexCreate()
{
	platform_mutex* mutex = (platform_mutex*)malloc(sizeof(platform_mutex));
	if(!mutex)
	{
		return(0);
	}
	if(pthread_mutex_init(&mutex->pmutex, 0) != 0)
	{
		free(mutex);
		return(0);
	}
	return(mutex);
}
int MutexDestroy(platform_mutex* mutex)
{
	if(pthread_mutex_destroy(&mutex->pmutex) != 0)
	{
		return(-1);
	}
	free(mutex);
	return(0);
}

int MutexLock(platform_mutex* mutex)
{
	return(pthread_mutex_lock(&mutex->pmutex));
}

int MutexUnlock(platform_mutex* mutex)
{
	return(pthread_mutex_unlock(&mutex->pmutex));
}


struct platform_condition
{
	pthread_cond_t pcond;
};

platform_condition* ConditionCreate()
{
	platform_condition* cond = (platform_condition*)malloc(sizeof(platform_condition));
	if(!cond)
	{
		return(0);
	}
	if(pthread_cond_init(&cond->pcond, 0) != 0)
	{
		free(cond);
		return(0);
	}
	return(cond);
}
int ConditionDestroy(platform_condition* cond)
{
	if(pthread_cond_destroy(&cond->pcond) != 0)
	{
		return(-1);
	}
	free(cond);
	return(0);
}
int ConditionWait(platform_condition* cond, platform_mutex* mutex)
{
	return(pthread_cond_wait(&cond->pcond, &mutex->pmutex));
}
int ConditionSignal(platform_condition* cond)
{
	return(pthread_cond_signal(&cond->pcond));
}

} // extern "C"
