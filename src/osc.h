/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: osc.h
*	@author: Martin Fouilleul
*	@date: 10/04/2019
*	@revision:
*
*****************************************************************/
#ifndef __OSC_DECODE_H_
#define __OSC_DECODE_H_

#include<string.h>
#include<stdarg.h>
#include"typedefs.h"
#include"debug_log.h"

#ifdef __cplusplus
extern "C" {
#endif	// __cplusplus


//---------------------------------------------------------------
// Endianness
//---------------------------------------------------------------

#if defined(OSC_HOST_LITTLE_ENDIAN) || defined(OSC_HOST_BIG_ENDIAN)
#elif defined(__WIN32__) || defined(WIN32) || defined(WINCE)
	#define OSC_HOST_LITTLE_ENDIAN 1
	#undef OSC_HOST_BIG_ENDIAN
#elif defined(__APPLE__)
	#if defined(__LITTLE_ENDIAN__)
		#define OSC_HOST_LITTLE_ENDIAN 1
		#undef OSC_HOST_BIG_ENDIAN
	#elif defined(__BIG_ENDIAN__)
		#define OSC_HOST_BIG_ENDIAN 1
		#undef OSC_HOST_LITTLE_ENDIAN
	#endif
#elif defined(__BYTE_ORDER__) && defined(__ORDER_LITTLE_ENDIAN__) && defined(__ORDER_BIG_ENDIAN__)
	#if (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
		#define OSC_HOST_LITTLE_ENDIAN 1
		#undef OSC_HOST_BIG_ENDIAN
	#elif (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
		#define OSC_HOST_BIG_ENDIAN 1
		#undef OSC_HOST_LITTLE_ENDIAN
	#endif
#else
	#if defined(__LITTLE_ENDIAN__) && !defined(__BIG_ENDIAN__)
		#define OSC_HOST_LITTLE_ENDIAN 1
		#undef OSC_HOST_BIG_ENDIAN
	#elif defined(__BIG_ENDIAN__) && !defined(__LITTLE_ENDIAN__)
		#define OSC_HOST_BIG_ENDIAN 1
		#undef OSC_HOST_LITTLE_ENDIAN
	#endif
#endif

#if !defined(OSC_HOST_LITTLE_ENDIAN) && !defined(OSC_HOST_BIG_ENDIAN)
	#error Endianness not defined for your platform !
#endif


//---------------------------------------------------------------
// Osc errors
//---------------------------------------------------------------

typedef enum
{
	OSC_OK = 0,
	OSC_MISSING_ARG,
	OSC_WRONG_TYPE,
	OSC_INVALID_SIZE,
	OSC_INVALID_ADDRESS,
	OSC_INVALID_TYPE_STRING,
	OSC_UNKNOWN_TYPE,
	OSC_UNMATCHED_ARRAY,
	OSC_ARG_EXCEED_MSG,
	OSC_BAD_STRING,
	OSC_BUFFER,
	OSC_MEM,
	OSC_INVALID_MARKER,
	OSC_NOT_BUNDLE,
	OSC_NOT_MESSAGE,

	OSC_UNMATCHED_PACKET_END,
	OSC_UNMATCHED_MESSAGE_END,
	OSC_UNMATCHED_BUNDLE_END
} OscErrCode;

const char* OscGetErrorMessage(OscErrCode err);
#define OscPrintError(err) ERROR_PRINTF("%s\n", OscGetErrorMessage(err))

//---------------------------------------------------------------
// Osc types, tags and constants
//---------------------------------------------------------------

const int32 OSC_INT32_MAX               = 0x7FFFFFFF,
            OSC_BUNDLE_ELEMENT_SIZE_MAX = 0x7FFFFFFC,
	    OSC_MIN_BUNDLE_SIZE		= 20;

typedef enum
{
	OSC_SIZEOF_INT32  = 4,
	OSC_SIZEOF_UINT32 = 4,
	OSC_SIZEOF_INT64  = 8,
	OSC_SIZEOF_UINT64 = 8,
} OscTypeSize;

typedef enum
{
	TRUE_TYPE_TAG         = 'T',
	FALSE_TYPE_TAG        = 'F',
	NIL_TYPE_TAG          = 'N',
	INFINITUM_TYPE_TAG    = 'I',
	INT16_TYPE_TAG        = 'W',
	INT32_TYPE_TAG        = 'i',
	INT64_TYPE_TAG        = 'h',
	FLOAT_TYPE_TAG        = 'f',
	DOUBLE_TYPE_TAG       = 'd',
	CHAR_TYPE_TAG         = 'c',
	STRING_TYPE_TAG       = 's',
	SYMBOL_TYPE_TAG       = 'S',
	BLOB_TYPE_TAG         = 'b',
	RGBA_COLOR_TYPE_TAG   = 'r',
	MIDI_MESSAGE_TYPE_TAG = 'm',
	TIME_TAG_TYPE_TAG     = 't',
	ARRAY_BEGIN_TYPE_TAG  = '[',
	ARRAY_END_TYPE_TAG    = ']',
	OSC_BLITZ_MARKER_TAG  = 'X',
	OSC_ELEMENT_TYPE_TAG  = 'O'
} OscTypeTag;


//NOTE(martin): ie. 'BLIT' + version number 0.0.1 in big endian
//              when we use it with OscPushInt64Variant(), the functions flips it to the correct endianness,
//		so we only define the value here, which is independant from its representation
const int64 OSC_BLITZ_MARKER_VALUE = 0x424c495400000001;


//NOTE(martin): on the other hand, we may use some markers for (faster) direct integer comparisons :
//              hence we define big/little endian versions

#ifdef OSC_BIG_ENDIAN
const int64 OSC_BUNDLE_MARKER = 0x2362756e646c6500, // "#bundle"
            OSC_BLITZ_MARKER  = 0x424c495400000001; // 'BLIT' + version number 0.0.1
#else
const int64 OSC_BUNDLE_MARKER = 0x00656c646e756223,
            OSC_BLITZ_MARKER  = 0x0100000054494c42;
#endif


//---------------------------------------------------------------
// Osc Endianness helpers
//---------------------------------------------------------------

INLINE_GEN int32 SwapInt32(int32 val)
{
	//NOTE(martin): this generates a bswapl (at least on clang)
	//		using a union would be more explicit but it doesn't seem to produce optimized code
	val = (val & 0xff000000)>>24
	     |(val & 0x00ff0000)>>8
	     |(val & 0x0000ff00)<<8
	     |(val & 0x000000ff)<<24;

	return(val);
}

INLINE_GEN void OscToNative32(const char* dst, const char* src)
{
#ifdef OSC_HOST_LITTLE_ENDIAN

	*(int32*)dst = (*(int32*)src & 0xff000000) >> 24
		      |(*(int32*)src & 0x00ff0000) >> 8
		      |(*(int32*)src & 0x0000ff00) << 8
		      |(*(int32*)src & 0x000000ff) << 24;
#else
	*(int32*)dst = *(int32*)src);
#endif
}


INLINE_GEN void OscToNative64(const char* dst, const char* src)
{
#ifdef OSC_HOST_LITTLE_ENDIAN

	*(int64*)dst = (*(int64*)src & 0xff00000000000000) >> 56
		      |(*(int64*)src & 0x00ff000000000000) >> 40
		      |(*(int64*)src & 0x0000ff0000000000) >> 24
		      |(*(int64*)src & 0x000000ff00000000) >> 8
		      |(*(int64*)src & 0x00000000ff000000) << 8
		      |(*(int64*)src & 0x0000000000ff0000) << 24
		      |(*(int64*)src & 0x000000000000ff00) << 40
		      |(*(int64*)src & 0x00000000000000ff) << 56;
#else
	*(int64*)dst = *(int64*)src);
#endif
}

//TODO(martin): check that these are effectively a bswap...
INLINE_GEN int32 OscToInt32(const char* p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	return((*(int32*)p & 0xff000000) >> 24
	       |(*(int32*)p & 0x00ff0000) >> 8
	       |(*(int32*)p & 0x0000ff00) << 8
	       |(*(int32*)p & 0x000000ff) << 24);
#else
	return(*(int32*)p);
#endif
}

INLINE_GEN uint32 OscToUInt32(const char* p)
{
	return((uint32)OscToInt32(p));
}

INLINE_GEN int16 OscToInt16(const char* p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	return((int16)((*(int32*)p & 0x000000ff) << 8
	              |(*(int32*)p & 0x0000ff00) >> 8));
#else
	return((int16)((*(int32*)p) & 0x00ff));
#endif
}

INLINE_GEN int64 OscToInt64(const char *p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN

	return( (*(int64*)p & 0xff00000000000000) >> 56
	       |(*(int64*)p & 0x00ff000000000000) >> 40
	       |(*(int64*)p & 0x0000ff0000000000) >> 24
	       |(*(int64*)p & 0x000000ff00000000) >> 8
	       |(*(int64*)p & 0x00000000ff000000) << 8
	       |(*(int64*)p & 0x0000000000ff0000) << 24
	       |(*(int64*)p & 0x000000000000ff00) << 40
	       |(*(int64*)p & 0x00000000000000ff) << 56);
#else
	return(*(int64*)p);
#endif
}

INLINE_GEN uint64 OscToUInt64(const char* p)
{
	return((uint64)OscToInt64(p));
}

INLINE_GEN float OscToFloat(const char* p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	union
	{
		float f;
		int64 i;
	} u;
	u.i = (*(int32*)p & 0xff0000000) >> 24
	      |(*(int32*)p & 0x00ff0000) >> 8
	      |(*(int32*)p & 0x0000ff00) << 8
	      |(*(int32*)p & 0x000000ff) << 24;
	return(u.f);
#else
	return(*(float*)p);
#endif
}

INLINE_GEN double OscToDouble(const char* p)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	union
	{
		double d;
		int64 i;
	} u;
	u.i = (*(int64*)p & 0xff00000000000000) >> 56
	      |(*(int64*)p & 0x00ff000000000000) >> 40
	      |(*(int64*)p & 0x0000ff0000000000) >> 24
	      |(*(int64*)p & 0x000000ff00000000) >> 8
	      |(*(int64*)p & 0x00000000ff000000) << 8
	      |(*(int64*)p & 0x0000000000ff0000) << 24
	      |(*(int64*)p & 0x000000000000ff00) << 40
	      |(*(int64*)p & 0x00000000000000ff) << 56;

	return(u.d);
#else
	return *(double*)p;
#endif
}

INLINE_GEN void OscFromInt16(char* p, int16 i)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	*((int32*)p) = ((int32)i & 0xff00) >> 8
	              |((int32)i & 0x00ff) << 8;
#else
	*((int32*)p) = ((int32)i) & (0x00ff);
#endif
}

INLINE_GEN void OscFromInt32(char* p, int32 i)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	*((int32*)p) = (i & 0xff000000) >> 24
	              |(i & 0x00ff0000) >> 8
		      |(i & 0x0000ff00) << 8
		      |(i & 0x000000ff) << 24;
#else
	*((int32*)p) = i;
#endif
}

INLINE_GEN void OscFromInt64(char* p, int64 i)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	*((int64*)p) = (i & 0xff00000000000000) >> 56
	              |(i & 0x00ff000000000000) >> 40
		      |(i & 0x0000ff0000000000) >> 24
		      |(i & 0x000000ff00000000) >> 8
		      |(i & 0x00000000ff000000) << 8
		      |(i & 0x0000000000ff0000) << 24
		      |(i & 0x000000000000ff00) << 40
		      |(i & 0x00000000000000ff) << 56;
#else
	*((int64*)p) = i;
#endif
}

INLINE_GEN void OscFromFloat(char* p, float f)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	union
	{
		float f;
		int32 i;
	} u;
	u.f = f;

	*((int32*)p) = (u.i & 0xff000000) >> 24
		      |(u.i & 0x00ff0000) >> 8
	              |(u.i & 0x0000ff00) << 8
	              |(u.i & 0x000000ff) << 24;
#else
	*((float*)p) = f;
#endif
}

INLINE_GEN void OscFromDouble(char* p, double d)
{
#ifdef OSC_HOST_LITTLE_ENDIAN
	union
	{
		double d;
		int64 i;
	} u;
	u.d = d;
	*((int64*)p) = (u.i & 0xff00000000000000) >> 56
	              |(u.i & 0x00ff000000000000) >> 40
		      |(u.i & 0x0000ff0000000000) >> 24
		      |(u.i & 0x000000ff00000000) >> 8
		      |(u.i & 0x00000000ff000000) << 8
		      |(u.i & 0x0000000000ff0000) << 24
		      |(u.i & 0x000000000000ff00) << 40
		      |(u.i & 0x00000000000000ff) << 56;
#else
	*((double*)p) = d;
#endif
}

//---------------------------------------------------------------
// Size & format checks
//---------------------------------------------------------------

INLINE_GEN bool OscIsMultipleOf4(int32 x)
{
    return((x & ((int32)0x03)) == 0);
}

INLINE_GEN uint32 OscRoundUp4(uint32 x)
{
    return((x + 3) & ~((uint32)0x03));
}

INLINE_GEN bool OscIsValidElementSize(int32 size)
{
	return(size >= 0 && size <= OSC_BUNDLE_ELEMENT_SIZE_MAX);
}

//TODO(martin): this function can miss unaligned '\0' characters
//		which could cause inconsistent behaviour !
//		Maybe rename to OscFindStr4EndUnsafe and use it only when we know for sure that the string is valid.

INLINE_GEN const char* OscFindStr4End(const char *p, const char *end)
{
	//NOTE(martin): return the first 4 byte boundary after the first null char of str4,
	//              without scanning further than end (return 0 if no aligned null byte is found in the valid range)
	DEBUG_ASSERT(p < end);
	p += 3;

	//NOTE(martin): the loop inspect the string at each 4*n-1 index to detect a null char.
	//              the loop is unrolled with a factor 2 for performance reasons
	//		NB : This loop _can_ poke passed the end, but since we never poke passed the 16 bytes aligned block in which
	//		the last valid byte of the buffer is stored, we won't trigger a page fault.

	const char* lastBlockStart = (const char*)((int64)(end-1) & ~0x03);
	while(p < lastBlockStart)
	{
		if(*p == 0)
		{
			return(p+1);
		}
		if(p[4] == 0)
		{
			return(p < (end-4) ? p+5 : 0);
		}
		p += 8;
	}

	//NOTE(martin): continue on the last valid block with the non-unrolled loop
	while(p < end)
	{
		if(*p == 0)
		{
			return(p+1);
		}
		p += 4;
	}
	return(0);
}

INLINE_GEN uint32 OscStr4Size(const char* str)
{
	uint32 size = strlen(str) + 1;
	return(OscRoundUp4(size));
}

INLINE_GEN void OscStr4UASizes(const char* str, uint32* usize, uint32* asize)
{
	*usize = strlen(str) + 1;
	*asize = OscRoundUp4(*usize);
}

INLINE_GEN void OscStr4CopyAndPadUA(char* dst, const char* src, uint32 usize, uint32 asize)
{
	*((uint32*)(dst+asize-4)) = 0;
	memcpy(dst, src, usize);
}

INLINE_GEN void OscStr4Copy(char* dst, const char* src)
{
	uint32 usize, asize;
	OscStr4UASizes(src, &usize, &asize);
	OscStr4CopyAndPadUA(dst, src, usize, asize);
}

INLINE_GEN int32 OscGetArgSize(char tag, const char* arg)
{
	switch(tag)
	{
		case TRUE_TYPE_TAG:
		case FALSE_TYPE_TAG:
		case NIL_TYPE_TAG:
		case INFINITUM_TYPE_TAG:
		case ARRAY_BEGIN_TYPE_TAG:
		case ARRAY_END_TYPE_TAG:
			return(0);

		case INT16_TYPE_TAG:
		case INT32_TYPE_TAG:
		case FLOAT_TYPE_TAG:
		case CHAR_TYPE_TAG:
		case RGBA_COLOR_TYPE_TAG:
		case MIDI_MESSAGE_TYPE_TAG:
			return(4);

		case INT64_TYPE_TAG:
		case TIME_TAG_TYPE_TAG:
		case DOUBLE_TYPE_TAG:
		case OSC_BLITZ_MARKER_TAG:
			return(8);

		case STRING_TYPE_TAG:
		case SYMBOL_TYPE_TAG:
		{
			return(OscStr4Size(arg));
		}
		case BLOB_TYPE_TAG:
		case OSC_ELEMENT_TYPE_TAG:
		{
			uint32 blobSize = OscToUInt32(arg);
			return(4 + OscRoundUp4(blobSize));
		}

		default:
			ERROR_PRINTF("Unkown type tag\n");
			return(-1);
	}
}

//---------------------------------------------------------------
// Structs forward declarations
//---------------------------------------------------------------

struct osc_element;
struct osc_arg_iterator;

//---------------------------------------------------------------
// Osc argument tagged union
//---------------------------------------------------------------

struct osc_blob
{
	int32 size;
	const char* data;
};

/*
	WARNING: the osc_argument struct does _not_ own the memory associated with strings, blobs or nested osc elements.
	         eg., calling OscArgSetString() then freeing the string would result in a dangling pointer.
		 You be warned.
*/

struct osc_argument
{
	char tag;
	union
	{
		char c;
		int16 i16;
		int32 i32;
		int64 i64;
		float f;
		double d;
		const char* s;
		osc_blob b;
		osc_element* elt;
	};
};

INLINE_GEN void OscArgSetArrayBegin(osc_argument* arg) { arg->tag = ARRAY_BEGIN_TYPE_TAG; }
INLINE_GEN void OscArgSetArrayEnd(osc_argument* arg) { arg->tag = ARRAY_END_TYPE_TAG; }
INLINE_GEN void OscArgSetNil(osc_argument* arg) { arg->tag = NIL_TYPE_TAG; }
INLINE_GEN void OscArgSetInfinity(osc_argument* arg) { arg->tag = INFINITUM_TYPE_TAG; }
INLINE_GEN void OscArgSetBool(osc_argument* arg, bool b) { arg->tag = b ? TRUE_TYPE_TAG : FALSE_TYPE_TAG; }
INLINE_GEN void OscArgSetInt32(osc_argument* arg, int32 i) { arg->tag = 'i' ; arg->i32 = i;}
INLINE_GEN void OscArgSetInt16(osc_argument* arg, int16 i) { arg->tag = 'W' ; arg->i16 = i; }
INLINE_GEN void OscArgSetInt64(osc_argument* arg, int64 i) { arg->tag = 'h' ; arg->i64 = i; }
INLINE_GEN void OscArgSetChar(osc_argument* arg, char c)  { arg->tag = 'c' ; arg->c = c; }
INLINE_GEN void OscArgSetFloat(osc_argument* arg, float f) { arg->tag = 'f' ; arg->f = f; }
INLINE_GEN void OscArgSetDouble(osc_argument* arg, double d) { arg->tag = 'd' ; arg->d = d; }
INLINE_GEN void OscArgSetString(osc_argument* arg, const char* s) { arg->tag = 's' ; arg->s = s; }
INLINE_GEN void OscArgSetSymbol(osc_argument* arg, const char* s) { arg->tag = 'S' ; arg->s = s; }
INLINE_GEN void OscArgSetBlob(osc_argument* arg, uint32 size, const char* data)
{
	arg->tag = 'b';
	arg->b.size = size;
	arg->b.data = data;
}
INLINE_GEN void OscArgSetRgbaColor(osc_argument* arg, uint32 col) { arg->tag = 'r' ; arg->i32 = col; }
INLINE_GEN void OscArgSetMidiMessage(osc_argument* arg, uint32 m) { arg->tag = 'm' ; arg->i32 = m; }
INLINE_GEN void OscArgSetTimeTag(osc_argument* arg, uint64 t) { arg->tag = 't' ; arg->i64 = t; }
INLINE_GEN void OscArgSetInt32Variant(osc_argument* arg, char tag, int32 i) { arg->tag = tag ; arg->i32 = i; }
INLINE_GEN void OscArgSetInt64Variant(osc_argument* arg, char tag, int64 i) { arg->tag = tag ; arg->i64 = i; }
INLINE_GEN void OscArgSetStringVariant(osc_argument* arg, char tag, const char* s) { arg->tag = tag ; arg->s = s; }
INLINE_GEN void OscArgSetElement(osc_argument* arg, osc_element* nested) { arg->tag = 'O' ; arg->elt = nested; }

//---------------------------------------------------------------
// Osc message struct
//---------------------------------------------------------------

struct osc_msg
{
	uint32 msgSize;
	uint32 addressPatternSize;
	uint32 typeStringSize;
	uint32 argsSize;

	const char* addressPattern;    // points to the begining of the address pattern (also the packet start)
	const char* typeString;        // points to the type string, INCLUDING the leading ','
	const char* typeStringEnd;     // points to the first '\0' of the typeString
	const char* args;              // points to the first argument
};

//NOTE(martin): point the message to a memory buffer
INLINE_GEN OscErrCode OscMessagePrepare(osc_msg* msg, uint32 size, char* buffer)
{
	//TODO(martin): ASSERT size
	memset(msg, 0, sizeof(osc_msg));
	msg->addressPattern = buffer;
	msg->msgSize = size;
	return(OSC_OK);
}

INLINE_GEN const char* OscMessageData(osc_msg* msg)
{
	return(msg->addressPattern);
}

INLINE_GEN uint32 OscMessageSize(osc_msg* msg)
{
	return(msg->msgSize);
}

INLINE_GEN uint32 OscArgumentsCount(osc_msg* msg)
{
	return(msg->typeStringEnd ? (msg->typeStringEnd - msg->typeString - 1) : 0);
}

INLINE_GEN const char* OscAddressPattern(osc_msg* msg)
{
	return(msg->addressPattern);
}

INLINE_GEN const char* OscTypeTags(osc_msg* msg)
{
	return(msg->typeString ? (msg->typeString+1) : 0);
}

//---------------------------------------------------------------
// Osc element
//---------------------------------------------------------------

/* NOTE(martin): osc_element allows to parse and iterate thru OSC elements, which are either messages or bundles
                 It also allows to compose OSC packets
*/

struct osc_composer
{
	uint8 isBundle;
	int32 size;
	char* data;
	osc_composer* parent;

	union
	{
		struct
		{
			char* args;
			char* nextArg;
			char* nextTag;
			int arrayLevel;
		} msg;

		struct
		{
			char* nextSlot;
		} bundle;
	};
};

//NOTE(martin): composition memory overhead (per nesting level)
const int OSC_COMPOSE_OVERHEAD = sizeof(osc_composer);

struct osc_element
{
	int32 size;
	const char* data;

	//NOTE(martin): iterator data
	osc_element* parent;
	bool msgCache;
	osc_msg msg;

	//NOTE(martin): composition data
	bool composing;
	osc_composer* composer;
};

INLINE_GEN const char* OscElementData(osc_element* element)
{
	return(element->data);
}

INLINE_GEN int OscElementSize(osc_element* element)
{
	return(element->size);
}

//---------------------------------------------------------------
// Osc composing
//---------------------------------------------------------------

OscErrCode OscBeginPacket(osc_element* elt, int32 size, char* buffer);
OscErrCode OscEndPacket(osc_element* elt);

OscErrCode OscBeginMessage(osc_element* elt, const char* addressPattern);
OscErrCode OscEndMessage(osc_element* elt);

OscErrCode OscBeginBundle(osc_element* elt, uint64 timetag = 0);
OscErrCode OscEndBundle(osc_element* elt);

OscErrCode OscArrayBegin(osc_element* elt);
OscErrCode OscArrayEnd(osc_element* elt);
OscErrCode OscPushIterator(osc_element* elt, osc_arg_iterator* it);
OscErrCode OscPushArgument(osc_element* elt, osc_argument* arg);
OscErrCode OscPushNil(osc_element* elt);
OscErrCode OscPushInfinity(osc_element* elt);
OscErrCode OscPushBool(osc_element* elt, bool b);
OscErrCode OscPushInt32(osc_element* elt, int32 i);
OscErrCode OscPushInt16(osc_element* elt, int16 i);
OscErrCode OscPushInt64(osc_element* elt, int64 i);
OscErrCode OscPushChar(osc_element* elt, char c);
OscErrCode OscPushFloat(osc_element* elt, float f);
OscErrCode OscPushDouble(osc_element* elt, double d);
OscErrCode OscPushString(osc_element* elt, const char* s);
OscErrCode OscPushSymbol(osc_element* elt, const char* s);
OscErrCode OscPushBlob(osc_element* elt, uint32 size, const char* data);
OscErrCode OscPushRgbaColor(osc_element* elt, uint32 col);
OscErrCode OscPushMidiMessage(osc_element* elt, uint32 m);
OscErrCode OscPushTimeTag(osc_element* elt, uint64 t);

OscErrCode OscPushInt32Variant(osc_element* elt, char tag, int32 i);
OscErrCode OscPushInt64Variant(osc_element* elt, char tag, int64 i);
OscErrCode OscPushStringVariant(osc_element* elt, char tag, const char* s);

//NOTE(martin): this is the copying version for pushing an already composed nested element
OscErrCode OscPushElement(osc_element* elt, osc_element* nested);

//---------------------------------------------------------------
// OSC Parsing functions
//---------------------------------------------------------------

OscErrCode OscParseMessage(osc_msg* msg, int32 size, const char* packet);
OscErrCode OscParsePacket(osc_element* element, int32 size, const char* packet);

//---------------------------------------------------------------------------------
// Bundle iterator functions
//---------------------------------------------------------------------------------

osc_element OscElementsBegin(osc_element* bundle);
osc_element OscElementsEnd(osc_element* bundle);
osc_element* OscElementNext(osc_element* elt);

osc_element* OscElementIteratorCreate(osc_element* bundle);
void OscElementIteratorDestroy(osc_element* iterator);

OscErrCode OscElementAsMessage(osc_element* element, osc_msg** msg);
osc_msg* OscElementAsMessageUnchecked(osc_element* element);

//NOTE(martin): INLINE_GEN comparison functions

INLINE_GEN bool OscElementIsEqual(const osc_element* lhs, const osc_element* rhs)
{
	return(lhs->parent == rhs->parent
	      && lhs->data == rhs->data
	      && lhs->size == rhs->size);
}

__attribute__((used))
INLINE_GEN bool OscIsAtEndOfBundle(osc_element* bundle, osc_element* elt)
{
	osc_element end = OscElementsEnd(bundle);
	return(OscElementIsEqual(&end, elt));
}

INLINE_GEN bool OscElementIsBundle(const osc_element* elt)
{
	#ifdef DEBUG
		return((elt->size >= 8) && !strcmp(elt->data, "#bundle"));
	#else
		return(elt->data[0] == '#');
	#endif
}

INLINE_GEN bool OscElementIsMessage(const osc_element* elt)
{
	return(!OscElementIsBundle(elt));
}

//NOTE(martin): C++ overloaded operators, for those who like it...
#ifdef __cplusplus
	extern "C++" INLINE_GEN bool operator==(const osc_element& lhs, const osc_element& rhs)
	{
		return(OscElementIsEqual(&lhs, &rhs));
	}
	extern "C++" INLINE_GEN bool operator!=(const osc_element& lhs, const osc_element& rhs)
	{
		return(!(lhs == rhs));
	}

	extern "C++" INLINE_GEN osc_element& operator++(osc_element& elt)
	{
		return(*OscElementNext(&elt));
	}

	extern "C++" INLINE_GEN osc_element operator++(osc_element& elt, int d)
	{
		osc_element old = elt;
		OscElementNext(&elt);
		return(old);
	}
#endif // __cplusplus

//---------------------------------------------------------------------------------
// Osc arguments iterator
//---------------------------------------------------------------------------------
struct osc_arg_iterator
{
	const char* tag;
	const char* data;
};

INLINE_GEN osc_arg_iterator OscArgumentsBegin(osc_msg* msg)
{
	osc_arg_iterator arg;
	arg.tag = msg->typeString;
	if(arg.tag)
	{
		arg.tag++;
	}
	arg.data = msg->args;
	return(arg);
}

INLINE_GEN osc_arg_iterator OscArgumentsEnd(osc_msg* msg)
{
	osc_arg_iterator arg;
	arg.tag = msg->typeStringEnd;
	arg.data = msg->args + msg->argsSize;
	return(arg);
}

INLINE_GEN osc_arg_iterator* OscArgumentNext(osc_arg_iterator* arg)
{
	if(*(arg->tag))
	{
		arg->data += OscGetArgSize(*arg->tag, arg->data);
		arg->tag++;
	}
	return(arg);
}

/* NOTE(martin): osc_arg_iterator creation/destruction function
		they're useful for bindings where we prefer to only deal with pointers (eg. lisp)
*/
osc_arg_iterator* OscArgumentCreate(osc_msg* msg);
void OscArgumentDestroy(osc_arg_iterator* arg);


//NOTE(martin): get type / remaining arg count
__attribute__((used))
INLINE_GEN char OscArgumentType(const osc_arg_iterator* arg)
{
	return(*(arg->tag));
}

INLINE_GEN const char* OscArgumentRemainingTags(const osc_arg_iterator* arg)
{
	return(arg->tag);
}

INLINE_GEN uint32 OscArgumentRemainingArgCount(const osc_arg_iterator* arg)
{
	return(strlen(arg->tag));
}

//NOTE(martin): argument iterator comparisons

INLINE_GEN bool OscArgumentIsEqual(const osc_arg_iterator* lhs, const osc_arg_iterator* rhs)
{
	return((lhs->tag == rhs->tag) && (lhs->data == rhs->data));
}

INLINE_GEN bool OscIsAtEndOfMessage(osc_msg* msg, osc_arg_iterator* arg)
{
	return(arg->data == (msg->args + msg->argsSize));
}


//NOTE(martin): C++ overloaded operators
#ifdef __cplusplus
	extern "C++" INLINE_GEN bool operator==(const osc_arg_iterator& lhs, const osc_arg_iterator& rhs)
	{
		return(OscArgumentIsEqual(&lhs, &rhs));
	}
	extern "C++" INLINE_GEN bool operator!=(const osc_arg_iterator& lhs, const osc_arg_iterator& rhs)
	{
		return(!(lhs == rhs));
	}

	extern "C++" INLINE_GEN osc_arg_iterator& operator++(osc_arg_iterator& arg)
	{
		return(*OscArgumentNext(&arg));
	}

	extern "C++" INLINE_GEN osc_arg_iterator operator++(osc_arg_iterator& arg, int d)
	{
		osc_arg_iterator old = arg;
		OscArgumentNext(&arg);
		return(old);
	}
#endif // __cplusplus

//---------------------------------------------------------------------------------
// OSC Iterator conversion functions
//---------------------------------------------------------------------------------

OscErrCode OscAsBool(osc_arg_iterator* arg, bool* b);
OscErrCode OscAsInt16(osc_arg_iterator* arg, int16* i);
OscErrCode OscAsInt32(osc_arg_iterator* arg, int32* i);
OscErrCode OscAsInt64(osc_arg_iterator* arg, int64* i);
OscErrCode OscAsFloat(osc_arg_iterator* arg, float* f);
OscErrCode OscAsDouble(osc_arg_iterator* arg, double* d);
OscErrCode OscAsChar(osc_arg_iterator* arg, char* c);
OscErrCode OscAsString(osc_arg_iterator* arg, const char** s);
OscErrCode OscAsSymbol(osc_arg_iterator* arg, const char** s);
OscErrCode OscAsBlob(osc_arg_iterator* arg, const void** data, uint32* size);
OscErrCode OscAsElement(osc_arg_iterator* arg, osc_element* element);
OscErrCode OscAsRgbaColor(osc_arg_iterator* arg, uint32* col);
OscErrCode OscAsMidiMessage(osc_arg_iterator* arg, uint32* msg);
OscErrCode OscAsTimeTag(osc_arg_iterator* arg, uint64* t);

//NOTE(martin): INLINE_GEN unchecked versions

INLINE_GEN bool OscAsBoolUnchecked(const osc_arg_iterator* arg)
{
	return(*(arg->tag) == TRUE_TYPE_TAG);
}

INLINE_GEN int32 OscAsInt32Unchecked(const osc_arg_iterator* arg)
{
	return(OscToInt32(arg->data));
}

INLINE_GEN int16 OscAsInt16Unchecked(const osc_arg_iterator* arg)
{
	return(OscToInt16(arg->data));
}

INLINE_GEN int64 OscAsInt64Unchecked(const osc_arg_iterator* arg)
{
	return(OscToInt64(arg->data));
}

INLINE_GEN float OscAsFloatUnchecked(const osc_arg_iterator* arg)
{
	return(OscToFloat(arg->data));
}

INLINE_GEN double OscAsDoubleUnchecked(const osc_arg_iterator* arg)
{
	return(OscToDouble(arg->data));
}

INLINE_GEN char OscAsCharUnchecked(const osc_arg_iterator* arg)
{
    return (char)OscToInt32(arg->data);
}

INLINE_GEN const char* OscAsStringUnchecked(const osc_arg_iterator* arg)
{
	return(arg->data);
}

INLINE_GEN const char* OscAsSymbolUnchecked(const osc_arg_iterator* arg)
{
	return(OscAsStringUnchecked(arg));
}

INLINE_GEN OscErrCode OscAsBlobUnchecked(const osc_arg_iterator* arg, const void** data, uint32* size)
{
	int32 sizeResult = (int32)OscToUInt32(arg->data);
	if(!OscIsValidElementSize(sizeResult))
	{
		return(OSC_INVALID_SIZE);
	}
	*size = sizeResult;
	*data = (void*)(arg->data + OSC_SIZEOF_INT32);
	return(OSC_OK);
}

INLINE_GEN OscErrCode OscAsElementUnchecked(const osc_arg_iterator* arg, osc_element* element)
{
	int32 sizeResult = (int32)OscToUInt32(arg->data);
	if(!OscIsValidElementSize(sizeResult))
	{
		//TODO(martin): should have been checked already in OscParseMessage() ?
		return(OSC_INVALID_SIZE);
	}
	return(OscParsePacket(element, sizeResult, arg->data + OSC_SIZEOF_INT32));
}

INLINE_GEN uint32 OscAsRgbaColorUnchecked(const osc_arg_iterator* arg)
{
	return(OscToUInt32(arg->data));
}

INLINE_GEN uint32 OscAsMidiMessageUnchecked(const osc_arg_iterator* arg)
{
	return(OscToUInt32(arg->data));
}

INLINE_GEN uint64 OscAsTimeTagUnchecked(const osc_arg_iterator* arg)
{
	return(OscToUInt64(arg->data));
}

//---------------------------------------------------------------------------------
// variadic and array-based composing / parsing functions
//---------------------------------------------------------------------------------

//NOTE(martin): Push several arguments in a message
OscErrCode OscPushFormat(osc_element* elt, const char* typeTags, ...);
OscErrCode OscPushFormatVA(osc_element* elt, const char* typeTags, va_list ap);
OscErrCode OscPushA(osc_element* elt, int argc, osc_argument* argv);

//NOTE(martin): insert a message into a composing packet with a single function
OscErrCode OscFormatMessage(osc_element* elt, const char* pattern, const char* typeTags = 0, ...);
OscErrCode OscFormatMessageVA(osc_element* elt, const char* pattern, const char* typeTags, va_list ap);
OscErrCode OscFormatMessageA(osc_element* elt, const char* pattern, int argc, osc_argument* argv);

//NOTE(martin): format a message packet with a single function
OscErrCode OscFormat(osc_element* elt, uint32 size, char* buffer, const char* pattern, const char* typeTags = 0, ...);
OscErrCode OscFormatVA(osc_element* elt, uint32 size, char* buffer, const char* pattern, const char* typeTags, va_list ap);
OscErrCode OscFormatA(osc_element* elt, uint32 size, char* buffer, const char* pattern, int argc, osc_argument* argv);

//NOTE(martin): scan a message's arguments
OscErrCode OscScanArguments(osc_msg* elt, const char* typeTags = 0, ...);
OscErrCode OscScanArgumentsVA(osc_msg* elt, const char* typeTags, va_list ap);

OscErrCode OscScan(osc_msg* elt, const char* address, const char* typeTags = 0, ...);
OscErrCode OscScanVA(osc_msg* elt, const char* address, const char* typeTags, va_list ap);

//TODO(martin): scan packet ?

//---------------------------------------------------------------
// Print / Debug functions
//---------------------------------------------------------------

void OscArgumentsPrint(const char* typeTags, const char* args);
void OscMessagePrint(osc_msg* msg);
OscErrCode OscElementPrint(osc_element* element, int indentLevel = 0);

#ifdef DEBUG
#define DEBUG_OSC_MSG_PRINT(msg) OscMessagePrint(msg)
#define DEBUG_OSC_ELT_PRINT(elt) OscElementPrint(elt)
#else
#define DEBUG_OSC_MSG_PRINT(msg)
#define DEBUG_OSC_ELT_PRINT(elt)
#endif


#ifdef __cplusplus
} // extern "C"
#endif	// __cplusplus


#endif //__OSC_DECODE_H_
