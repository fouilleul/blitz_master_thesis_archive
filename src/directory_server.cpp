/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: directory.cpp
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*****************************************************************/

/*
	Functions of the service directory server, which provides service names lookup to blitz modules
*/

#include<signal.h>	// signal()
#include<string.h>	// memset(), memcpy(), et al.

#include"debug_log.h"
#include"containers.h"
#include"platform_socket.h"
#include"platform_thread.h"
#include"osc.h"

#include"directory.h"


//-----------------------------------------------------------------------------------------------
// Server discovery responder thread
//-----------------------------------------------------------------------------------------------

struct responder_info
{
	bool run;
	platform_socket* sock;

};

void* ServerDiscoveryResponder(void* user)
{
	responder_info* info = (responder_info*)user;

	const int maxSize = 4096;
	char buffer[maxSize];

	while(info->run)
	{
		socket_address from;
		int size = SocketReceiveFrom(info->sock, buffer, maxSize, 0, &from);

		if(size > 0)
		{
			DEBUG_PRINTF("Received a packet from %s:%hu\n",
				      NetIPToString(from.ip),
				      NetToHostPort(from.port));
			osc_msg msg;
			if(OscParseMessage(&msg, size, buffer) == 0)
			{
				if(!strcmp(OscAddressPattern(&msg), "/hello"))
				{
					DEBUG_PRINTF("Received a /hello message\n");

					//TODO(martin): include the main thread's port in the followme message
					//		(and the time thread port, too !)
					//NOTE(martin): reply
					osc_element out;
					OscFormat(&out, maxSize, buffer, "/followme");
					SocketSendTo(info->sock, (void*)OscElementData(&out), OscElementSize(&out), 0, &from);
				}
			}
			else
			{
				ERROR_PRINTF("malformed message from %s:%hu\n",
				      NetIPToString(from.ip),
				      NetToHostPort(from.port));
			}
		}
	}
	return(0);
}


//-----------------------------------------------------------------------------------------------
// Service directory Server main function
//-----------------------------------------------------------------------------------------------

const short DIRECTORY_BACKLOG	= 100;

bool _mainLoopRun = true;

void SignalHandler(int sig)
{
	_mainLoopRun = false;
}

int CheckReceiveErrors(service_directory* directory, socket_activity* activity, socket_address* from, int32 size, uint32 expectedSize)
{
	if(size < expectedSize)
	{
		if(size == 0)
		{
			//NOTE(martin): this is a disconnection
			DEBUG_PRINTF("Client %s:%hu disconnected\n",
				     NetIPToString(from->ip),
				     NetToHostPort(from->port));

			//NOTE(martin): release all held locks for this client
			//ServiceDirectoryReleaseClientLocks(directory, activity->sock);
		}
		else if(size == -1)
		{
			//NOTE(martin): this is a socket error
			ERROR_PRINTF("Socket error on client %s:%hu : %s\n",
				     NetIPToString(from->ip),
				     NetToHostPort(from->port),
				     SocketGetLastErrorMessage());
		}
		else
		{
			//NOTE(martin): this is a client error
			ERROR_PRINTF("Protocol error on client %s:%hu : short count\n",
				     NetIPToString(from->ip),
				     NetToHostPort(from->port));
		}
		SocketClose(activity->sock);
		activity->sock = 0;
		return(-1);
	}
	else
	{
		return(0);
	}
}

int main(int argc, char** argv)
{
	//NOTE(martin): install a dummy signal handler, so that we can break blocking io when receiving SIGINT
	struct sigaction sa = {};
	sa.sa_handler = SignalHandler;
	sa.sa_flags =  0;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGINT, &sa, 0);


	service_directory directory;


	//NOTE(martin): lauch time server

	TimeServerStart(&directory.timeServer, DIRECTORY_TIME_PORT);

	//NOTE(martin): create a socket to listen on module's requests
	//		the modules communicate with the directory through TCP port 5559

	platform_socket* sock = SocketOpen(SOCK_TCP);

	if(!sock)
	{
		ERROR_PRINTF("Couldn't create main socket : %s\n", SocketGetLastErrorMessage());
		return(-1);
	}
	//TODO(martin): add a cleaning lambda to close the socket and avoid leaving it in wait state in case of error

	socket_address addr;
	addr.ip = SOCK_IP_ANY;
	addr.port = HostToNetPort(DIRECTORY_PORT);

	if(SocketBind(sock, &addr) != 0)
	{
		ERROR_PRINTF("Failed binding socket : %s\n", SocketGetLastErrorMessage());
		return(-1);
	}

	//NOTE(martin): listen on port and accept incoming connections

	if(SocketListen(sock, DIRECTORY_BACKLOG) != 0)
	{
		ERROR_PRINTF("Failed listening : %s\n", SocketGetLastErrorMessage());
		return(-1);
	}

	//NOTE(martin): launch the responder thread

	responder_info responderInfo;
	responderInfo.run = true;
	responderInfo.sock = SocketOpen(SOCK_UDP);

	if(!responderInfo.sock)
	{
		ERROR_PRINTF("Couldn't create responder socket\n");
		return(-1);
	}
	{
		socket_address addr;
		addr.ip = SOCK_IP_ANY;
		addr.port = HostToNetPort(DIRECTORY_RESPONDER_PORT);
		if(SocketBind(responderInfo.sock, &addr) != 0)
		{
			ERROR_PRINTF("Couldn't bind responder socket : %s\n", SocketGetLastErrorMessage());
			SocketClose(responderInfo.sock);
			return(-1);
		}
	}
	if(SocketSetBroadcast(sock, true) != 0)
	{
		ERROR_PRINTF("Couldn't set socket broadcast option : %s\n", SocketGetLastErrorMessage());
		SocketClose(responderInfo.sock);
		return(-1);
	}
	platform_thread* responder = ThreadCreate(ServerDiscoveryResponder, &responderInfo);

	//NOTE(martin): Initialize clients array

	const int buffSize = 1024;
	char buffer[buffSize];

	//TODO(martin): dynamic max clients ?

	const int maxClients = 1024;
	socket_address addresses[maxClients];
	memset(addresses, 0, (maxClients+1) * sizeof(socket_address));

	socket_activity activities[maxClients+1];
	memset(activities, 0, (maxClients+1)*sizeof(socket_activity));
	activities[0].sock = sock;
	activities[0].watch = SOCK_ACTIVITY_IN;


	while(_mainLoopRun)
	{
		if(SocketSelect(maxClients, activities, 0) <= 0)
		{
			continue;
		}

		//NOTE(martin): check activity on the main socket
		if(activities[0].set)
		{
			//NOTE(martin): this is an incomming connection, add it to the clients array
			socket_address from;
			platform_socket* connection = SocketAccept(sock, &from);
			//TODO(martin): check error

			for(int i=1; i<maxClients; i++)
			{
				if(activities[i].sock == 0)
				{
					activities[i].sock = connection;
					activities[i].watch = SOCK_ACTIVITY_IN;
					addresses[i] = from;

					break;
				}
			}
			DEBUG_PRINTF("New connection from %s\n", NetIPToString(from.ip));
		}

		//NOTE(martin): check IO operations on the clients
		for(int i=1; i<maxClients; i++)
		{
			socket_activity* activity = &(activities[i]);
			platform_socket* sd = activity->sock;
			socket_address& from = addresses[i];

			if(activity->sock && activity->set)
			{
				//NOTE(martin): we extract the first int of the stream, which gives us the OSC message length
				char oscSizeBuff[4];
				ssize_t size  = SocketReceive(sd, oscSizeBuff, sizeof(uint32), 0);

				if(CheckReceiveErrors(&directory, activity, &from, size, sizeof(uint32)) != 0)
				{
					continue;
				}
				uint32 oscSize = OscToInt32(oscSizeBuff);
				if(oscSize >= buffSize)
				{
					ERROR_PRINTF("Protocol error on client %s:%hu : size exceeds receive buffer\n",
						     NetIPToString(from.ip),
						     NetToHostPort(from.port));

					//TODO(martin): handle incorrect type (ie purge the stream ?)
					continue;
				}

				//NOTE(martin): now we receive the osc packet itself
				size = SocketReceive(sd, buffer, oscSize, 0);
				if(CheckReceiveErrors(&directory, activity, &from, size, oscSize) != 0)
				{
					continue;
				}

				//NOTE(martin): ignore bundles and dispatch messages
				if(*buffer == '#')
				{
					ERROR_PRINTF("Received an OSC bundle, ignore...\n");
				}
				else
				{
					ServiceDirectoryReceiveOscPacket(&directory, sd, from.ip, buffer, oscSize);
				}
			}
		}
	}

	responderInfo.run = false;
	SocketClose(responderInfo.sock);
	void* r;
	ThreadJoin(responder, &r);

	SocketClose(sock);

	TimeServerStop(&directory.timeServer);
	return(0);
}
