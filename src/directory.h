/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: directory.h
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*****************************************************************/
#ifndef __DIRECTORY_H_
#define __DIRECTORY_H_

/*
	Service Directory structures
*/
#include<vector>	// the boss wants us to use the STL, so we _sometimes_ do
#include<unordered_map>
#include<string>

#include"typedefs.h"
#include"containers.h"
#include"platform_socket.h"
#include"time/time_server.h"

const host_port DIRECTORY_PORT           = 5559,
                DIRECTORY_RESPONDER_PORT = 5560,
		DIRECTORY_TIME_PORT      = 5561;

//-----------------------------------------------------------------------------------------------
// sd_record  : allows to asscociate an ID = (index,generation) to an object
//-----------------------------------------------------------------------------------------------

typedef enum { RT_NONE, RT_PUBLISHER, RT_SUBSCRIBER, RT_CHANNEL, RT_RPC } record_type;

struct sd_record
{
	uint32 valid;
	uint32 genIndex;
	record_type type;
	void* object;

	list_info freeListElt;
};

//-----------------------------------------------------------------------------------------------
// service directory objects
//-----------------------------------------------------------------------------------------------

/* forward declarations */

struct publisher_info;
struct subscriber_info;

typedef std::vector<publisher_info*> pub_vec;
typedef std::vector<subscriber_info*> sub_vec;

/* channel object */

struct channel_info
{
	pub_vec publishers;
	sub_vec subscribers;
};

/* publisher object */

struct publisher_info
{
	uint64 channelID;
	socket_address address;
	//...
};

/* subscriber object */

struct subscriber_info
{
	uint64 channelID;
	socket_address address;
	//...
};

/* rpc object */

struct rpc_info
{
	socket_address address;
	//...
};


//-----------------------------------------------------------------------------------------------
// service directory
//-----------------------------------------------------------------------------------------------

typedef std::unordered_map<std::string, uint64> path_map;

const int SD_MAX_RECORD_COUNT = 32<<10;

struct service_directory
{
	sd_record records[SD_MAX_RECORD_COUNT];
	list_info freeList;
	uint32 pushIndex;
	uint32 genIndex;

	path_map pathToID;

	time_server timeServer;

	service_directory();
};

//-----------------------------------------------------------------------------------------------
// Service directory functions
//-----------------------------------------------------------------------------------------------

void ServiceDirectoryReceiveOscPacket(service_directory* directory, platform_socket* sock, net_ip fromIP, const char* buffer, uint32 oscSize);


#endif //__DIRECTORY_H_
