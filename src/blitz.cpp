/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: blitz.cpp
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*****************************************************************/

/*
	libblitz API
*/

#include<signal.h>
#include<stdio.h>
#include<assert.h>
#include<string.h>
#include<vector>

#include"debug_log.h"
#include"platform_thread.h"
#include"platform_socket.h"
#include"directory.h"
#include"osc.h"
#include"time/time_client.h"
#include"blitz.h"


extern "C" {


//------------------------------------------------------------------------------------------
// Signal helpers
//------------------------------------------------------------------------------------------

static void BlitzDummySignalHandler(int sig)
{
	//NOTE(martin): do nothing. The sole purpose of this handler is to catch user signals,
	//		which we raise in order to break free from blocking system calls (eg. recv() et al.)
}

static void BlitzInstallDummySignalHandler()
{
	//NOTE(martin): install a dummy signal handler, so that we can break blocking io from the main tread
	struct sigaction sa = {};
	sa.sa_handler = BlitzDummySignalHandler;
	sa.sa_flags =  0;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGUSR1, &sa, 0);
}

//------------------------------------------------------------------------------------------
// Blitz context
//------------------------------------------------------------------------------------------

struct blitz_context
{
	socket_address address;
	platform_socket* sock;

	time_client timeClient;
};

blitz_context* BlitzContextCreate()
{
	blitz_context* context = malloc_type(blitz_context);
	if(!context)
	{
		ERROR_PRINTF("Can't allocate context\n");
		return(0);
	}
	context->sock = 0;

	//NOTE(martin): discover the address of the service directory server

	platform_socket* sock = SocketOpen(SOCK_UDP);

	/*
	auto handle_error = [&sock, &context] () -> blitz_context*
	{
		if(context->sock)
		{
			SocketClose(sock);
		}
		free(context);
		return(0);
	};
	*/
	#define _handle_error_()	\
	{				\
		if(context->sock)	\
		{			\
			SocketClose(sock);\
		}			\
		free(context);		\
		return(0);		\
	}				\

	if(!sock)
	{
		ERROR_PRINTF("Couln't create socket : %s\n", SocketGetLastErrorMessage());
		_handle_error_();
	}
	if(SocketSetBroadcast(sock, true) != 0)
	{
		ERROR_PRINTF("Couldn't set socket broadcast option : %s\n", SocketGetLastErrorMessage());
		_handle_error_();
	}

	timeval tv;
	tv.tv_sec = 2;
	tv.tv_usec = 0;
	if(SocketSetReceiveTimeout(sock, &tv))
	{
		ERROR_PRINTF("Couldn't set socket receive timeout : %s\n", SocketGetLastErrorMessage());
		_handle_error_();
	}

	socket_address from;
	socket_address broadcast;
	broadcast.ip = 0xffffffff;
	broadcast.port = HostToNetPort(DIRECTORY_RESPONDER_PORT);

	const int maxSize = 4096;
	char rcvBuffer[maxSize];
	char sndBuffer[maxSize];
	osc_element hello;
	OscFormat(&hello, maxSize, sndBuffer, "/hello");

	DEBUG_PRINTF("Looking for the service directory server...\n");

	bool receivedReply = false;
	for(int i=0; i<8; i++)
	{
		SocketSendTo(sock, (void*)OscElementData(&hello), OscElementSize(&hello), 0, &broadcast);
		int size = SocketReceiveFrom(sock, rcvBuffer, maxSize, 0, &from);
		if(size > 0)
		{
			osc_msg reply;
			if(OscParseMessage(&reply, size, rcvBuffer) != 0)
			{
				ERROR_PRINTF("Responder reply is not a valid osc message");
			}
			else if(!strcmp(OscAddressPattern(&reply), "/followme"))
			{
				DEBUG_PRINTF("Received a reply from the directory responder at %s:%hu\n",
				              NetIPToString(from.ip),
					      NetToHostPort(from.port));
				receivedReply = true;
				context->address.ip = from.ip;
				context->address.port = HostToNetPort(DIRECTORY_PORT);
				break;
			}
			else
			{
				ERROR_PRINTF("Received unsupported reply from the directory server : %s\n",
				             OscAddressPattern(&reply));
			}
		}
		else
		{
			WARNING_PRINTF("Timeout elapsed before receiving the directory server's reply. %s\n",
			               i<4 ? "Retrying now..." : "Stop");
		}
	}
	if(!receivedReply)
	{
		ERROR_PRINTF("Couldn't get a reply from the service directory (the server may be down)\n");
		_handle_error_();
	}

	//TODO(martin): check time client status
	TimeClientStart(&context->timeClient, NetToHostIP(context->address.ip), DIRECTORY_TIME_PORT);
	return(context);

	#undef _handle_error_
}

int BlitzContextDestroy(blitz_context* context)
{
	if(context->sock)
	{
		SocketClose(context->sock);
	}
	TimeClientStop(&context->timeClient);
	free(context);
	return(0);
}

u64 BlitzTimestamp(blitz_context* context)
{
	return(UserClockGetTimestamp(TimeClientGetClock(&context->timeClient)));
}

/**
	@brief Opens a connection to the service directory

	@param[in] context	The blitz context
	@return Zero if no error occurred, -1 otherwise.

	This function must be called before calling any low-level blitz API function that communicates with the service directory (such as BlitzDirectoryRequest(), etc.)
*/
int BlitzConnect(blitz_context* context)
{
	DEBUG_ASSERT(context);
	DEBUG_ASSERT(context->sock == 0);

	context->sock = SocketOpen(SOCK_TCP);
	if(!context->sock)
	{
		return(-1);
	}
	if(SocketConnect(context->sock, &context->address) != 0)
	{
		return(-1);
	}
	return(0);
}

/**
	@brief Closes a connection to the service directory

	@param[in] context	The blitz context
	@return Zero if no error occurred, -1 otherwise.
*/
int BlitzDisconnect(blitz_context* context)
{
	DEBUG_ASSERT(context);
	if(!context->sock)
	{
		ERROR_PRINTF("Trying to close a connection that was not open\n");
		return(-1);
	}
	if(SocketClose(context->sock) != 0)
	{
		ERROR_PRINTF("Couldn't close socket : %s\n", SocketGetLastErrorMessage());
		return(-1);
	}
	context->sock = 0;
	return(0);
}

//------------------------------------------------------------------------------------------
// Blitz low-level API (sending requests to the service directory)
//------------------------------------------------------------------------------------------

/**
	@brief Sends a request to the service directory and gets the reply

	@param[in] context	The blitz context
	@param[in] request	The Osc message to send to the service directory server
	@param[out] reply	The Osc message containing the service directory reply

	@return Zero if no error occurred, -1 otherwise.
*/
int BlitzDirectoryRequest(blitz_context* context, osc_element* request, osc_msg* reply)
{
	platform_socket* sock = context->sock;
	DEBUG_ASSERT(sock);

	int32 outSize = OscElementSize(request);
	char outSizeBuff[4];
	OscFromInt32(outSizeBuff, outSize);

	void* outData = (void*)OscElementData(request);
	if(SocketSend(sock, outSizeBuff, sizeof(outSize), 0) != sizeof(outSize))
	{
		ERROR_PRINTF("Can't send packet size : %s\n", SocketGetLastErrorMessage());
		return(-1);
	}
	if(SocketSend(sock, outData, outSize, 0) != outSize)
	{
		ERROR_PRINTF("Can't send packet : %s\n", SocketGetLastErrorMessage());
		return(-2);
	}

	uint32 msgSize;
	char msgSizeBuff[4];
	if(SocketReceive(sock, msgSizeBuff, sizeof(uint32), 0) != sizeof(uint32))
	{
		ERROR_PRINTF("Can't read packet size\n");
		return(-3);
	}
	msgSize = OscToInt32(msgSizeBuff);
	if(msgSize > reply->msgSize)
	{
		ERROR_PRINTF("Packet size exceeds receive buffer\n");
		return(-4);
	}
	if(SocketReceive(sock, (void*)reply->addressPattern, msgSize, 0) != msgSize)
	{
		ERROR_PRINTF("Couldn't read packet\n");
		return(-5);
	}

	OscErrCode err = OSC_OK;
	if((err = OscParseMessage(reply, msgSize, reply->addressPattern)) != OSC_OK)
	{
		ERROR_PRINTF("Bad OSC packet : %s", OscGetErrorMessage(err));
	}
	return(0);
}

//------------------------------------------------------------------------------------------
// RPC services structures
//------------------------------------------------------------------------------------------

struct rpc_service
{
	bool run;
	platform_socket* sock;
	platform_thread* thread;
	uint64 serviceNodeID;
	uint64 hostNodeID;
	RPCServiceCallback callback;
	void* userPointer;
	char* path;

	rpc_service(const char* path_, RPCServiceCallback callback_, void* userPointer_)
	{
		run = true;
		sock = 0;
		thread = 0;
		callback = callback_;
		userPointer = userPointer_;
		path = (char*)malloc(strlen(path_)+1);
		strcpy(path, path_);
	}

	~rpc_service()
	{
		if(thread)
		{
			void* ret;
			run = false;
			ThreadSignal(thread, SIGUSR1);
			ThreadJoin(thread, &ret);
		}
		if(sock)
		{
			SocketClose(sock);
		}
		free(path);
	}
};

struct rpc_client
{
	platform_socket* sock;
	uint64  callID;
};

const uint16 SERVICE_BACKLOG = 10;

//------------------------------------------------------------------------------------------
// RPC service Internals
//------------------------------------------------------------------------------------------

static int CheckReceiveErrors(socket_activity* activity, socket_address* from, int32 size, uint32 expectedSize)
{
	if(size < expectedSize)
	{
		if(size == 0)
		{
			//NOTE(martin): this is a disconnection
			DEBUG_PRINTF("Client %s:%hu disconnected\n",
				     NetIPToString(from->ip),
				     NetToHostPort(from->port));
		}
		else if(size == -1)
		{
			//NOTE(martin): this is a socket error
			ERROR_PRINTF("Socket error on client %s:%hu : %s\n",
				     NetIPToString(from->ip),
				     NetToHostPort(from->port),
				     SocketGetLastErrorMessage());
		}
		else
		{
			//NOTE(martin): this is a client error
			ERROR_PRINTF("Protocol error on client %s:%hu : short count\n",
				     NetIPToString(from->ip),
				     NetToHostPort(from->port));
		}
		SocketClose(activity->sock);
		activity->sock = 0;
		return(-1);
	}
	else
	{
		return(0);
	}
}

static void* BlitzRPCServiceListener(void* p)
{
	DEBUG_ASSERT(p);

	rpc_service* data = (rpc_service*)p;

	//NOTE(martin): install a dummy signal handler, so that we can break blocking io from the main tread
	BlitzInstallDummySignalHandler();

	platform_socket* sock = data->sock;

	//NOTE(martin): initialize our client array
	//TODO(martin): handle dynamic client count ??

	const int maxClients = 1024;

	socket_address addresses[maxClients];
	memset(addresses, 0, (maxClients+1) * sizeof(socket_address));

	socket_activity activities[maxClients];
	memset(activities, 0, maxClients*sizeof(socket_activity));

	activities[0].sock = sock;
	activities[0].watch = SOCK_ACTIVITY_IN;

	//NOTE(martin): process clients
	//TODO(martin): handle bigger requests

	const int buffSize = 4096;
	char buffer[buffSize];

	while(data->run)
	{
		//NOTE(martin): wait for IO operations on clients or main socket with select()

		if(SocketSelect(maxClients, activities, 0) <= 0)
		{
			continue;
		}

		//NOTE(martin): check IO operations on main socket
		if(activities[0].set)
		{
			//NOTE(martin): this is an incomming connection, add it to the clients array
			socket_address from;
			platform_socket* connection = SocketAccept(sock, &from);
			//TODO(martin): check error

			for(int i=1; i<maxClients; i++)
			{
				if(activities[i].sock == 0)
				{
					activities[i].sock = connection;
					activities[i].watch = SOCK_ACTIVITY_IN;
					addresses[i] = from;
					break;
				}
			}
			DEBUG_PRINTF("New connection from %s\n", NetIPToString(from.ip));
		}

		//NOTE(martin): check IO operations on the clients
		for(int i=1; i<maxClients; i++)
		{
			socket_activity* activity = &(activities[i]);
			platform_socket* sd = activity->sock;
			socket_address& from = addresses[i];

			if(activity->sock && activity->set)
			{
				//NOTE(martin): we extract the first int of the stream, which gives us the OSC message length
				char oscSizeBuff[4];
				ssize_t size  = SocketReceive(sd, oscSizeBuff, sizeof(uint32), 0);

				if(CheckReceiveErrors(activity, &from, size, sizeof(uint32)) != 0)
				{
					continue;
				}
				uint32 oscSize = OscToInt32(oscSizeBuff);
				if(oscSize >= buffSize)
				{
					ERROR_PRINTF("Protocol error on client %s:%hu : size exceeds receive buffer\n",
						     NetIPToString(from.ip),
						     NetToHostPort(from.port));

					//TODO(martin): handle incorrect type (ie purge the stream ?)
					continue;
				}

				//NOTE(martin): now we receive the osc packet itself
				size = SocketReceive(sd, buffer, oscSize, 0);
				if(CheckReceiveErrors(activity, &from, size, oscSize) != 0)
				{
					continue;
				}

				//NOTE(martin): ignore bundles and dispatch messages
				if(*buffer == '#')
				{
					ERROR_PRINTF("Received an OSC bundle, ignore...\n");
				}
				else
				{
					//NOTE(martin): here we parse the incoming message, extract the callID,
					//		and compose a 'user' version of the message,
					//		which we pass to the callback

					osc_msg rcvMsg;
					osc_element userElement;

					if(OscParseMessage(&rcvMsg, oscSize, buffer) != 0)
					{
						ERROR_PRINTF("Can't parse Osc message\n");
						SocketClose(activity->sock);
						activity->sock = 0;
						continue;
						//TODO(martin): handle error with a /FAIL reply ?
					}

					int argCount = OscArgumentsCount(&rcvMsg);
					const char* tags = OscTypeTags(&rcvMsg);

					if(strcmp(tags, "hO") || strcmp(OscAddressPattern(&rcvMsg), "/REQ"))
					{
						ERROR_PRINTF("Osc message is not formatted as an RPC request\n");
						OscMessagePrint(&rcvMsg);
						SocketClose(activity->sock);
						activity->sock = 0;
						continue;
					}

					osc_arg_iterator arg = OscArgumentsBegin(&rcvMsg);
					uint64 callID = OscAsInt64Unchecked(&arg); OscArgumentNext(&arg);

					OscAsElementUnchecked(&arg, &userElement); //TODO(martin): return type could be an error

					rpc_client* client = malloc_type(rpc_client);
					client->sock = activity->sock;
					client->callID = callID;

					data->callback(client, &userElement, data->userPointer);
				}
			}
		}
	}

	//NOTE(martin): close remaining clients
	for(int i=1; i<maxClients; i++)
	{
		if(activities[i].sock)
		{
			SocketClose(activities[i].sock);
		}
	}

	//NOTE(martin): the main socket is closed when the rpc_service is deleted

	return(0);
}

static inline int SendOscPacket(platform_socket* sock, osc_element* elt)
{
	uint32 oscSize = OscElementSize(elt);
	char oscSizeBuff[4];
	OscFromInt32(oscSizeBuff, oscSize);

	SocketSend(sock, oscSizeBuff, sizeof(uint32), 0);
	SocketSend(sock, (void*)OscElementData(elt), oscSize, 0);
	//TODO(martin): check and return errors
	return(0);
}

//------------------------------------------------------------------------------------------
// RPC service API
//------------------------------------------------------------------------------------------

/**
	@brief	Registers an RPC service provider to the service directory

	@param[in] context	The blitz context
	@param[in] path		The path to the service
	@param[in] callback	The callback that will be called upon RPC request
	@param[in] userPointer  A pointer that will be passed to the callback. It allows to provide it some (user-defined) context

	@return A handle to the service
*/
rpc_service* BlitzRPCOpenService(blitz_context* context, const char* path, RPCServiceCallback callback, void* userPointer)
{
	DEBUG_ASSERT(context);
	DEBUG_ASSERT(path);

	rpc_service* service = new rpc_service(path, callback, userPointer);

	//NOTE(martin): create a socket and a listener thread for our service

	service->sock = SocketOpen(SOCK_TCP);
	ASSERT(service->sock);

	socket_address addr;
	addr.ip = SOCK_IP_ANY;
	addr.port = SOCK_PORT_ANY;

	if(SocketBind(service->sock, &addr) != 0)
	{
		ERROR_PRINTF("Failed binding socket\n");
		delete service;
		return(0);
	}
	if(SocketGetAddress(service->sock, &addr) != 0)
	{
		ERROR_PRINTF("Failed getting back the socket address\n");
		delete service;
		return(0);
	}
	if(SocketListen(service->sock, SERVICE_BACKLOG) != 0)
	{
		ERROR_PRINTF("Failed listening on socket\n");
		delete service;
		return(0);
	}

	service->thread = ThreadCreate(BlitzRPCServiceListener, service);
	if(!service->thread)
	{
		ERROR_PRINTF("Failed to create listener thread\n");
		delete service;
		return(0);
	}

	//NOTE(martin): register our service to the service directory

	BlitzConnect(context);

	const int buffSize = 4096;
	char sndBuff[buffSize];
	char rcvBuff[buffSize];
	osc_element request;
	osc_msg reply;

	OscFormat(&request, buffSize, sndBuff, "/regrpc", "si", path, NetToHostPort(addr.port));
	OscMessagePrepare(&reply, buffSize, rcvBuff);
	BlitzDirectoryRequest(context, &request, &reply);

	uint64 serviceNode = 0;
	if(OscScan(&reply, "/OK!", "h", &serviceNode) != OSC_OK)
	{
		//TODO(martin): handle error...
	}

	WARNING_PRINTF("service node is %llx\n", serviceNode);
	service->serviceNodeID = serviceNode;

	BlitzDisconnect(context);

	ASSERT(context->sock == 0);
	return(service);
}

/**
	@brief	Closes an RPC service and unregisters it from the service directory

	@param[in] context	The blitz context
	@param[in] service	The handle to the service, which was returned by BlitzRegisterRPCService()

	@return Zero if no error occured.
*/
int BlitzRPCCloseService(blitz_context* context, rpc_service* service)
{
	DEBUG_ASSERT(context);
	DEBUG_ASSERT(service);

	//NOTE(martin): unregister from the directory

	BlitzConnect(context);

	const int buffSize = 4096;
	char sndBuff[buffSize];
	char rcvBuff[buffSize];
	osc_element request;
	osc_msg reply;

	OscFormat(&request, buffSize, sndBuff, "/unrpc", "h", service->serviceNodeID);
	OscMessagePrepare(&reply, buffSize, rcvBuff);
	BlitzDirectoryRequest(context, &request, &reply);

	if(OscScan(&reply, "/OK!") != OSC_OK)
	{
		//TODO(martin): handle error...
	}

	BlitzDisconnect(context);

	delete service; //NOTE(martin): terminates the listening thread and free the resources
	return(0);
}

/**
	@brief	Sends a response to an RPC client. This function should be used inside the user-provided callback to reply to the request.

	@param[in] client	The rpc client, as passed to the user callback
	@param[in] msg		The OSC message to send to the client as a reply to its request

	@return Zero if no error occured. Otherwise a socket error code could be returned
*/
int BlitzRPCReply(rpc_client* client, osc_element* element)
{
	DEBUG_ASSERT(client);
	DEBUG_ASSERT(element);

	osc_element sndMsg;
	char buffer[4096];

	OscFormat(&sndMsg, 4096, buffer, "/REP", "hO", client->callID, element);

	platform_socket* sock = client->sock;
	free(client);
	return(SendOscPacket(sock, &sndMsg));
}


//------------------------------------------------------------------------------------------
// RPC Link / handle structures
//------------------------------------------------------------------------------------------

struct rpc_link;

struct rpc_handle
{
	rpc_status status; // Indicate the current status of the handle

	uint32	    genIndex;	// a unique counter allowing to differentiate reused handles from old ones
	rpc_link*   link;	// if not null, there is an open link and a listener thread
	uint8	    retired;	// mark the handle as retired. It can't be a status, because the status
				//could change during retirement

	platform_mutex* mutex;
	platform_condition* condition;

	// user callback and data
	RPCLinkCallback callback;
	void* userPointer;

	uint32 replyMaxSize;	 // size of replyBuffer
	char*  replyBuffer;	 // buffer containing the reply upon return
	osc_element replyElement; // layout of the osc element reply
};

const uint32 BLITZ_MAX_PENDING_RPC = 1024;

struct rpc_link
{
	// Connection data
	socket_address address;
	platform_socket* sock;

	// Listener thread
	bool	  run;
	platform_thread* thread;

	// user callback
	RPCLinkCallback callback;
	void* userPointer;

	// call handles management
	uint32 genIndex;
	rpc_handle* pendingCalls[BLITZ_MAX_PENDING_RPC];
};

inline uint64 MakeCallID(int32 index, uint32 genID)
{
	return((uint64) ( (((int64)index)<<32) | (int64)genID ));
}

inline int32 GetIndexFromCallID(uint64 callID)
{
	return((int32) (((int64)callID)>>32) );
}

inline uint32 GetGenFromCallID(uint64 callID)
{
	return((uint32) (callID & 0xffffffff));
}

//------------------------------------------------------------------------------------------
// RPC Link / handle Internals
//------------------------------------------------------------------------------------------

static void BlitzRPCNotifyHandle(rpc_handle* handle, rpc_status status)
{
	DEBUG_ASSERT(handle);

	if(handle->status == RPC_STATUS_BLOCKING)
	{
		MutexLock(handle->mutex);
		handle->status = status;
		MutexUnlock(handle->mutex);
		ConditionSignal(handle->condition);
	}
	else
	{
		handle->status = status;
		if(handle->callback)
		{
			handle->callback(handle, handle->status, &handle->replyElement, handle->userPointer);
		}
	}
}

static void BlitzRPCAsyncCallConnectionError(rpc_link* link)
{
	DEBUG_ASSERT(link);

	//NOTE(martin): close the socket and set it to zero, so the user thread knows that we are disconnected

	SocketClose(link->sock);
	link->sock = 0;

	//NOTE(martin):	We need a memory fence here to ensure that link->sock is effectively set to zero
	//              before starting the following for loop so that blocking calls can check link->sock
	//              and don't wait.
	PlatformMemoryFence();

	//NOTE(martin): mark all pending calls as failed
	for(int i=0; i<BLITZ_MAX_PENDING_RPC; i++)
	{
		rpc_handle* handle = link->pendingCalls[i];
		if(handle)
		{
			//WARNING(martin): There could be a race with a handle committed just after the check
			//		   Its not dramatic if the handle is not blocking, because it would be
			//                 freed at the next unlock, but it could cause a deadlock if its blocking....
			//		   That's why we use timed waits to periodically give a chance to unlock
			//UPDATE(martin): is this still accurate ?

			BlitzRPCNotifyHandle(handle, RPC_STATUS_FAIL);
		}
	}

	//NOTE(martin): wait until awaken by a signal. We will then loop to process retired handles,
	//		fail to recv again if the connection has not been reestablished, etc.. until closed by the user
	sigset_t set;
	sigemptyset(&set);
	sigsuspend(&set);
}

static int BlitzRPCDiscardExcessBytes(rpc_link* link, char* buffer, int32 bufferSize, int32 size)
{
	DEBUG_ASSERT(link);

	while(size > 0)
	{
		int32 maxSize = minimum(size, bufferSize);
		int rcvSize = SocketReceive(link->sock, buffer, maxSize, 0);
		if(rcvSize <= 0)
		{
			return(-1);
		}
		else
		{
			size -= rcvSize;
		}
	}
	return(0);
}

static int BlitzRPCReceiveOscReply(rpc_link* link)
{
	platform_socket* sd = link->sock;

	char oscSizeBuff[4];
	int size = SocketReceive(link->sock, oscSizeBuff, sizeof(uint32), 0);
	if(size != sizeof(uint32))
	{
		//NOTE(martin): disconnection or error. If EINTR, we were just awaken by a signal,
		//		either to quit or to clean.

		if(SocketGetLastError() != SOCK_ERR_INTR)
		{
			//NOTE(martin): otherwise, there's a connection error.
			if(size == -1)
			{
				ERROR_PRINTF("Failed to receive RPC return\n");
			}
			DEBUG_PRINTF("Connection closed\n");
			BlitzRPCAsyncCallConnectionError(link);
		}
		return(-1);
	}

	//TODO(martin): make this size dynamic
	const int maxSize = 4096;
	char buffer[maxSize];

	uint32 oscSize = OscToInt32(oscSizeBuff);
	if(!OscIsValidElementSize(oscSize) || oscSize > maxSize)
	{
		ERROR_PRINTF("RPC reply data is bigger than user reply buffer\n");
		if(BlitzRPCDiscardExcessBytes(link, buffer, maxSize, oscSize) != 0)
		{
			//NOTE(martin): a connection error occured while purging the excess bytes
			ERROR_PRINTF("Error while purging the excess bytes of RPC reply");
			BlitzRPCAsyncCallConnectionError(link);
		}
		else
		{
			//TODO(martin): having the handle identifiers in the message makes this kind of errors
			//		unrecoverable.... this is problematic.

			//NOTE(martin): all went well, signal the out of bound message
			//BlitzRPCNotifyHandle(handle, RPC_STATUS_MEM);
			BlitzRPCAsyncCallConnectionError(link);
		}
		return(-1);
	}

	size = SocketReceive(link->sock, buffer, oscSize, 0);
	if(size != oscSize)
	{
		ERROR_PRINTF("Connection closed during message transfer\n");
		BlitzRPCAsyncCallConnectionError(link);
		return(-1);
	}

	osc_msg rcvMsg;
	if(OscParseMessage(&rcvMsg, oscSize, buffer) != 0)
	{
		ERROR_PRINTF("Can't parse osc message reply\n");
		BlitzRPCAsyncCallConnectionError(link);
		return(-1);
	}

	const char* tags = OscTypeTags(&rcvMsg);
	if(strcmp(tags, "hO"))
	{
		ERROR_PRINTF("Bad type string for osc message reply\n");
		//OscMessagePrint(&rcvMsg);
		BlitzRPCAsyncCallConnectionError(link);
		return(-1);
	}

	osc_arg_iterator arg = OscArgumentsBegin(&rcvMsg);
	uint64 callID = OscAsInt64Unchecked(&arg); OscArgumentNext(&arg);
	int32 callIndex = GetIndexFromCallID(callID);
	uint32 genIndex = GetGenFromCallID(callID);

	if(callIndex < 0 || callIndex >= BLITZ_MAX_PENDING_RPC)
	{
		ERROR_PRINTF("Bad call ID\n");
		BlitzRPCAsyncCallConnectionError(link);
		return(-1);
	}

	rpc_handle* handle = link->pendingCalls[callIndex];

	if(  handle == 0
	  || handle->retired != 0
	  || handle->genIndex != genIndex)
	{
		//NOTE(martin): this can happen if we canceled the call with BlitzRPCHandleClose()
		//		before a reply was received
		if(handle == 0)
		{
			ERROR_PRINTF("CallID associated with null handle\n");
		}
		{
			if(handle->retired)
			{
				ERROR_PRINTF("CallID associated with retired handle\n");
			}
			else
			{
				ERROR_PRINTF("Bad generation Index %i, supposed to be %i\n",
					    handle->genIndex,
					    genIndex);
			}
		}
		return(-1);
	}

	//NOTE(martin): extract the 'user' message, without the callID

	OscAsElementUnchecked(&arg, &handle->replyElement);
	BlitzRPCNotifyHandle(handle, RPC_STATUS_OK);

	return(0);
}

static void* BlitzRPCAsyncCallListener(void* p)
{
	DEBUG_ASSERT(p);

	rpc_link* link = (rpc_link*)p;

	//NOTE(martin): install a dummy signal handler, to unblock the recv if the call is canceled by BlitzRPCLinkClose()
	BlitzInstallDummySignalHandler();

	//NOTE(martin): wait the replies, dispatch them to the corresponding caller

	platform_socket* sd = link->sock;
	while(link->run)
	{
		//NOTE(martin): process the received replies
		BlitzRPCReceiveOscReply(link);

		//NOTE(martin): clean retired handles
		for(int i=0; i<BLITZ_MAX_PENDING_RPC; i++)
		{
			rpc_handle* handle = link->pendingCalls[i];
			if(handle)
			{
				if(handle->retired)
				{
					if(handle->mutex)
					{
						MutexDestroy(handle->mutex);
						ConditionDestroy(handle->condition);
					}
					free(handle);
					link->pendingCalls[i] = 0;
				}
			}
		}
	}

	//NOTE(martin): We were required to quit from the user's thread, which is currently blocked trying to join us.
	//		We deassociate all pending handles and free the retired ones

	for(int i=0; i<BLITZ_MAX_PENDING_RPC; i++)
	{
		rpc_handle* handle = link->pendingCalls[i];
		if(handle)
		{
			if(handle->retired)
			{
				if(handle->mutex)
				{
					MutexDestroy(handle->mutex);
					ConditionDestroy(handle->condition);
				}
				free(handle);
			}
			else
			{
				handle->link = 0;
				if(handle->status == RPC_STATUS_PENDING || handle->status == RPC_STATUS_BLOCKING)
				{
					handle->status = RPC_STATUS_FAIL;
				}
				//NOTE(martin): We don't awake blocking handles because we know that the user's thread
				//		is not in a blocking call
			}
		}
	}
	return(0);
}

static rpc_handle* BlitzRPCCreateHandle(rpc_link* link, rpc_status initialStatus, osc_element* request, uint32 replyMaxSize, char* replyBuffer)
{
	//NOTE(martin): Look for an available slot in the pending calls array

	int32 callIndex = -1;
	for(int i=0; i<BLITZ_MAX_PENDING_RPC; i++)
	{
		if(link->pendingCalls[i] == 0)
		{
			callIndex = i;
			break;
		}
	}
	if(callIndex == -1)
	{
		ERROR_PRINTF("Too many pending calls\n");
		return(0);
	}

	uint32 genIndex = link->genIndex++;
	uint64 callID = MakeCallID(callIndex, genIndex);

	//NOTE(martin): Create the rpc handle

	rpc_handle* handle = (rpc_handle*)malloc(sizeof(rpc_handle));
	handle->replyMaxSize = replyMaxSize;
	handle->replyBuffer = replyBuffer;
	handle->status = initialStatus;
	handle->retired = 0;
	handle->link = link;
	handle->callback = link->callback;
	handle->userPointer = link->userPointer;
	handle->genIndex = genIndex;

	if(initialStatus == RPC_STATUS_BLOCKING)
	{
		handle->mutex = MutexCreate();
		handle->condition = ConditionCreate();
	}
	else
	{
		handle->mutex = 0;
		handle->condition = 0;
	}

	//NOTE(martin): Commit the handle to the pending calls array

	link->pendingCalls[callIndex] = handle;

	//NOTE(martin): Send the RPC request

	uint32 maxSize = OscRoundUp4(strlen("/REQ")+1)
	                 + 4				// ",hO"
			 + sizeof(int64)		// size of callID
			 + sizeof(int32)		// nested element size slot
			 + OscElementSize(request)	// nested element data slot
			 + OSC_COMPOSE_OVERHEAD ;

	char* requestBuffer = malloc_array(char, maxSize);

	osc_element sndMsg;
	OscFormat(&sndMsg, maxSize, requestBuffer, "/REQ", "hO", callID, request);

	//TODO(martin): check error !

	if(SendOscPacket(link->sock, &sndMsg) != 0)
	{
		ERROR_PRINTF("Failed to send RPC call");
		SocketClose(link->sock);
		link->sock = 0;
		free(requestBuffer);
		handle->retired = 1; //NOTE(martin): the handle will be destroyed in the listener thread
		return(0);
	}
	free(requestBuffer);
	return(handle);
}

//------------------------------------------------------------------------------------------
// RPC Link / handle API
//------------------------------------------------------------------------------------------


/**
	@brief	Opens an RPC Link

	@param[in] context	A blitz context, created by BlitzContextCreate()
	@param[in] path		The service path.

	@return If no error occured, an RPC link is returned, otherwise zero is returned. The link can then be used to call the RPC service.
*/

rpc_link* BlitzRPCOpenLink(blitz_context* context, const char* path)
{
	DEBUG_ASSERT(context);
	DEBUG_ASSERT(path);

	if(BlitzConnect(context) != 0)
	{
		return(0);
	}

	const int buffSize = 4096;
	char sndBuff[buffSize];
	char rcvBuff[buffSize];
	osc_element request;
	osc_msg reply;
	host_ip ip = 0;
	int iport = 0;

	OscFormat(&request, buffSize, sndBuff, "/getrpc", "s", path);
	OscMessagePrepare(&reply, buffSize, rcvBuff);
	BlitzDirectoryRequest(context, &request, &reply);

	if(OscScan(&reply, "/OK!", "ii", &ip, &iport) != OSC_OK)
	{
		//TODO(martin): handle error...
	}
	host_port port = (host_port)iport;

	BlitzDisconnect(context);

	//NOTE(martin): try to connect to the service

	platform_socket* sock = SocketOpen(SOCK_TCP);
	if(!sock)
	{
		perror("BlizRPCLinkOpen() : Couldn't create link socket");
		return(0);
	}
	socket_address addr;
	addr.ip = HostToNetIP(ip);
	addr.port = HostToNetPort(port);

	DEBUG_PRINTF("Received response from the directory server. Service %s is at %s:%u\n",
		     path,
		     NetIPToString(addr.ip),
		     addr.port);

	if(SocketConnect(sock, &addr) != 0)
	{
		ERROR_PRINTF("Can't connect to the service\n");
		SocketClose(sock);
		return(0);
	}

	rpc_link* link = malloc_type(rpc_link);
	link->run = true;
	link->address = addr;
	link->sock = sock;

	link->callback = 0;
	link->userPointer = 0;

	link->genIndex = 0;
	memset(link->pendingCalls, 0, BLITZ_MAX_PENDING_RPC * sizeof(rpc_handle*));

	//NOTE(martin): launch the async listener thread

	link->thread = ThreadCreate(BlitzRPCAsyncCallListener, link);
	if(!link->thread)
	{
		ERROR_PRINTF("BlizRPCLinkOpen() : Couldn't create listener thread\n");
		free(link);
		SocketClose(sock);
		return(0);
	}
	return(link);
}

/**
	@brief	Closes an RPC Link

	@param[in] context	The blitz context, in which the link was created.
	@param[in] link		The RPC link to close.

	@return Zero if no error occured.

	An RPC Link MUST be closed in the same thread as the calls to BlitzRPCCall(), BlitzRPCCallAsync(), or BlitzRPCHandleClose()
	Closing a link will mark all pending calls as failed and call the callback (if any). Call handles can still be polled after the link is closed.

*/

int BlitzRPCCloseLink(blitz_context* context, rpc_link* link)
{
	DEBUG_ASSERT(context);
	if(link)
	{
		//NOTE(martin): we unblock the listener and wait for him to exit.
		link->run = false;
		ThreadSignal(link->thread, SIGUSR1);
		void* res;
		ThreadJoin(link->thread, &res);
		ThreadDestroy(link->thread);

		//NOTE(martin): clean up
		SocketClose(link->sock);
		free(link);
	}
	return(0);
}

/**
	@brief	Set the RPC Link callback

	@param[in] link		The RPC link, previously opened with BlitzRPCLinkOpen()
	@param[in] callback	The link callback, which will be called when an asynchronous call returns
	@param[in] userPointer	A pointer that will be passed to the callback. It allows to provide it some (user-defined) context

	@return Zero if no error occurred.

	Changing the callback when calls are pending can result in a callback being called with the wrong user pointer. Don't do that.
*/
int BlitzRPCLinkSetCallback(rpc_link* link, RPCLinkCallback callback, void* userPointer)
{
	if(!link)
	{
		return(-1);
	}
	link->callback = callback;
	link->userPointer = userPointer;
	return(0);
}

/**
	@brief	Synchronous Remote Procedure Call

	@param[in] link		The RPC link, previously opened with BlitzRPCLinkOpen()
	@param[in] request	The OSC request to send to the RPC server
	@param[out] reply	The OSC message that will hold the parsed reply upon return
	@param[in] replyMaxSize The size of the replyBuffer.
	@param[out] replyBuffer	The reply buffer, which will contain the raw OSC data of the reply.

	@return 0 on success, otherwise an error code is returned.
*/
rpc_status BlitzRPCCall(rpc_link* link, osc_element* request, osc_element* reply, uint32 replyMaxSize, char* replyBuffer)
{
	DEBUG_ASSERT(replyBuffer);
	if(!link)
	{
		return(RPC_STATUS_FAIL);
	}

	//NOTE(martin): check the link connection
	if(!link->sock)
	{
		platform_socket* sock = SocketOpen(SOCK_TCP);
		if(!sock)
		{
			return(RPC_STATUS_FAIL);
		}

		if(SocketConnect(sock, &link->address) != 0)
		{
			perror("Failed connecting service socket");
			SocketClose(sock);
			return(RPC_STATUS_FAIL);
		}
		link->sock = sock;
	}


	rpc_handle* handle = BlitzRPCCreateHandle(link, RPC_STATUS_BLOCKING, request, replyMaxSize, replyBuffer);
	if(!handle)
	{
		ERROR_PRINTF("Couldn't create rpc handle\n");
		return(RPC_STATUS_FAIL);
	}

	//NOTE(martin): take the mutex and check if the call already returned
	//		if not, we wait on the condition until being awaken by the listener thread

	MutexLock(handle->mutex);

	if(!link->sock)
	{
		//NOTE(martin): a connection error occurred in the listener thread, we should not wait, and mark us as failed
		ERROR_PRINTF("A connection error occurred\n");
		handle->status = RPC_STATUS_FAIL;
	}
	else if(handle->status == RPC_STATUS_BLOCKING)
	{
		ConditionWait(handle->condition, handle->mutex);
	}

	rpc_status status = handle->status;
	if(status == RPC_STATUS_OK)
	{
		*reply = handle->replyElement;
	}

	MutexUnlock(handle->mutex);

	return(status);
}


/**
	@brief	Asynchronously call an RPC service

	@param[in] link		The RPC link, previously opened with BlitzRPCLinkOpen()
	@param[in] request	The request to send to the RPC server
	@param[in] replyMaxSize The size of the replyBuffer
	@param[in] replyBuffer	The reply buffer, which will contain the raw OSC data of the reply.

	@return An rpc_handle pointer, which can then be used to poll the results of the call.

	The user owns the memory passed in replyBuffer. The asynchronous call only write results here, but don't manage the memory in any way.
	Calls on a RPC link MUST happen in the same thread. Thus if different threads need to call the same service, they should each open their own link.

*/
rpc_handle* BlitzRPCCallAsync(rpc_link* link, osc_element* request, uint32 replyMaxSize, char* replyBuffer)
{
	if(!link)
	{
		return(0);
	}

	//NOTE(martin): check the link connection
	if(!link->sock)
	{
		platform_socket* sock = SocketOpen(SOCK_TCP);
		if(!sock)
		{
			return(0);
		}

		if(SocketConnect(sock, &link->address) != 0)
		{
			perror("Failed connecting service socket");
			SocketClose(sock);
			return(0);
		}
		link->sock = sock;
	}

	return(BlitzRPCCreateHandle(link, RPC_STATUS_PENDING, request, replyMaxSize, replyBuffer));
}

/**
	@brief	Get the reply buffer and status of an asynchronous RPC

	@param	    handle	The RPC handle
	@param[out] reply	The osc_msg struct that will hold the parsing infos for the OSC message of the reply. The actual data of the reply is contained inside the replyBuffer that was passed to BlitzRPCCallAsync().

	@return The status of the call, which can be RPC_STATUS_PENDING, RPC_STATUS_FAIL, RPC_STATUS_OK

	Polling must happen in the same thread as the call.
*/
rpc_status BlitzRPCPollResult(rpc_handle* handle, osc_element* reply)
{
	if(!handle || handle->retired)
	{
		return(RPC_STATUS_FAIL);
	}
	//NOTE(martin): it has to be an if/else if/else so that the returned status can't change after the test
	if(handle->status == RPC_STATUS_FAIL)
	{
		return(RPC_STATUS_FAIL);
	}
	else if(handle->status == RPC_STATUS_OK)
	{
		DEBUG_ASSERT(reply);
		*reply = handle->replyElement;
		return(RPC_STATUS_OK);
	}
	else
	{
		return(RPC_STATUS_PENDING);
	}
}

/**
	@brief	Closes an RPC call handle and free the resources associated with it

	@param[in] handle	The RPC handle
	@return Zero if no error occurred

	If the call is still pending, it will be canceled.
	A handle MUST be closed in the same thread as the call OR in the callback resulting from that call.

*/
int BlitzRPCCloseHandle(rpc_handle* handle)
{
	if(!handle)
	{
		return(-1);
	}
	if(handle->link)
	{
		//NOTE(martin): we mark the handle as retired and signal the listener thread to do the clean-up job
		handle->retired = 1;
		ThreadSignal(handle->link->thread, SIGUSR1);
	}
	else
	{
		//NOTE(martin): the handle is not associated with a listener thread, so we free it ourselves
		if(handle->mutex)
		{
			MutexDestroy(handle->mutex);
			ConditionDestroy(handle->condition);
		}
		free(handle);
	}
	return(0);
}

//------------------------------------------------------------------------------------------
// Channel publisher structs
//------------------------------------------------------------------------------------------

struct subscriber_record
{
	uint64 ID;
	net_ip ip;
	net_port port;
	//...
};

typedef std::vector<subscriber_record> subscriber_vec;

struct publisher_link
{
	bool run;
	platform_thread* thread;
	platform_socket* recvSock;
	platform_socket* sendSock;
	uint64 channelID;
	uint64 publisherID;
	char* path;
	uint32 sequenceNumber;

	subscriber_vec subscribers;

	publisher_link(platform_socket* notifySock, const char* path_)
	{
		run = true;
		recvSock = notifySock;
		channelID = 0;
		publisherID = 0;
		path = (char*)malloc(strlen(path_)+1);
		strcpy(path, path_);
		sequenceNumber = 1;
	}
	~publisher_link()
	{
		void* ret;
		run = false;
		ThreadSignal(thread, SIGUSR1);
		ThreadJoin(thread, &ret);

		if(recvSock)
		{
			SocketClose(recvSock);
		}
		if(sendSock)
		{
			SocketClose(sendSock);
		}
		free(path);
	}

};

//------------------------------------------------------------------------------------------
// Publisher Internals
//------------------------------------------------------------------------------------------

static void* BlitzPublisherNotificationListener(void* ptr)
{
	DEBUG_ASSERT(ptr);

	//NOTE(martin): install a dummy signal handler, so that we can break blocking io from the main tread
	BlitzInstallDummySignalHandler();


	publisher_link* link = (publisher_link*)ptr;
	platform_socket* sock = link->recvSock;

	//NOTE(martin): initialize our activities array

	const int maxClients = 1024;

	socket_activity activities[maxClients];
	memset(activities, 0, maxClients*sizeof(socket_activity));

	activities[0].sock = sock;
	activities[0].watch = SOCK_ACTIVITY_IN;

	//NOTE(martin): process clients
	//TODO(martin): handle bigger requests ?

	const int buffSize = 1024;
	char buffer[buffSize];

	while(link->run)
	{
		//NOTE(martin): wait for IO operations on clients or main socket with select()

		if(SocketSelect(maxClients, activities, 0) <= 0)
		{
			continue;
		}

		//NOTE(martin): check IO operations on main socket
		if(activities[0].set)
		{
			//NOTE(martin): this is an incomming connection, add it to the clients array
			socket_address from;
			platform_socket* connection = SocketAccept(sock, &from);
			//TODO(martin): check error

			for(int i=1; i<maxClients; i++)
			{
				if(activities[i].sock == 0)
				{
					activities[i].sock = connection;
					activities[i].watch = SOCK_ACTIVITY_IN;
					break;
				}
			}
			DEBUG_PRINTF("New connection from %s\n", NetIPToString(from.ip));
		}

		//NOTE(martin): check IO operations on the clients
		for(int i=1; i<maxClients; i++)
		{
			socket_activity* activity = &(activities[i]);

			if(activity->sock && activity->set)
			{
			//TODO(martin): Could simplify !!!

				char oscSizeBuff[4];
				int64 size = SocketReceive(activity->sock, oscSizeBuff, sizeof(uint32), 0);

				if(size < sizeof(uint32))
				{
					//NOTE(martin): this is a disconnection
					//TODO(martin): handle differently when size > 0 but < sizeof(rpc_request_msg) (eg send a failed reply)
					SocketClose(activity->sock);
					activity->sock = 0;
				}
				else
				{
					uint32 oscSize = OscToInt32(oscSizeBuff);
					size = SocketReceive(activity->sock, buffer, oscSize, 0);

					if(size<0)
					{
						SocketClose(activity->sock);
						activity->sock = 0;
					}
					else
					{
						osc_msg msg;
						OscParseMessage(&msg, size, buffer);
						if(!strcmp(OscAddressPattern(&msg), "/sub"))
						{
							//TODO(martin): check typeString
							DEBUG_PRINTF("received a notification to add a subscriber\n");
							osc_arg_iterator arg = OscArgumentsBegin(&msg);
							host_ip ip = OscAsInt32Unchecked(&arg); OscArgumentNext(&arg);
							host_port port = OscAsInt32Unchecked(&arg);

							subscriber_record record;
							record.ip = HostToNetIP(ip);
							record.port = HostToNetPort(port);
							link->subscribers.push_back(record);
						}
						else if(!strcmp(OscAddressPattern(&msg), "/unsub"))
						{
							//TODO(martin): check typeString

							osc_arg_iterator arg = OscArgumentsBegin(&msg);
							host_ip ip = OscAsInt32Unchecked(&arg); OscArgumentNext(&arg);
							host_port port = OscAsInt32Unchecked(&arg);

							DEBUG_PRINTF("received a notification to remove a subscriber (%s:%hu)\n",
							             NetIPToString(HostToNetIP(ip)), port);

							for(subscriber_vec::iterator it = link->subscribers.begin();
							    it != link->subscribers.end();
							    it++)
							{
								if(it->ip == HostToNetIP(ip) && it->port == HostToNetPort(port))
								{
									link->subscribers.erase(it);
									break;
								}
							}
						}
						else
						{
							ERROR_PRINTF("bad notification message from directory");
							//erreur
						}
					}
				}
			}
		}
	}
	//NOTE(martin): close remaining clients and main socket
	for(int i=1; i<maxClients; i++)
	{
		if(activities[i].sock)
		{
			SocketClose(activities[i].sock);
		}
	}
	//NOTE(martin): main socket is closed when the publisher is destroyed
	return(0);
}

//------------------------------------------------------------------------------------------
// Channel Publisher API
//------------------------------------------------------------------------------------------

/**
	@brief Creates a channel publisher and registers it to the service directory

	@param[in] context	The blitz context
	@param[in] path		The path of the service

	@return A link to the publisher, which can be used to publish data on the channel
*/

publisher_link* BlitzOpenPublisher(blitz_context* context, const char* path)
{
	DEBUG_ASSERT(context);
	DEBUG_ASSERT(path);
	//NOTE(martin): create a socket for receiving subscription notifications from the directory

	socket_address address;
	address.ip = SOCK_IP_ANY;
	address.port = SOCK_PORT_ANY;

	platform_socket* notifySock = SocketOpen(SOCK_TCP);

	SocketBind(notifySock, &address);
	SocketGetAddress(notifySock, &address);
	SocketListen(notifySock, 100);

	//NOTE(martin): launch the notification listener thread

	publisher_link* publisher = new publisher_link(notifySock, path);

	publisher->thread = ThreadCreate(BlitzPublisherNotificationListener, publisher);

	//NOTE(martin): register our publisher to the service directory

	BlitzConnect(context);
	{
		const int buffSize = 4096;
		char sndBuff[buffSize];
		char rcvBuff[buffSize];
		osc_element request;
		osc_msg reply;
		uint64 pubID = 0;

		OscFormat(&request, buffSize, sndBuff, "/regpub", "si", path, NetToHostPort(address.port));
		OscMessagePrepare(&reply, buffSize, rcvBuff);
		BlitzDirectoryRequest(context, &request, &reply);

		OscMessagePrint(&reply);
		if(OscScan(&reply, "/OK!", "h", &pubID) != OSC_OK)
		{
			ERROR_PRINTF("Error reply from service directory...\n");
			//TODO(martin): handle error...
		}

		//publisher->channelID = channelID;
		publisher->publisherID = pubID;

		WARNING_PRINTF("publisherID = %llx\n", pubID);
	}
	BlitzDisconnect(context);

	//NOTE(martin): create the send socket

	publisher->sendSock = SocketOpen(SOCK_UDP);

	if(!publisher->sendSock)
	{
		delete publisher;
		return(0);
	}
	return(publisher);
}

/**
	@brief Closes a channel publisher and unregisters it from the service directory

	@param[in] context	The blitz context
	@param[in] publisher	A publisher link, that was returned by a call to BlitzRegisterPublisher()

	@return Zero if no error occured
*/

int BlitzClosePublisher(blitz_context* context, publisher_link* publisher)
{
	DEBUG_ASSERT(context);

	if(!publisher)
	{
		return(-1);
	}
	BlitzConnect(context);
	{
		const int buffSize = 4096;
		char sndBuff[buffSize];
		char rcvBuff[buffSize];
		osc_element request;
		osc_msg reply;

		OscFormat(&request, buffSize, sndBuff, "/unpub", "h", publisher->publisherID);
		OscMessagePrepare(&reply, buffSize, rcvBuff);
		BlitzDirectoryRequest(context, &request, &reply);
		if(OscScan(&reply, "/OK!") != OSC_OK)
		{
			//TODO(martin): handle error...
		}
	}
	BlitzDisconnect(context);

	delete publisher;
	return(0);
}

/**
	@brief Publishes a message on a channel

	@param[in] link		A publisher link, created by BlitzRegisterPublisher()
	@param[in] msg		The OSC message to send to the channel

	@return Zero if no error occured. Otherwise a socket error code could be returned.
*/

static int BlitzPublishXMessage(publisher_link* link, osc_element* msg)
{
	//NOTE(martin): increment and wrap the publisher's sequence number
	link->sequenceNumber++;
	if(link->sequenceNumber == 0)
	{
		link->sequenceNumber++;
	}

	//NOTE(martin): run through the list of client and send them udp messages each in turn...

	platform_socket* sendSock = link->sendSock;
	int ret = 0;

	for(subscriber_vec::iterator it = link->subscribers.begin();
	    it != link->subscribers.end();
	    it++)
	{
		subscriber_record& sub = *it;

		socket_address to;
		to.ip = sub.ip;
		to.port = sub.port;

		ret |= SocketSendTo(sendSock, (void*)OscElementData(msg), OscElementSize(msg), 0, &to);
	}
	return(ret);
}

int BlitzPublish(publisher_link* link, uint64 timetag, const char* pattern, const char* fmt, ...)
{
	DEBUG_ASSERT(link);
	DEBUG_ASSERT(fmt);

	const int buffSize = 4096;
	char buffer[buffSize];
	osc_element msg;


	///////////////////////////////////////////////////////////////////


	//TODO(martin): we nest the message in a rather inefficient way.
	//		we should add more efficient nesting composition in osc.h
	char userBuffer[buffSize];
	osc_element userMsg;

	va_list ap;
	va_start(ap, fmt);
	OscFormatVA(&userMsg, buffSize, userBuffer, pattern, fmt, ap);
	va_end(ap);

	//TODO(martin): check/sanitize format string...
	OscBeginPacket(&msg, buffSize, buffer);

		OscBeginMessage(&msg, link->path);
			OscPushInt64Variant(&msg, 'X', OSC_BLITZ_MARKER);
			OscPushTimeTag(&msg, timetag);
			OscPushInt32(&msg, link->sequenceNumber);

			OscPushElement(&msg, &userMsg);
		OscEndMessage(&msg);
	OscEndPacket(&msg);

	return(BlitzPublishXMessage(link, &msg));
}

int BlitzPublishArguments(publisher_link* link, uint64 timetag, const char* pattern, int argc, osc_argument* argv)
{
	DEBUG_ASSERT(link);
	DEBUG_ASSERT(argv);

	const int buffSize = 4096;
	char buffer[buffSize];
	osc_element msg;

	//TODO(martin): we nest the message in a rather inefficient way.
	//		we should add more efficient nesting composition in osc.h
	char userBuffer[buffSize];
	osc_element userMsg;

	OscBeginPacket(&userMsg, buffSize, userBuffer);
		OscBeginMessage(&userMsg, pattern);
			OscPushA(&msg, argc, argv);
		OscEndMessage(&userMsg);
	OscEndPacket(&userMsg);

	//TODO(martin): check/sanitize format string...
	OscBeginPacket(&msg, buffSize, buffer);
		OscBeginMessage(&msg, link->path);
			OscPushInt64Variant(&msg, 'X', OSC_BLITZ_MARKER);
			OscPushTimeTag(&msg, timetag);
			OscPushInt32(&msg, link->sequenceNumber);

			OscPushElement(&msg, &userMsg);
		OscEndMessage(&msg);
	OscEndPacket(&msg);

	return(BlitzPublishXMessage(link, &msg));
}


int BlitzPublishMessage(publisher_link* link, uint64 timetag, osc_msg* msg)
{
	DEBUG_ASSERT(msg);

	osc_element elt;
	OscParsePacket(&elt, OscMessageSize(msg), OscMessageData(msg));
	return(BlitzPublishElement(link, timetag, &elt));
}

int BlitzPublishElement(publisher_link* link, uint64 timetag, osc_element* element)
{
	DEBUG_ASSERT(link);
	DEBUG_ASSERT(element);

	const int buffSize = 4096;
	char buffer[buffSize];
	osc_element msg;

	//TODO(martin): check/sanitize format string...
	OscBeginPacket(&msg, buffSize, buffer);
		OscBeginMessage(&msg, link->path);
			OscPushInt64Variant(&msg, 'X', OSC_BLITZ_MARKER);
			OscPushTimeTag(&msg, timetag);
			OscPushInt32(&msg, link->sequenceNumber);

			OscPushElement(&msg, element);
		OscEndMessage(&msg);
	OscEndPacket(&msg);

	return(BlitzPublishXMessage(link, &msg));
}


//------------------------------------------------------------------------------------------
// Channel Subscriber structs
//------------------------------------------------------------------------------------------

struct subscriber_link
{
	bool run;
	platform_socket* sock;
	host_ip ip;	// ip and port as registered to the service directory
	host_port port;
	platform_thread* thread;
	uint64 channelID;
	uint64 subscriberID;
	char* path;

	ChannelSubscriberCallback callback;
	void* userPointer;

	subscriber_link(const char* path_, ChannelSubscriberCallback callback_, void* userPointer_)
	{
		run = true;
		callback = callback_;
		userPointer = userPointer_;
		path = (char*)malloc(strlen(path_)+1);
		strcpy(path, path_);

		thread = 0;
		sock = 0;
	}
	~subscriber_link()
	{
		if(thread)
		{
			void* ret;
			run = false;
			ThreadSignal(thread, SIGUSR1);
			ThreadJoin(thread, &ret);
		}
		if(sock)
		{
			SocketClose(sock);
		}
		free(path);
	}
};

//------------------------------------------------------------------------------------------
// Channel Subscriber Internals
//------------------------------------------------------------------------------------------

static void* BlitzSubscriberListener(void* ptr)
{
	DEBUG_ASSERT(ptr);

	//NOTE(martin): install a dummy signal handler, so that we can break blocking io from the main tread
	BlitzInstallDummySignalHandler();


	// get updates from the publishers, and call the callback function

	subscriber_link* link = (subscriber_link*)ptr;
	platform_socket* sock = link->sock;

	//TODO(martin): the user should be the one who provides the memory we need !!
	const int buffSize = 1024;
	char buffer[buffSize];

	while(link->run)
	{
		socket_address addr;
		int64 size = SocketReceiveFrom(sock, buffer, buffSize, 0, &addr);
		if(size >= 0)
		{
			//DEBUG_PRINTF("received a message from publisher %s\n", NetIPToString(addr.ip));

			osc_element element;
			if(OscParsePacket(&element, size, buffer) != 0)
			{
				ERROR_PRINTF("Couldn't parse received packet as an OSC bundle\n");
				continue;
			}
			const char* addressPattern = 0;
			const char* typeTags = 0;
			uint64 timetag = 0;
			uint32 seqNumber = 0;

			if(OscElementIsBundle(&element))
			{
				WARNING_PRINTF("Don't support bundles yet... ignore\n");
				continue;
			}
			else
			{
				osc_msg msg;

				if(OscParseMessage(&msg, size, buffer) != 0)
				{
					ERROR_PRINTF("Couldn't parse received packet as an OSC message\n");
					continue;
				}
				addressPattern = OscAddressPattern(&msg);
				typeTags = OscTypeTags(&msg);
				osc_arg_iterator arg = OscArgumentsBegin(&msg);

				if(typeTags[0] == 'X')
				{
					//NOTE(martin): blitz extended format

					uint64 marker = OscAsInt64Unchecked(&arg); OscArgumentNext(&arg);

					if(marker != OSC_BLITZ_MARKER)
					{
						WARNING_PRINTF("Unknown blitz version - ignore message\n");
						continue;
					}
					//TODO(martin): check that we conform to the current version...

					timetag = OscAsInt64Unchecked(&arg); OscArgumentNext(&arg);
					seqNumber = OscAsInt32Unchecked(&arg); OscArgumentNext(&arg);

					OscAsElementUnchecked(&arg, &element);
				}

				if(link->callback)
				{
					link->callback(addressPattern, timetag, &element, link->userPointer);
				}
			}
		}
		else
		{
			WARNING_PRINTF("SocketReceiveFrom() returned an error : %s", SocketGetLastErrorMessage());
		}
	}
	//...
	return(0);
}

static void SubscribeError(platform_socket* sock, subscriber_link* subscriber)
{
	if(sock)
	{
		SocketClose(sock);
	}
	if(subscriber->sock)
	{
		SocketClose(subscriber->sock);
	}
	if(subscriber->thread)
	{
		void* ret;
		subscriber->run = false;
		ThreadSignal(subscriber->thread, SIGUSR1);
		ThreadJoin(subscriber->thread, &ret);
	}
	free(subscriber);
}

//------------------------------------------------------------------------------------------
// Channel Subscriber API (subscriber side)
//------------------------------------------------------------------------------------------

/**
	@brief Creates a subscriber to a channel service

	@param[in] context	The blitz context
	@param[in] path		The path to the channel service
	@param[in] callback	The callback that will be called when a message is published on the channel
	@param[in] userPointer 	A pointer that will be passed to the callback. It allows to provide it some (user-defined) context

	@return A link to the subscriber, or 0 if an error occured.
*/

subscriber_link* BlitzSubscribe(blitz_context* context, const char* path, ChannelSubscriberCallback callback, void* userPointer)
{
	DEBUG_ASSERT(context);
	DEBUG_ASSERT(path);

	//NOTE(martin): open the listening socket and launch the listener thread

	subscriber_link* subscriber = new subscriber_link(path, callback, userPointer);

	socket_address addr;
	addr.ip = SOCK_IP_ANY;
	addr.port = SOCK_PORT_ANY;

	subscriber->sock = SocketOpen(SOCK_UDP);
	if(!subscriber->sock)
	{
		delete subscriber;
		return(0);
	}
	if(SocketBind(subscriber->sock, &addr))
	{
		delete subscriber;
		return(0);
	}
	if(SocketGetAddress(subscriber->sock, &addr))
	{
		delete subscriber;
		return(0);
	}

	subscriber->thread = ThreadCreate(BlitzSubscriberListener, subscriber);
	if(!subscriber->thread)
	{
		delete subscriber;
		return(0);
	}

	//NOTE(martin): send the subscribe request to the service directory

	BlitzConnect(context);
	{
		const int buffSize = 4096;
		char sndBuff[buffSize];
		char rcvBuff[buffSize];
		osc_element request;
		osc_msg reply;
		uint64 subID = 0;

		OscFormat(&request, buffSize, sndBuff, "/sub", "si", path, NetToHostPort(addr.port));
		OscMessagePrepare(&reply, buffSize, rcvBuff);
		BlitzDirectoryRequest(context, &request, &reply);
		if(OscScan(&reply, "/OK!", "h", &subID) != OSC_OK)
		{
			//TODO(martin): handle error...
		}

		//publisher->channelID = channelID;
		subscriber->subscriberID = subID;

		DEBUG_PRINTF("received service directory reply, subscriberID = %llu\n", subID);
	}
	BlitzDisconnect(context);

	return(subscriber);
}

/**
	@brief Closes a subscriber

	@param[in] context	The blitz context
	@param[in] subscriber	The subscriber link, created by a call to BlitzSubscribe()

	@return Zero if no error occured
*/

int BlitzUnsubscribe(blitz_context* context, subscriber_link* subscriber)
{
	DEBUG_ASSERT(context);

	if(!subscriber)
	{
		return(-1);
	}

	BlitzConnect(context);
	{
		//NOTE(martin): send notification to all publishers

		const int buffSize = 4096;
		char sndBuff[buffSize];
		char rcvBuff[buffSize];
		osc_element request;
		osc_msg reply;

		OscFormat(&request, buffSize, sndBuff, "/unsub", "h", subscriber->subscriberID);
		OscMessagePrepare(&reply, buffSize, rcvBuff);
		BlitzDirectoryRequest(context, &request, &reply);
		if(OscScan(&reply, "/OK!") != OSC_OK)
		{
			//TODO(martin): handle error...
		}
	}
	BlitzDisconnect(context);

	delete subscriber; // NOTE(martin): join the subscriber thread and free its resources
	return(0);
}


} // extern "C"
