/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: platform_thread.h
*	@author: Martin Fouilleul
*	@date: 21/03/2019
*	@revision:
*
*****************************************************************/
#ifndef __PLATFORM_THREAD_H_
#define __PLATFORM_THREAD_H_

//---------------------------------------------------------------
// Inline architecture-specific fence intrinsics
//---------------------------------------------------------------
#if __i386__ || __x86_64__

#include<emmintrin.h>
inline void PlatformMemoryFence()
{
	_mm_mfence();
}

#else // (not) (__i386__ || __x86_64__)

#error "PlatformMemoryFence() is not implemented yet for the target architecture"

#endif // __i386__ || __x86_64__

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

//---------------------------------------------------------------
// Platform Thread API
//---------------------------------------------------------------

struct platform_thread;

typedef void* (*ThreadStartFunction)(void* userPointer);

platform_thread* ThreadCreate(ThreadStartFunction start, void* userPointer);
int ThreadDestroy(platform_thread* thread);

int ThreadSignal(platform_thread* thread, int sig);
int ThreadJoin(platform_thread* thread, void** ret);

//---------------------------------------------------------------
// Platform Mutex API
//---------------------------------------------------------------

struct platform_mutex;

platform_mutex* MutexCreate();
int MutexDestroy(platform_mutex* mutex);
int MutexLock(platform_mutex* mutex);
int MutexUnlock(platform_mutex* mutex);

//---------------------------------------------------------------
// Platform condition variable API
//---------------------------------------------------------------

struct platform_condition;

platform_condition* ConditionCreate();
int ConditionDestroy(platform_condition* cond);
int ConditionWait(platform_condition* cond, platform_mutex* mutex);
int ConditionSignal(platform_condition* cond);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif //__PLATFORM_THREAD_H_
