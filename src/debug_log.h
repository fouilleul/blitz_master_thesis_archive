/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: debug_log.h
*	@author: Martin Fouilleul
*	@date: 05/04/2019
*	@revision:
*
*****************************************************************/
#ifndef __DEBUG_LOG_H_
#define __DEBUG_LOG_H_

#include<stdio.h>

#ifndef NO_ASSERT
#include<assert.h>
#define ASSERT(x) assert(x)
#else
#define ASSERT(x)
#endif

#define _WARNING_PRINTF_(func, file, line, msg, ...) printf( "\e[38;5;13m\e[1mWarning :\e[m %s() in %s line %i : ", func, file, line); printf(msg, ##__VA_ARGS__ )
#define WARNING_PRINTF(msg, ...) _WARNING_PRINTF_( __FUNCTION__ , __FILE__, __LINE__, msg, ##__VA_ARGS__ )

#define _ERROR_PRINTF_(func, file, line, msg, ...) fprintf(stderr, "\e[38;5;9m\e[1mError :\e[m %s() in %s line %i : ", func, file, line); fprintf(stderr, msg, ##__VA_ARGS__ )
#define ERROR_PRINTF(msg, ...) _ERROR_PRINTF_( __FUNCTION__ , __FILE__, __LINE__, msg, ##__VA_ARGS__ )


#ifdef DEBUG
	template<bool B>
	struct StaticAssert;
	template<> struct StaticAssert<true>{};

	#define STATIC_ASSERT(x) StaticAssert<x>()
	#define DEBUG_ASSERT(x) ASSERT(x)

	#define _DEBUG_PRINTF_(func, file, line, msg, ...) printf("\e[38;5;14m\e[1mDebug :\e[m %s() in %s line %i : ", func, file, line); printf(msg, ##__VA_ARGS__)
	#define DEBUG_PRINTF(msg, ...) _DEBUG_PRINTF_( __FUNCTION__ , __FILE__, __LINE__, msg, ##__VA_ARGS__ )
	#define DEBUG_WARNING(msg, ...) WARNING_PRINTF(msg, ##__VA_ARGS__ )
#else
	#define STATIC_ASSERT(x)
	#define DEBUG_ASSERT(x)
	#define DEBUG_PRINTF(msg, ...)
	#define DEBUG_WARNING(msg, ...)
#endif

#endif //__DEBUG_LOG_H_
