/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: huffpuff_filter.h
*	@author: Martin Fouilleul
*	@date: 28/06/2019
*	@revision:
*
*****************************************************************/
#ifndef __HUFFPUFF_FILTER_H_
#define __HUFFPUFF_FILTER_H_

#include"typedefs.h"

//--------------------------------------------------------------------------------
// huff-n'-puff filter
//--------------------------------------------------------------------------------

const int HUFF_PUFF_PERIOD = 10,   // Huff-n'-puff filter sampling period (s)
          HUFF_PUFF_WINDOW = 7200; // Huff-n'-puff sliding window length (s)

const int HUFF_PUFF_SIZE = HUFF_PUFF_WINDOW / HUFF_PUFF_PERIOD ; // Huff-n'-puff buffer length

struct huff_puff_filter
{
	f64 samples[HUFF_PUFF_SIZE];
	u32 currentIndex;
	f64 minSample;
	u64 timer;
};

void HuffPuffInit(huff_puff_filter* filter);
void HuffPuffSlide(huff_puff_filter* filter, u64 currentTime);
f64 HuffPuffFilter(huff_puff_filter* filter, f64 delay, f64 offset);


#endif //__HUFFPUFF_FILTER_H_
