/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: clock_filter.h
*	@author: Martin Fouilleul
*	@date: 13/03/2019
*	@revision:
*
*****************************************************************/
#ifndef __CLOCK_FILTER_H_
#define __CLOCK_FILTER_H_

#include"typedefs.h"


//NOTE(martin): move these constants elsewhere

const i64 MAX_DISP	   = 16,
	  PACKET_PRECISION = -20,	//TODO(martin): log precision of the server. Set it in the server
	  SYSTEM_PRECISION = -20,	//TODO(martin): log precision of reading the system clock
	  SYSTEM_POLL	   = 1,		// min poll exponent (4s)
	  SGATE		   = 3;		// Spike gate

const f64 SYSTEM_PHI	   = 15.e-6;	//TODO(martin): clock frequency drift, assumed to be 15ppm ??

const unsigned int CLOCK_FILTER_SIZE = 8;

struct clock_filter_value
{
	f64 offset;	// clock offset
	f64 delay;	// round trip delay
	f64 disp;	// maximum error due to the frequency tolerance and time since the last packet was sent
	u64 epoch;	// time at which the sample was received, according to seconds counter (s)
};

struct clock_filter_result
{
	f64 offset;	// clock offset
	f64 delay;	// round trip delay
	f64 disp;	// maximum error due to the frequency tolerance and time since the last packet was sent
	f64 jitter;	// jitter
	u64 epoch;	// time at which the sample was received (s)
};


struct clock_filter
{
	clock_filter_value reg[CLOCK_FILTER_SIZE];
	u64 lastUpdate;
};

void ClockFilterInit(clock_filter* filter);
bool ClockFilterPush(clock_filter* filter, clock_filter_value* val, clock_filter_result* result, uint8 pollTimeConstant);


#endif //__CLOCK_FILTER_H_
