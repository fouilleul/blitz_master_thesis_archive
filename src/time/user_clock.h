/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: user_clock.h
*	@author: Martin Fouilleul
*	@date: 28/06/2019
*	@revision:
*
*****************************************************************/
#ifndef __USER_CLOCK_H_
#define __USER_CLOCK_H_

#include<math.h>	// fabs()
#include"typedefs.h"
#include"platform_time.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


//--------------------------------------------------------------------------------
// user clock
//--------------------------------------------------------------------------------

struct user_clock
{
	i64 offset;
	i64 offsetTarget;
	u64 lastAdjust;
	f64 slewRate;		// user seconds per system second
	f64 residual;		// adjustment residue (s)
};

const f64 TIMESTAMPS_PER_SECOND = 4294967296.;		// 2^32 as a double

inline f64 SignedTimestampToSeconds(i64 timestamp)
{
	return(((f64)timestamp)/TIMESTAMPS_PER_SECOND);
}

inline f64 TimestampToSeconds(u64 timestamp)
{
	return(((f64)timestamp)/TIMESTAMPS_PER_SECOND);
}

inline i64 SecondsToTimestamp(f64 seconds)
{
	return(seconds * TIMESTAMPS_PER_SECOND);
}

inline u64 AbsSecondsToTimestamp(f64 seconds)
{
	return(fabs(seconds) * TIMESTAMPS_PER_SECOND);
}

inline u64 UserClockGetTimestamp(user_clock* clock)
{
	u64 time = GetSystemTimestamp();
	i64 offset = clock->offset + (time - clock->lastAdjust) * clock->slewRate;
	if(clock->slewRate > 0)
	{
		return(time + minimum(clock->offsetTarget, offset));// + SecondsToTimestamp(clock->residual));
	}
	else
	{
		return(time + maximum(clock->offsetTarget, offset));// + SecondsToTimestamp(clock->residual));
	}
}

inline i64 UserClockGetOffset(user_clock* clock)
{
	u64 time = GetSystemTimestamp();
	i64 offset = clock->offset + (time - clock->lastAdjust) * clock->slewRate;
	if(clock->slewRate > 0)
	{
		return(minimum(clock->offsetTarget, offset));
	}
	else
	{
		return(maximum(clock->offsetTarget, offset));
	}
}

void UserClockSetup(user_clock* clock);
void UserClockStepTime(user_clock* clock, f64 step);
void UserClockAdjustTime(user_clock* clock, f64 adj);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus


#endif //__USER_CLOCK_H_
