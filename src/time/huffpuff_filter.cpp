/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: huffpuff_filter.cpp
*	@author: Martin Fouilleul
*	@date: 28/06/2019
*	@revision:
*
*****************************************************************/

#include"huffpuff_filter.h"

void HuffPuffInit(huff_puff_filter* filter)
{
	filter->currentIndex = 0;
	filter->minSample = 1.e9;
	filter->timer = 0;

	for(int i=0; i<HUFF_PUFF_SIZE; i++)
	{
		filter->samples[i] = 1.e9;
	}
}

void HuffPuffSlide(huff_puff_filter* filter, u64 currentTime)
{
	//NOTE(martin): This is called each second from AdjustTimer() to check if we need to slide the huffpuff window

	if(filter->timer <= currentTime)
	{
		//NOTE(martin): each sampling interval we overwrite the oldest sample with a max value.
		filter->currentIndex++;
		if(filter->currentIndex >= HUFF_PUFF_SIZE)
		{
			filter->currentIndex = 0;
		}

		filter->samples[filter->currentIndex] = 1.e9;
		filter->minSample = 1.e9;


		//NOTE(martin): We then	recompute the minimum sample.
		for(int i=0; i<HUFF_PUFF_SIZE; i++)
		{
			if(filter->samples[i] < filter->minSample)
			{
				filter->minSample = filter->samples[i];
			}
		}

		//NOTE(martin): schedule our next update
		filter->timer += HUFF_PUFF_PERIOD;
	}
}

f64 HuffPuffFilter(huff_puff_filter* filter, f64 delay, f64 offset)
{
	//NOTE(martin): A new sample arrives from the clock discipline algorithm.
	//		We overwrite the current sample if its smaller.
	//		(ie we keep only the smallest sample for each sampling period)

	if(delay < filter->samples[filter->currentIndex])
	{
		filter->samples[filter->currentIndex] = delay;
	}

	//NOTE(martin): then we possibly update the smallest value of the sliding window
	if(delay < filter->minSample)
	{
		filter->minSample = delay;
	}

	//NOTE(martin): Now we apply the huff-n'-puff correction to the offset
	if(offset > 0)
	{
		return(offset - (delay - filter->minSample)/2);
	}
	else
	{
		return(offset + (delay - filter->minSample)/2);
	}
}
