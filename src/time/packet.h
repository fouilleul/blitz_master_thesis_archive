/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: packet.h
*	@author: Martin Fouilleul
*	@date: 07/03/2019
*	@revision:
*
*****************************************************************/
#ifndef __PACKET_H_
#define __PACKET_H_

/*
	synchronization packet structure
*/

#include"typedefs.h"

const u8 SYNC_REQUEST = 0,
	 SYNC_REPLY = 1;

struct sync_packet
{
	u8  type;

	u64 t1;		// This is the original request time, as given by GetUserTimestamp() on the client
	u64 t2;		// This is the received time, as given by GetSystemTimestamp() on the server when a request is received
	u64 t3;		// This is the reply time, as given by the GetSystemTimestamp() on the server when a reply is sent

}__attribute__((packed));


#endif //__PACKET_H_
