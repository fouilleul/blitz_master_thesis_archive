/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: osx_time.cpp
*	@author: Martin Fouilleul
*	@date: 07/03/2019
*	@revision:
*
*****************************************************************/

#include<sys/time.h>	// gettimeofday()
#include<mach/mach_time.h>
#include<unistd.h>	// nanosleep()
#include<stdlib.h>	// rand(), srand()...
#include"user_clock.h"
#include"platform_time.h"

extern "C" {

const u64 JAN_1970 = 2208988800L;	// seconds from january 1900 to january 1970

//TODO(martin): measure the actual values of these constants
const f64 SYSTEM_FUZZ = 25e-9,		// minimum time to read the clock (s)
          SYSTEM_TICK = 25e-9;		// minimum step between two clock readings (s)

static mach_timebase_info_data_t __machTimeBase__ = {1,1};
static u64 __initialTimestamp__ = 0;
static u64 __initialAbsTime__ = 0;

static inline u64 GetAbsNanoseconds()
{
	u64 now = mach_absolute_time();
	now *= __machTimeBase__.numer;
	now /= __machTimeBase__.denom;
	return(now);
}

void SystemInitTime()
{
	mach_timebase_info(&__machTimeBase__);

	timeval tv;
	gettimeofday(&tv, 0);
	__initialAbsTime__ = GetAbsNanoseconds();
	__initialTimestamp__ =   (unsigned long long) ((tv.tv_sec + JAN_1970) << 32)
			       + (unsigned long long) (tv.tv_usec * 1e-6 * TIMESTAMPS_PER_SECOND);

	srand((long)TimestampToSeconds(GetSystemTimestamp()));
}

u64 GetSystemTimestamp()
{
	//TODO(martin): We should use a timer with a better granularity,
	//		eg by using mach_absolute_time() relative to an init timestamp

	u64 noff = GetAbsNanoseconds() - __initialAbsTime__;

	//TODO(martin): review these computations regarding precision loss
	//NOTE(martin): compute fixed point offset
	u64 foff = (u64)(noff * 1e-9 * TIMESTAMPS_PER_SECOND);
	u64 timestamp = __initialTimestamp__ + foff;

	//NOTE(martin): add a random fuzz between 0 and 1 times the system fuzz
	f64 fuzz = rand() * 2/(f64)(1<<31) * SYSTEM_FUZZ;
	i64 tsfuzz = SecondsToTimestamp(fuzz);
	timestamp += tsfuzz;

	//TODO(martin): ensure that we always return a value greater than the last value

	return(timestamp);
}

void SystemSleepNanoseconds(u64 nanoseconds)
{
	timespec rqtp;
	rqtp.tv_sec = nanoseconds / 1000000000;
	rqtp.tv_nsec = nanoseconds - rqtp.tv_sec * 1000000000;
	nanosleep(&rqtp, 0);
}


} // extern "C"
