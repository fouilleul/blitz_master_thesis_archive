/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: time_server.h
*	@author: Martin Fouilleul
*	@date: 28/06/2019
*	@revision:
*
*****************************************************************/
#ifndef __TIME_SERVER_H_
#define __TIME_SERVER_H_

#include"platform_thread.h"
#include"platform_socket.h"
#include"user_clock.h"

struct time_server
{
	bool run;
	platform_thread* thread;
	platform_socket* sock;
	i64 offset;
};

void TimeServerStart(time_server* server, host_port port, i64 offset = 0);
void TimeServerStop(time_server* server);

#endif	// __TIME_SERVER_H_
