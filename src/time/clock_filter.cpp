/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: clock_filter.cpp
*	@author: Martin Fouilleul
*	@date: 13/03/2019
*	@revision:
*
*****************************************************************/
#include<string.h>	// memcpy()
#include<stdio.h>	//DEBUG printf()

#include"user_clock.h"
#include"clock_filter.h"

//--------------------------------------------------------------------------------
// Clock filter algorithm
//--------------------------------------------------------------------------------

void ClockFilterInit(clock_filter* filter)
{
	for(int i=0; i<CLOCK_FILTER_SIZE; i++)
	{
		clock_filter_value* val = &(filter->reg[i]);
		val->offset = 0;
		val->delay = MAX_DISP;
		val->disp = MAX_DISP;
		val->epoch = 0;
	}
	filter->lastUpdate = 0;
}

bool ClockFilterPush(clock_filter* filter, clock_filter_value* val, clock_filter_result* result, uint8 pollTimeConstant)
{
	/*
		Here we follow the RFC 5905 specification and example implementation
		(this _differs_ from ntpd implementation, which filters out more samples...)
	*/

	for(int i=1; i < CLOCK_FILTER_SIZE; i++)
	{
		//NOTE(martin): increase the dispersion since the last filter update
		//		and shift the filter register
		filter->reg[i] = filter->reg[i-1];
		filter->reg[i].disp += SYSTEM_PHI * (filter->lastUpdate - val->epoch);
	}
	filter->reg[0] = *val;
	filter->lastUpdate = val->epoch;

	//NOTE(martin): store samples in temp array, and sort by increasing delay
	//		we use a simple insert sort, which is efficient for such small counts as CLOCK_FILTER_SIZE

	clock_filter_value tmp;
	clock_filter_value buffer[CLOCK_FILTER_SIZE];
	memcpy(buffer, filter->reg, sizeof(clock_filter_value)*CLOCK_FILTER_SIZE);

	for(int i=0; i < CLOCK_FILTER_SIZE; i++)
	{
		clock_filter_value* p = &(buffer[i]);
		tmp = buffer[i];

		int j = i-1;

		while(j >= 0 && (tmp.delay < buffer[j].delay))
		{
			buffer[j+1] = buffer[j];
			j--;
		}
		buffer[j+1] = tmp;
	}

	//NOTE(martin): now we compute the resulting disp
	//		(keep in mind the last valid sample was passed in result)

	f64 disp = 0;
	f64 jitter = 0;
	f64 offset0 = buffer[0].offset;
	i64 div = 2;
	for(int i=0; i<CLOCK_FILTER_SIZE; i++)
	{
		jitter += SQUARE(offset0 - buffer[i].offset);
		disp += (buffer[i].disp / div);
		div = div << 1;
	}
	jitter = SQRT(jitter)/(CLOCK_FILTER_SIZE - 1);
	jitter = ClampLowBound(jitter, SYSTEM_PRECISION);

	result->offset = buffer[0].offset;
	result->delay = buffer[0].delay;
	result->disp = disp;
	result->jitter = jitter;

	//TODO(martin): filter out if candidate sample has a dispersion too high ?

	//TODO(martin): we should not rely on a good initialization of result->epoch !!!
	if(buffer[0].epoch <= result->epoch)
	{
		//NOTE(martin): we use a sample only once and never a sample older than the latest one
		return(false);
	}
	else
	{
		/*NOTE(martin): suppress popcorn spikes.
				if the difference between the offsets of the new and last sample exceed SGATE time the jitter
				and the interval between them is less than twice the poll interval,
				its considered as a popcorn spike
		*/
		if(fabs(buffer[0].offset - result->offset) > SGATE * result->jitter
		  && (buffer[0].epoch - result->epoch) < 2 * (1<<pollTimeConstant))
		{
			//printf("Clock filter : filtered out spike\n");
			return(false);
		}
		result->epoch = buffer[0].epoch;
		return(true);
	}
}
