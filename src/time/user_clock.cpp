/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: user_clock.cpp
*	@author: Martin Fouilleul
*	@date: 28/06/2019
*	@revision:
*
*****************************************************************/
#include<string.h>	// memcpy()
#include<stdio.h>	//DEBUG printf()

#include"user_clock.h"
#include"platform_time.h"

extern "C" {

//--------------------------------------------------------------------------------
// user clock
//--------------------------------------------------------------------------------

void UserClockSetup(user_clock* clock)
{
	//TODO(martin): this should be made thread safe !!!!!

	clock->lastAdjust = GetSystemTimestamp();
	clock->offset = 0;
	clock->slewRate = 0;
	clock->offsetTarget = 0;
}

void UserClockStepTime(user_clock* clock, f64 step)
{
	// TODO(martin): properly handle possible overflows
	clock->offset += SecondsToTimestamp(step);
	clock->offsetTarget = clock->offset;
}

void UserClockAdjustTime(user_clock* clock, f64 adj)
{
	if(adj == 0)
	{
		return;
	}
	//TODO(martin): keep track of residual adjustments

	f64 dtemp = adj + clock->residual;

/*
	//NOTE(martin): Quantize to the usec and keep track of residual
	long usec = (long)(dtemp * 1.e6 + .5);
	f64 rounded = usec * 1.e-6;
	clock->residual= dtemp - rounded;
*/
	f64 rounded = adj;

	i64 delta = SecondsToTimestamp(rounded);
	u64 time = GetSystemTimestamp();
	i64 offset = clock->offset + (time - clock->lastAdjust) * clock->slewRate;
	if(clock->slewRate > 0)
	{
		offset = minimum(clock->offsetTarget, offset);
	}
	else
	{
		offset = maximum(clock->offsetTarget, offset);
	}
	clock->offset = offset;
	clock->lastAdjust = time;
	clock->offsetTarget = clock->offset + delta;
	clock->slewRate = delta > 0 ? 500e-6 : -500e-6 ;
}

} // extern "C"
