/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: time_client.cpp
*	@author: Martin Fouilleul
*	@date: 07/03/2019
*	@revision:
*
*****************************************************************/

/*
	Synchronizes on time server
*/


#include<errno.h>
#include<string.h>	// strerror()
#include<signal.h>	// signal(), to handle Ctrl-C

//DEBUG
#include<stdio.h>	// printf()
#include<assert.h>	// assert()

#include"packet.h"
#include"platform_time.h"
#include"time_client.h"
#include"simulator.h"

inline f64 Log2Lin(int a)
{
	return(a < 0 ? (1. / (1L << -a)) : (1L << a));
}

//--------------------------------------------------------------------------------
// Clock Adjust process
//--------------------------------------------------------------------------------

void Poll(clock_peer* peer, user_clock* clock)
{
	sync_packet packet = {0};
	packet.type = SYNC_REQUEST;
	packet.t1 = UserClockGetTimestamp(clock);

	if(SocketSendTo(peer->sock, &packet, sizeof(packet), 0, &peer->addr) == -1)
	{
		printf("Error %i : %s\n", errno, strerror(errno));
		exit(0);
	}
}

void ClockAdjust(clock_discipline* discipline, user_clock* clock, time_client* client)
{
	//NOTE(martin): runs at one-second intervals and adjust the local user clock

	double offsetAdjust, frequencyAdjust;

	if(client->enableDiscipline)
	{
		if(discipline->state != CS_SYNC)
		{
			offsetAdjust = 0;
		}
		else if(discipline->freqHoldTimer > 0)
		{
			offsetAdjust = discipline->offset / (CLOCK_PLL * Log2Lin(1));
			discipline->freqHoldTimer--;
		}
		else
		{
			offsetAdjust = discipline->offset / (CLOCK_PLL * minimum(Log2Lin(discipline->pollTimeConstant), Log2Lin(CLOCK_ALLAN)));
		}
		frequencyAdjust = discipline->freq;

		//NOTE(martin): bound absolute total adjustment
		if(offsetAdjust + frequencyAdjust > CLOCK_MAX_FREQ)
		{
			offsetAdjust = CLOCK_MAX_FREQ - frequencyAdjust;
		}
		else if(offsetAdjust + frequencyAdjust < -CLOCK_MAX_FREQ)
		{
			offsetAdjust = -CLOCK_MAX_FREQ - frequencyAdjust;
		}
		discipline->offset -= offsetAdjust;
	}
	else
	{
		frequencyAdjust = 0;
		offsetAdjust = discipline->offset;
		discipline->offset = 0;
		discipline->pollTimeConstant = 2;
	}

	#ifdef PRINT_SYNC_INFO
	{
		#ifdef TIME_SERVER_SIMULATOR
			double serverOffset = SignedTimestampToSeconds(TimeServerSimulatorOffset());
		#else
			double serverOffset = 0;
		#endif //TIME_SERVER_SIMULATOR

		int pollInterval = 1 << Clamp(client->discipline.pollTimeConstant, CLOCK_MIN_POLL, CLOCK_MAX_POLL);
		printf("%.15f ; %.15f ; %.15f ; %.15f ; %.15f ; %.15f ; %i; %i\n",
			       SignedTimestampToSeconds(UserClockGetOffset(&client->clock)) - serverOffset,
			       client->filterOutput.delay,
			       client->filterOutput.offset,
			       offsetAdjust,
			       frequencyAdjust,
			       offsetAdjust+frequencyAdjust,
			       client->discipline.state,
			       pollInterval);
	}
	#endif // PRINT_SYNC_INFO

	UserClockAdjustTime(clock, offsetAdjust + frequencyAdjust);
}

void* PollAdjustTimer(void* p)
{
	time_client* client = (time_client*)p;
	clock_peer* peer = &client->peer;

	uint64 date = GetSystemTimestamp();
	while(client->run)
	{
		client->epoch++;

		//NOTE(martin): clock adjust timer
		ClockAdjust(&client->discipline, &client->clock, client); //NOTE(martin): pass the client for debugging only

		if(client->enableHuffPuff)
		{
			//NOTE(martin): huff-puff timer
			HuffPuffSlide(&client->huffpuff, client->epoch);
		}

		//NOTE(martin): poll timer
		if(client->epoch >= peer->nextPoll)
		{
			#ifndef TIME_SERVER_SIMULATOR
				Poll(peer, &client->clock);
			#else
				PollSimulator(peer, &client->clock);
			#endif // TIME_SERVER_SIMULATOR

			u64 pollInterval = 1 << Clamp(client->discipline.pollTimeConstant, CLOCK_MIN_POLL, CLOCK_MAX_POLL);
			peer->nextPoll = client->epoch + pollInterval;
		}

		//TODO(martin): better scheduling of next execution : this one is quite dumb and may present rounding inaccuracies
		uint64 nextDate = date + TIMESTAMPS_PER_SECOND;
		double timeToSleep = ClampLowBound(SignedTimestampToSeconds(nextDate - GetSystemTimestamp()), 0);
		SystemSleepNanoseconds(timeToSleep * 1.e9);
		date = nextDate;
	}
	return(0);
}

//--------------------------------------------------------------------------------
// clock discipline algorithm
//--------------------------------------------------------------------------------

void ClockDisciplineInit(clock_discipline* discipline)
{
	memset(discipline, 0, sizeof(clock_discipline));

	discipline->state = CS_FSET;
	discipline->pollTimeConstant = CLOCK_MIN_POLL;
	discipline->offset = 0;
	discipline->freq = 0;
	discipline->freqHoldTimer = CLOCK_STEPOUT_TIME;
}

const int RVAL_PANIC = -1,
	  RVAL_IGNORE = 0,
	  RVAL_STEP = 1,
	  RVAL_SLEW = 2;


void ClockTransition(clock_discipline* discipline, CLOCK_STATE state, f64 offset, u64 currentTime)
{
	discipline->state = state;
	discipline->lastOffset = discipline->offset = offset;
	discipline->lastUpdate = currentTime;			// this is the current time according to the seconds counter
}

int ClockDiscipline(time_client* client, clock_discipline* discipline, huff_puff_filter* huffpuff, user_clock* clock, u64 epoch, f64 delay, f64 offset)
{
	if(fabs(offset) > CLOCK_PANIC_THRESHOLD)
	{
		//NOTE(martin): in NTPD it exits the server and requires an operator to manually adjust the clock
		//		here we just log the panic and let the sync adjust over time (our first values could be way off)
		printf("Error: ClockDiscipline() : offset (%f) beyond panic threshold. Please set the system clock manually.\n",
		        offset);
		return(RVAL_PANIC);
	}

	if(client->enableHuffPuff)
	{
		offset = HuffPuffFilter(huffpuff, delay, offset);
	}

	//NOTE(martin): clock discipline state machine, see RFC5905, p98

	f64 mu = epoch - discipline->lastUpdate; assert(mu != 0);
	f64 freq = discipline->freq;
	int rval = RVAL_SLEW;

	if(fabs(offset) > CLOCK_STEP_THRESHOLD)
	{
		//printf("offset bigger than step threshold\n");

		switch(discipline->state)
		{
			case CS_SYNC:
				//NOTE(rfc): ignore the first outlier and switch to CS_SPIKE
				discipline->state = CS_SPIKE;
				return(RVAL_IGNORE);

			case CS_FREQ:
				//NOTE(rfc): ignore outliers and inliers. At the first outlier after the stepout threshold,
				//		compute the apparent frequency correction and step the time
				if(mu < CLOCK_STEPOUT_TIME)
				{
					return(RVAL_IGNORE);
				}
				freq = discipline->freq = offset / mu ;

				// fall through CS_SPIKE

			case CS_SPIKE:
				//NOTE(rfc): ignore succeeding outliers until either an inlier is found or the stepout threshold is exceeded
				if(mu < CLOCK_STEPOUT_TIME)
				{
					//printf("mu < step out time !\n");
					return(RVAL_IGNORE);
				}
				// fall through default

			default:
				/*NOTE(rfc):
				*	we get here by default in CS_NSET and CS_FSET states and from above in CS_FREQ state.
				*	Step time and clamp down the poll interval.
				*
				*	In CS_NSET state, an initial frequency correction is
				*	not available, usually because the frequency file has
				*	not yet been written.  Since the time is outside the
				*	capture range, the clock is stepped.  The frequency
				*	will be set directly following the stepout interval.
				*
				*	In CS_FSET state, the initial frequency has been set
				*	from the frequency file.  Since the time is outside
				*	the capture range, the clock is stepped immediately,
				*	rather than after the stepout interval.  Guys get
				*	nervous if it takes 17 minutes to set the clock for
				*	the first time.
				*
				*	In CS_SPIKE state, the stepout threshold has expired
				*	and the phase is still above the step threshold.
				*	Note that a single spike greater than the step
				*	threshold is always suppressed, even at the longer
				*	poll intervals.
				*/
				//printf("step time\n");
				UserClockStepTime(clock, offset);

				discipline->jiggleCounter = 0;
				discipline->jitter = Log2Lin(SYSTEM_PRECISION);
				rval = RVAL_STEP;
				//TODO(martin): set poll interval to minimum
				if(discipline->state == CS_NSET)
				{
					//printf("return rval !\n");
					ClockTransition(discipline, CS_FREQ, 0, epoch);
					return(rval);
				}
				break;
		}
		ClockTransition(discipline, CS_SYNC, 0, epoch);
	}
	else
	{
		//NOTE(rfc): compute the clock jitter as the RMS of exponentially weighted offset differences.
		//	     This is used by the poll-adjust code

		f64 etemp = SQUARE(discipline->jitter);
		f64 dtemp = SQUARE(ClampLowBound(fabs(offset - discipline->lastOffset), Log2Lin(SYSTEM_PRECISION)));
		//TODO(martin) system_precision should be in system struct
		discipline->jitter = SQRT(etemp + (dtemp - etemp)/CLOCK_AVG);

		//NOTE(martin): here we follow the ntp canonical implementation which DIFFERS from the RFC skeleton code !
		switch(discipline->state)
		{
			case CS_NSET:
				//NOTE(ntpd): this is the first update received and the frequency has not been initialized.
				//	      Adjust the phase, but do not adjust the frequency until after the stepout threshold.
				UserClockAdjustTime(clock, offset);
				ClockTransition(discipline, CS_FREQ, offset, epoch);
				break;

			case CS_FREQ:
				//NOTE(ntpd): ignore updates until the stepout threshold. After that, compute the new frequency,
				//           but do not adjust the frequency until the holdoff counter decrements to zero.
				if(mu < CLOCK_STEPOUT_TIME)
				{
					return(RVAL_IGNORE);
				}
				freq = discipline->freq = offset / mu;

				//fall through

			default:
				//NOTE(ntpd): we get here by default in fset, sync and spike states.
				//	     Compute the frequency update due to PLL and FLL contributions

				if(discipline->freqHoldTimer == 0)
				{
					/*NOTE(ntpd):
						The FLL and PLL frequency gain constants
						depend on the time constant and Allan
						intercept. The PLL is always used, but
						becomes ineffective above the Allan intercept
						where the FLL becomes effective.
					 */
					if(discipline->pollTimeConstant > CLOCK_ALLAN / 2)
					{
						//NOTE(martin): freq is initially set to discipline->freq
						freq += (offset - discipline->offset)
						       / maximum(Log2Lin(discipline->pollTimeConstant), mu)
						       * CLOCK_FLL;
					}
					/*NOTE(ntpd):
						The PLL frequency gain (numerator) depends on
						the minimum of the update interval and Allan
						intercept. This reduces the PLL gain when the
						FLL becomes effective.
					 */
					f64 etemp = minimum(Log2Lin(CLOCK_ALLAN), mu);
					f64 dtemp = 4 * CLOCK_PLL * Log2Lin(discipline->pollTimeConstant);
					freq += offset * etemp / SQUARE(dtemp);
				}
				ClockTransition(discipline, CS_SYNC, offset, epoch);
				if(fabs(offset) < CLOCK_FLOOR)
				{
					discipline->freqHoldTimer = 0;
				}
				break;
		}
	}

	//NOTE(rfc): calculate the new frequency and frequency stability (wander).
	//	     Compute the clock wander as the RMS of exponentially weighted frequency differences.

	f64 dtemp = SQUARE(freq - discipline->freq);
	f64 etemp = SQUARE(discipline->wander);

	discipline->freq = Clamp(freq, -CLOCK_MAX_FREQ, CLOCK_MAX_FREQ);
	discipline->wander = SQRT(etemp + (dtemp-etemp)/CLOCK_AVG);

	//NOTE(rfc): Here we adjust the poll interval by comparing the current offset with the clock jitter.
	//	     If the offset is less than the clock jitter times a constant,
	//	     then the averaging interval is increased; otherwise, it is decreased.
	//	     A bit of hysteresis helps calm the dance.  Works best using burst mode.

	if(discipline->freqHoldTimer > 0)
	{
		discipline->jiggleCounter = 0;
	}
	else if(fabs(discipline->offset) < CLOCK_PGATE * discipline->jitter)
	{
		discipline->jiggleCounter += discipline->pollTimeConstant;
		if(discipline->jiggleCounter > CLOCK_LIMIT)
		{
			discipline->jiggleCounter = CLOCK_LIMIT;
			if(discipline->pollTimeConstant < CLOCK_MAX_POLL)
			{
				discipline->jiggleCounter = 0;
				discipline->pollTimeConstant++;
			}
		}
	}
	else
	{
		discipline->jiggleCounter -= discipline->pollTimeConstant << 1;
		if(discipline->jiggleCounter < -CLOCK_LIMIT)
		{
			discipline->jiggleCounter = -CLOCK_LIMIT;
			if(discipline->pollTimeConstant > CLOCK_MIN_POLL)
			{
				discipline->jiggleCounter = 0;
				discipline->pollTimeConstant--;
			}
		}
	}
	return(rval);
}

//--------------------------------------------------------------------------------
// Peer process thread
//--------------------------------------------------------------------------------


int ReceiveSyncReply(clock_peer* peer, user_clock* clock, sync_packet* packet, u64* t4)
{
	socket_address from = {0};

	ssize_t size = SocketReceiveFrom(peer->sock, packet, sizeof(sync_packet), 0, &from);
	*t4 = UserClockGetTimestamp(clock);

	if(size == -1)
	{
		fprintf(stderr, "Error %i : %s\n", errno, strerror(errno));
		return(-1);
	}
	else if(size != sizeof(sync_packet))
	{
		//bad reply packet, ignore
		//TODO(martin): maybe we should tolerate size mismatches to allow future extensions with different packet sizes
		printf("Error : bad reply packet (size mismatch)\n");
		return(-1);
	}
	else if(packet->type != SYNC_REPLY)
	{
		//bad reply packet, ignore
		printf("Error : bad reply packet (type mismatch)\n");
		return(-1);
	}
	else
	{
		return(0);
	}
}

void* PeerProcess(void* data)
{
	time_client* client = (time_client*)data;
	user_clock* clock = &client->clock;
	clock_peer* peer = &client->peer;

	while(client->run)
	{
		sync_packet packet = {0};
		u64 t4;

		#ifndef TIME_SERVER_SIMULATOR
			int err = ReceiveSyncReply(peer, clock, &packet, &t4);
		#else
			int err = ReceiveSyncReplySimulator(peer, clock, &packet, &t4);
		#endif // TIME_SERVER_SIMULATOR

		if(err == 0)
		{
			/*NOTE(martin):
			*	Compute the packet offset, delay and dispersion values, according to the on-wire protocol
			*	(See RFC5905 p29, p37)
			*	First order differences are computed on fixed point values. Then the remaining computations are
			*	carried on floating point (seconds) values
			*
			*	The convention used here is that a positive offset means the server clock is into the future
			*	relative to the client clock.
			*/

			//TODO(martin): t4 should always be > t1, but we should check for incorrect packets !!

			f64 t21 = (packet.t2 > packet.t1) ?
					(TimestampToSeconds(packet.t2 - packet.t1)) :
					-(TimestampToSeconds(packet.t1 - packet.t2));

			f64 t34 = (packet.t3 > t4) ?
					(TimestampToSeconds(packet.t3 - t4)) :
					-(TimestampToSeconds(t4 - packet.t3));

			f64 delay = ClampLowBound(t21 - t34, Log2Lin(SYSTEM_PRECISION));
			f64 offset = 0.5*(t21 + t34);
			f64 disp =  Log2Lin(PACKET_PRECISION)
				  + Log2Lin(SYSTEM_PRECISION)
				  + SYSTEM_PHI * delay;

			if(client->enableDiscipline)
			{
				//NOTE(martin): Apply the clock filter algorithm

				clock_filter_value sample;
				sample.offset = offset;
				sample.delay = delay;
				sample.disp = disp;
				sample.epoch = client->epoch;

				bool res = ClockFilterPush(&client->clockFilter,
				                           &sample,
							   &client->filterOutput,
							   client->discipline.pollTimeConstant);

				//NOTE(martin): Apply the clock discipline algorithm if we have a good sample
				if(res)
				{
					switch(ClockDiscipline(client,
							       &client->discipline,
							       &client->huffpuff,
							       &client->clock,
							       client->filterOutput.epoch,
							       client->filterOutput.delay,
							       client->filterOutput.offset))
					{
						case RVAL_PANIC:
							client->run = false;
							continue;
						case RVAL_IGNORE:
							//printf("Clock Discipline returned RVAL_IGNORE\n");
							break;
						case RVAL_STEP:
							//printf("Clock Discipline returned RVAL_STEP\n");
							break;
						case RVAL_SLEW: default:
							//printf("Clock Discipline returned RVAL_SLEW\n");
							break;
					}
				}
				else
				{
					//printf("Ignored sample\n");
				}
			}
			else
			{
				//NOTE(martin): discipline is not enabled, only use the wire protocol
				client->discipline.offset = offset;
				client->filterOutput.delay = delay;
				client->filterOutput.offset = offset;
				client->filterOutput.epoch = client->epoch;
			}
		}
	}
	return(0);
}

//--------------------------------------------------------------------------------
// Time client thread
//--------------------------------------------------------------------------------

void TimeClientStart(time_client* client, host_ip serverIP, host_port serverPort, time_client_options* options)
{
	//NOTE(martin): setup our local clock
	UserClockSetup(&client->clock);

	//NOTE(martin): initialize the filters
	ClockFilterInit(&client->clockFilter);
	HuffPuffInit(&client->huffpuff);
	ClockDisciplineInit(&client->discipline);

	memset(&client->filterOutput, 0, sizeof(clock_filter_result));

	//NOTE(martin): Init the peer structure for time server polling

	#ifndef TIME_SERVER_SIMULATOR
		client->peer.sock = SocketOpen(SOCK_UDP);
		assert(client->peer.sock);

		client->peer.addr.ip = HostToNetIP(serverIP);
		client->peer.addr.port = HostToNetPort(serverPort);
	#endif //TIME_SERVER_SIMULATOR

	client->peer.nextPoll = 0;

	//NOTE(martin): spawn poll/adjust and peer process threads
	client->epoch = 0;
	client->run = true;
	client->enableDiscipline = options ? options->enableDiscipline : true;
	client->enableHuffPuff = options ? options->enableHuffPuff : true;



	client->pollAdjThread = ThreadCreate(PollAdjustTimer, client);
	assert(client->pollAdjThread);

	client->peerProcessThread = ThreadCreate(PeerProcess, client);
	assert(client->peerProcessThread);

	//TODO(martin): check return values and return err code
}

void TimeClientStop(time_client* client)
{
	client->run = false;

	#ifndef TIME_SERVER_SIMULATOR
	SocketClose(client->peer.sock);
	#endif //TIME_SERVER_SIMULATOR

	//TODO(martin): close sock, but check for errors in the peer process thread...

	void* retVal = 0;
	ThreadJoin(client->pollAdjThread, &retVal);
	ThreadJoin(client->peerProcessThread, &retVal);

	ThreadDestroy(client->pollAdjThread);
	ThreadDestroy(client->peerProcessThread);
}


void TimeClientStartSimulation(time_client* client, time_simulator_options* options)
{
	//NOTE(martin): Init time server simulation

	InitTimeServerSimulator(options->offset,
	                        options->randomSeed,
				options->upScale,
				options->upShape,
				options->downScale,
				options->downShape);

	//NOTE(martin): setup our local clock
	UserClockSetup(&client->clock);

	//NOTE(martin): initialize the filters
	ClockFilterInit(&client->clockFilter);
	HuffPuffInit(&client->huffpuff);
	ClockDisciplineInit(&client->discipline);

	memset(&client->filterOutput, 0, sizeof(clock_filter_result));

	//NOTE(martin): Init the peer structure for time server polling

	client->peer.nextPoll = 0;

	//NOTE(martin): spawn poll/adjust and peer process threads
	client->epoch = 0;
	client->run = true;

	client->enableDiscipline = options->enableDiscipline;
	client->enableHuffPuff = options->enableHuffPuff;

	client->pollAdjThread = ThreadCreate(PollAdjustTimer, client);
	assert(client->pollAdjThread);

	client->peerProcessThread = ThreadCreate(PeerProcess, client);
	assert(client->peerProcessThread);

	//TODO(martin): check return values and return err code
}

void TimeClientStopSimulation(time_client* client)
{
	CleanTimeServerSimulator();

	client->run = false;

	//TODO(martin): close sock, but check for errors in the peer process thread...

	void* retVal = 0;
	ThreadJoin(client->pollAdjThread, &retVal);
	ThreadJoin(client->peerProcessThread, &retVal);

	ThreadDestroy(client->pollAdjThread);
	ThreadDestroy(client->peerProcessThread);
}
