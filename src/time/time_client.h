/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: time_client.h
*	@author: Martin Fouilleul
*	@date: 07/03/2019
*	@revision:
*
*****************************************************************/
#ifndef __TIME_CLIENT_H_
#define __TIME_CLIENT_H_

#include"typedefs.h"
#include"user_clock.h"
#include"clock_filter.h"
#include"huffpuff_filter.h"
#include"platform_socket.h"
#include"platform_thread.h"

//------------------------------------------------------------------------------------------------
// Clock peer constants and structs
//------------------------------------------------------------------------------------------------

struct clock_peer
{
	platform_socket* sock;
	socket_address addr;
	u64 nextPoll;
};

//--------------------------------------------------------------------------------
// clock discipline constants and structs
//--------------------------------------------------------------------------------

const u32 CLOCK_MAX_POLL = 3,		// maximum poll interval in log2 (=8s)
	  CLOCK_MIN_POLL = 1,		// minimum poll interval in log2 (=2s)
          CLOCK_ALLAN	 = 11;		// Allan intercept (log2 s)

const f64 CLOCK_PANIC_THRESHOLD = 43200,	// panic threshold (s) = 12h
	  CLOCK_STEP_THRESHOLD	= .128,		// step threshold (s)
	  CLOCK_STEPOUT_TIME	= 300,		// stepout theshold (s)
	  CLOCK_MAX_FREQ	= 500e-6,	// frequency tolerance (500 ppm)
	  CLOCK_PLL		= 16.,		// PLL loop gain (log2)
	  CLOCK_FLL		= .25,		// FLL loop gain
	  CLOCK_AVG		= 4,		// parameter averaging constant
	  CLOCK_LIMIT		= 30,		// poll-adjust threshold
	  CLOCK_PGATE		= 4,		// poll-adjust gate
	  CLOCK_FLOOR		= .0005;	// startup offset floor (s)

typedef enum { CS_NSET, CS_FSET, CS_SPIKE, CS_FREQ, CS_SYNC } CLOCK_STATE;

struct clock_discipline
{
	CLOCK_STATE state;		// state machine's state

	u8	pollTimeConstant;	// time constant in log2 seconds. This gives the decrease rate of the discipline offset
	u32	jiggleCounter;		// jiggle counter
	f64	lastOffset;		// last offset set by the discipline algorithm, in seconds
	u64	lastUpdate;		// last time the discipline algorithm was run, in seconds (according to the seconds counter)

	f64	offset;			// current clock offset in seconds. It is set by the discipline algorithm, and decrease with each adjustment pass
	f64	freq;			// frequency rate with regard to system clock, in s/s
	f64	jitter;			// RMS of the offset jitter
	f64	wander;			// RMS of the frequency wander

	i32	freqHoldTimer;		// initial frequency clamp

};

//--------------------------------------------------------------------------------
// time client struct
//--------------------------------------------------------------------------------

struct time_client
{
	bool run;
	platform_thread* pollAdjThread;
	platform_thread* peerProcessThread;

	bool enableDiscipline;	// if false, use only the wire protocol
	bool enableHuffPuff;	// if false, don't correct round-trip asymetry

	u64 epoch;

	user_clock	    clock;
	clock_peer	    peer;
	clock_filter	    clockFilter;
	clock_filter_result filterOutput;
	huff_puff_filter    huffpuff;
	clock_discipline    discipline;
};

struct time_client_options
{
	bool enableHuffPuff;
	bool enableDiscipline;
};

//--------------------------------------------------------------------------------
// time client public API functions
//--------------------------------------------------------------------------------

void TimeClientStart(time_client* client, host_ip serverIP, host_port serverPort, time_client_options* options = 0);
void TimeClientStop(time_client* client);


inline user_clock* TimeClientGetClock(time_client* client){return(&client->clock);}

// Time Simulator API (for measurement and debugging purposes)

struct time_simulator_options
{
	int64 offset;
	uint32 randomSeed;
	double upScale;
	double upShape;
	double downScale;
	double downShape;

	bool enableDiscipline;
	bool enableHuffPuff;
};

void TimeClientStartSimulation(time_client* client, time_simulator_options* options);
void TimeClientStopSimulation(time_client* client);


#ifdef TIME_SERVER_SIMULATOR
	#include"simulator.h"
#endif

#endif	// __TIME_CLIENT_H_
