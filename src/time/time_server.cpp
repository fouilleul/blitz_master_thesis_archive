/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: server.cpp
*	@author: Martin Fouilleul
*	@date: 07/03/2019
*	@revision:
*
*****************************************************************/

/*
	Respond to synchronization requests from clients
*/

#include"time_server.h"

//DEBUG
#include<stdio.h>
#include<assert.h>

#include"platform_time.h"
#include"packet.h"

void* TimeServerRun(void* p)
{
	time_server* server = (time_server*)p;
	sync_packet packet;
	socket_address from;

	while(server->run)
	{
		//TODO(martin): use select instead of recvfrom ?

		ssize_t size = SocketReceiveFrom(server->sock, &packet, sizeof(sync_packet), 0, &from);

		u64 t2 = GetSystemTimestamp() + server->offset;

		if(size != sizeof(sync_packet))
		{
			//invalid packet, ignore
			//TODO(martin): maybe we should tolerate size mismatches to allow future extensions with different packet sizes
			printf("Error : invalid packet (size mismatch)\n");
			continue;
		}
		else if(packet.type != SYNC_REQUEST)
		{
			//invalid packet, ignore
			printf("Error : invalid packet (type mismatch)\n");
			continue;
		}
		else
		{
			packet.type = SYNC_REPLY;
			packet.t2 = t2;
			packet.t3 = GetSystemTimestamp() + server->offset;
			if(SocketSendTo(server->sock, &packet, sizeof(sync_packet), 0, &from) == -1)
			{
				perror("Server sendto()");
			}
		}
	}
	return(0);
}

void TimeServerStart(time_server* server, host_port port, i64 offset)
{
	server->offset = offset;
	server->sock = SocketOpen(SOCK_UDP);
	assert(server->sock);

	socket_address addr;
	addr.ip = SOCK_IP_ANY;
	addr.port = HostToNetPort(port);

	int res = SocketBind(server->sock, &addr);
	if(res)
	{
		perror("Failed binding socket");
		assert(0);
	}

	server->run = true;
	server->thread = ThreadCreate(TimeServerRun, server);
}

void TimeServerStop(time_server* server)
{
	server->run = false;
	SocketClose(server->sock);

	void* res;
	ThreadJoin(server->thread, &res);
	ThreadDestroy(server->thread);
}
