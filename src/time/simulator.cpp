/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: poll_receive.cpp
*	@author: Martin Fouilleul
*	@date: 13/07/2019
*	@revision:
*
*****************************************************************/

#include<errno.h>
#include<string.h>	// strerror()
#include<signal.h>	// signal(), to handle Ctrl-C

//DEBUG
#include<stdio.h>	// printf()
#include<assert.h>	// assert()
#include<stdlib.h>	// sranddev(), rand()

#include<queue>

#include"debug_log.h"
#include"packet.h"
#include"platform_time.h"
#include"time_client.h"

#include"platform_thread.h"

struct server_sim
{
	bool exit;
	int64 offset;
	std::queue<sync_packet> queue;
	platform_mutex* m;
	platform_condition* c;

	double upScale;
	double upShape;
	double downScale;
	double downShape;
};

static server_sim _serverSim;

void InitTimeServerSimulator(int64 offset, uint32 seed, double upScale, double upShape, double downScale, double downShape)
{
	_serverSim.offset = offset;
	_serverSim.exit = false;
	_serverSim.m = MutexCreate();
	_serverSim.c = ConditionCreate();
	_serverSim.upScale = upScale;
	_serverSim.upShape = upShape;
	_serverSim.downScale = downScale;
	_serverSim.downShape = downShape;

	srand(seed);
}

void CleanTimeServerSimulator()
{
	MutexLock(_serverSim.m);
	_serverSim.exit = true;
	MutexUnlock(_serverSim.m);
	ConditionSignal(_serverSim.c);

	MutexDestroy(_serverSim.m);
	ConditionDestroy(_serverSim.c);
}

int64 TimeServerSimulatorOffset()
{
	return(_serverSim.offset);
}

double RandomPareto(double scale, double shape)
{
	double uniform = rand()/(double)RAND_MAX;
	double pareto = scale / pow(uniform, 1./shape);
	return(pareto);
}

void PollSimulator(clock_peer* peer, user_clock* clock)
{
	MutexLock(_serverSim.m);
	{
		sync_packet p = {0};
		p.type = SYNC_REQUEST;
		p.t1 = UserClockGetTimestamp(clock);
		_serverSim.queue.push(p);
	}
	MutexUnlock(_serverSim.m);

	ConditionSignal(_serverSim.c);
}

int ReceiveSyncReplySimulator(clock_peer* peer, user_clock* clock, sync_packet* packet, u64* t4)
{
	//NOTE(martin): better time this function....

	MutexLock(_serverSim.m);

	while(!_serverSim.queue.size() && !_serverSim.exit)
	{
		ConditionWait(_serverSim.c, _serverSim.m);
	}
	if(_serverSim.exit)
	{
		MutexUnlock(_serverSim.m);
		return(-1);
	}

	*packet = _serverSim.queue.front();
	_serverSim.queue.pop();

	//NOTE(martin): simulate outward and return delays

	double dly = minimum(RandomPareto(_serverSim.upScale, _serverSim.upShape), 1);
	SystemSleepNanoseconds(dly * 1e9);

	uint64 t2 = GetSystemTimestamp() + _serverSim.offset;

	if(packet->type != SYNC_REQUEST)
	{
		ERROR_PRINTF("Bad packet type\n");
		return(-1);
	}
	else
	{
		packet->type = SYNC_REPLY;
		packet->t2 = t2;
		packet->t3 = GetSystemTimestamp() + _serverSim.offset;
	}

	dly = minimum(RandomPareto(_serverSim.downScale, _serverSim.downShape), 1);
	SystemSleepNanoseconds(dly * 1e9);

	MutexUnlock(_serverSim.m);

	*t4 = UserClockGetTimestamp(clock);

	return(0);
}
