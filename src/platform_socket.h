/*
Copyright (c) Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: platform_socket.h
*	@author: Martin Fouilleul
*	@date: 22/03/2019
*	@revision:
*
*****************************************************************/
#ifndef __PLATFORM_SOCKET_H_
#define __PLATFORM_SOCKET_H_

#include<sys/time.h> // timeval

#include"typedefs.h"


#ifdef __cplusplus
extern "C" {
#endif

//TODO(martin): extend these error codes
const int SOCK_ERR_OK	    = 0,
	  SOCK_ERR_UNKNOWN  = -1,
	  SOCK_ERR_ACCESS   = -2,
	  SOCK_ERR_MEM      = -3,
	  SOCK_ERR_INTR	    = -4,
	  SOCK_ERR_USED	    = -5,
	  SOCK_ERR_BADF	    = -6,
	  SOCK_ERR_ABORT    = -7;

//NOTE(martin): net_ip and net_port are stored in network byte order
//              host_ip and host_port are stored in host byte order
typedef uint32 net_ip;
typedef uint16 net_port;
typedef uint32 host_ip;
typedef uint16 host_port;

struct socket_address
{
	net_ip ip;
	net_port port;
};

//NOTE(martin): these are in host byte order !
const host_ip SOCK_IP_LOOPBACK = 0x7f000001;
const host_ip SOCK_IP_ANY = 0;
const host_port SOCK_PORT_ANY = 0;

net_ip StringToNetIP(const char* addr);
const char* NetIPToString(net_ip ip);

host_ip StringToHostIP(const char* addr);
const char* HostIPToString(host_ip ip);

net_ip HostToNetIP(host_ip ip);
net_port HostToNetPort(host_port port);
host_ip NetToHostIP(net_ip ip);
host_port NetToHostPort(net_port port);

struct platform_socket;

typedef enum { SOCK_UDP, SOCK_TCP } socket_transport;

const int SOCK_MSG_OOB		= 0x01,
	  SOCK_MSG_PEEK		= 0x02,
	  SOCK_MSG_DONTROUTE	= 0x04,
	  SOCK_MSG_WAITALL	= 0x40;

platform_socket* SocketOpen(socket_transport transport);
int SocketClose(platform_socket* socket);

int SocketBind(platform_socket* socket, socket_address* addr);
int SocketListen(platform_socket* socket, int backlog);
platform_socket* SocketAccept(platform_socket* socket, socket_address* from);

int SocketConnect(platform_socket* socket, socket_address* addr);

int64 SocketReceive(platform_socket* socket, void* buffer, uint64 size, int flags);
int64 SocketReceiveFrom(platform_socket* socket, void* buffer, uint64 size, int flags, socket_address* from);

int64 SocketSend(platform_socket* socket, void* buffer, uint64 size, int flags);
int64 SocketSendTo(platform_socket* socket, void* buffer, uint64 size, int flags, socket_address* to);

int SocketGetAddress(platform_socket* socket, socket_address* addr);

int SocketSetReceiveTimeout(platform_socket* socket, timeval* tv);
int SocketSetSendTimeout(platform_socket* socket, timeval* tv);

int SocketSetBroadcast(platform_socket* sock, bool enable);

const uint8 SOCK_ACTIVITY_IN  = 1<<0,
            SOCK_ACTIVITY_OUT = 1<<2,
	    SOCK_ACTIVITY_ERR = 1<<3;

struct socket_activity
{
	platform_socket* sock;
	uint8 watch;
	uint8 set;
};

int SocketSelect(uint32 count, socket_activity* set, double timeout);

int SocketGetLastError();
const char* SocketGetLastErrorMessage();

int SocketGetIFAddresses(int* count, net_ip* ips);

#ifdef __cplusplus
} // extern "C"
#endif


#endif //__PLATFORM_SOCKET_H_
