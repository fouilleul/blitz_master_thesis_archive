/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: directory.cpp
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*****************************************************************/

/*
	Functions of the service directory server, which provides service names lookup to blitz modules
*/


#include<string.h>	// memset(), memcpy(), et al.

#include"debug_log.h"
#include"osc.h"
#include"directory.h"

//-----------------------------------------------------------------------------------------------
// Error strings
//-----------------------------------------------------------------------------------------------

typedef enum
{
	SDERR_NONE = 0,
	SDERR_UNKNOWN_COMMAND,
	SDERR_RECORD_LIMIT,
	SDERR_RECORD_TYPE,
	SDERR_INVALID_ID,
	SDERR_INTERNAL,
	SDERR_PATH_USED,
	SDERR_INVALID_PATH,
	SDERR_INVALID_TYPES

} sd_err;

const char* SD_ERROR_STRINGS[] = {
		"No errors",
		"Unknown service directory command",
		"Can't allocate a new record (max record count reached)",
		"A record was found but it didn't have the expected type",
		"Tried to use an invalid record identifier",
		"The service directory encountered an internal error",
		"Path already in use",
		"Path not found",
		"Invalid type string"};

#define PrintDirectoryError(err) ERROR_PRINTF("%s\n", SD_ERROR_STRINGS[err])

//-----------------------------------------------------------------------------------------------
// object ID helpers
//-----------------------------------------------------------------------------------------------

inline uint32 GetIndexFromID(uint64 ID)
{
	return((uint32)(ID>>32));
}

inline uint32 GetGenFromID(uint64 ID)
{
	return((uint32)(ID & 0xffffffff));
}

inline uint32 GetRecordIndex(service_directory* directory, sd_record* record)
{
	DEBUG_ASSERT(record >= directory->records);
	DEBUG_ASSERT(record - directory->records < directory->pushIndex);
	return(record - directory->records);
}

inline uint64 GetRecordID(service_directory* directory, sd_record* record)
{
	DEBUG_ASSERT(record->genIndex);
	return(((uint64)GetRecordIndex(directory, record))<<32 | (uint64)record->genIndex);
}

inline sd_record* GetRecord(service_directory* directory, uint64 ID)
{
	uint32 index = GetIndexFromID(ID);
	if(index >= directory->pushIndex)
	{
		return(0);
	}
	sd_record* record = &(directory->records[index]);
	if(  record->valid
	  && record->genIndex == GetGenFromID(ID))
	{
		return(record);
	}
	else
	{
		return(0);
	}
}

//-----------------------------------------------------------------------------------------------
// Add / remove records
//-----------------------------------------------------------------------------------------------

void RecordInit(sd_record* record, int genIndex)
{
	record->valid = true;
	record->genIndex = genIndex;
	record->type = RT_NONE;
	record->object = 0;
}

sd_record* RecordAlloc(service_directory* directory)
{
	sd_record* record = 0;
	if(!ListEmpty(&directory->freeList))
	{
		list_info* it = ListPop(&directory->freeList);
		record = ListEntry(it, sd_record, freeListElt);
		DEBUG_ASSERT(!record->valid);
	}
	else
	{
		if(directory->pushIndex == SD_MAX_RECORD_COUNT)
		{
			ERROR_PRINTF("Can't allocate record : reached max record count (%u)\n", SD_MAX_RECORD_COUNT);
			return(0);
		}
		uint32 index = directory->pushIndex++;
		record = &(directory->records[index]);
	}

	RecordInit(record, directory->genIndex++);
	if(!directory->genIndex)
	{
		//NOTE(martin): wrap-around to 1, because 0 is a reserved value for "Invalid ID"
		directory->genIndex = 1;
	}
	return(record);
}

void RecordInvalidate(service_directory* directory, sd_record* record)
{
	record->valid = false;
	ListPush(&directory->freeList, &record->freeListElt);
}

//-----------------------------------------------------------------------------------------------
// Add / remove / search paths
//-----------------------------------------------------------------------------------------------

inline void PathMapInsert(service_directory* directory, const char* path, uint64 ID)
{
	DEBUG_ASSERT(path);
	DEBUG_ASSERT(ID);

	directory->pathToID[std::string(path)] = ID;
}

inline uint64 PathMapFind(service_directory* directory, const char* path)
{
	DEBUG_ASSERT(path);

	path_map::iterator it = directory->pathToID.find(std::string(path));
	if(it == directory->pathToID.end())
	{
		return(0);
	}
	else
	{
		return(it->second);
	}
}

inline void PathMapRemove(service_directory* directory, const char* path)
{
	path_map::iterator it = directory->pathToID.find(std::string(path));
	if(it != directory->pathToID.end())
	{
		directory->pathToID.erase(it);
	}
}

inline void PathMapRemoveID(service_directory* directory, uint64 ID)
{
	for(path_map::iterator it = directory->pathToID.begin();
	    it != directory->pathToID.end();
	    it++)
	{
		if(it->second == ID)
		{
			directory->pathToID.erase(it);
			break;
		}
	}
}


//-----------------------------------------------------------------------------------------------
// TCP/Osc Messages helper
//-----------------------------------------------------------------------------------------------

inline int SendTCPOscPacket(platform_socket* sock, osc_element* elt)
{
	uint32 oscSize = OscElementSize(elt);
	char oscSizeBuff[4];
	OscFromInt32(oscSizeBuff, oscSize);

	SocketSend(sock, oscSizeBuff, sizeof(uint32), 0);
	SocketSend(sock, (void*)OscElementData(elt), oscSize, 0);
	//TODO(martin): check and return errors
	return(0);
}

inline void SuccessReply(platform_socket* sock)
{
	const int buffSize = 256;
	char buffer[buffSize];
	osc_element elt;

	OscFormat(&elt, buffSize, buffer, "/OK!", "");
	SendTCPOscPacket(sock, &elt);
}

void SuccessReplyWithID(uint64 ID, platform_socket* sock)
{
	const int buffSize = 256;
	char buffer[buffSize];
	osc_element elt;

	OscFormat(&elt, buffSize, buffer, "/OK!", "h", ID);
	OscElementPrint(&elt);
	SendTCPOscPacket(sock, &elt);
}

void SuccessReplyWithAddress(socket_address* address, platform_socket* sock)
{
	const int buffSize = 256;
	char buffer[buffSize];
	osc_element elt;

	OscFormat(&elt, buffSize, buffer, "/OK!", "ii", NetToHostIP(address->ip), NetToHostPort(address->port));
	SendTCPOscPacket(sock, &elt);
}


void FailureReply(sd_err err, platform_socket* sock)
{
	const int buffSize = 256;
	char buffer[buffSize];
	osc_element elt;

	OscFormat(&elt, buffSize, buffer, "/FAIL", "is", err, SD_ERROR_STRINGS[err]);
	SendTCPOscPacket(sock, &elt);
}

//-----------------------------------------------------------------------------------------------
// Service directory functions
//-----------------------------------------------------------------------------------------------

service_directory::service_directory()
{
	memset(records, 0, sizeof(sd_record)*SD_MAX_RECORD_COUNT);
	ListInit(&freeList);
	pushIndex = 0;
	genIndex = 1;
}

sd_record* CheckRecordWithExpectedType(service_directory* directory, uint64 ID, record_type expectedType, sd_err& err)
{
	sd_record* record = GetRecord(directory, ID);
	if(!record)
	{
		err = SDERR_INVALID_ID;
		PrintDirectoryError(err);
		return(0);
	}
	else if(record->type != expectedType)
	{
		err = SDERR_RECORD_TYPE;
		PrintDirectoryError(err);
		return(0);
	}
	err = SDERR_NONE;
	return(record);
}


sd_err GetOrCreateChannel(service_directory* directory,
		       const char* path,
		       sd_record*& channelRecord,
		       channel_info*& channel,
		       uint64& channelID)
{
	//NOTE(martin): get or create channel
	//TODO(martin): extract that part from HandleRegisterPublisher() and HandleSubscribe()

	channelID = PathMapFind(directory, path);
	if(!channelID)
	{
		channelRecord = RecordAlloc(directory);
		if(!channelRecord)
		{
			return(SDERR_RECORD_LIMIT);
		}
		channel = new channel_info;
		channelRecord->type = RT_CHANNEL;
		channelRecord->object = channel;

		channelID = GetRecordID(directory, channelRecord);
		PathMapInsert(directory, path, channelID);
	}
	else
	{
		sd_err err = SDERR_NONE;
		channelRecord = CheckRecordWithExpectedType(directory, channelID, RT_CHANNEL, err);
		if(!channelRecord)
		{
			return(err);
		}
		channel = (channel_info*)channelRecord->object;
	}
	DEBUG_ASSERT(channel);

	return(SDERR_NONE);
}

void HandleRegisterPublisher(service_directory* directory,
			     osc_msg* msg,
			     platform_socket* sock,
			     net_ip fromIP)
{
	//NOTE(martin): check and extract arguments

	if(strcmp(OscTypeTags(msg),"si") != 0)
	{
		ERROR_PRINTF("Invalid type string\n");
		FailureReply(SDERR_INVALID_TYPES, sock);
		return;
	}
	osc_arg_iterator arg = OscArgumentsBegin(msg);
	const char* path = OscAsStringUnchecked(&arg); OscArgumentNext(&arg);
	net_port port = HostToNetPort((host_port)OscAsInt32Unchecked(&arg));

	//NOTE(martin): get or create channel
	//TODO(martin): extract that part from HandleRegisterPublisher() and HandleSubscribe()

	sd_record* channelRecord = 0;
	channel_info* channel = 0;
	uint64 channelID = 0;

	sd_err err = GetOrCreateChannel(directory, path, channelRecord, channel, channelID);
	if(err != SDERR_NONE)
	{
		PrintDirectoryError(err);
		FailureReply(err, sock);
		return;
	}

	//NOTE(martin): create our publisher and add it to the channel

	sd_record* pubRecord = RecordAlloc(directory);
	if(!pubRecord)
	{
		PrintDirectoryError(SDERR_RECORD_LIMIT);
		FailureReply(SDERR_RECORD_LIMIT, sock);
		return;
	}

	publisher_info* pub = new publisher_info;
	pub->channelID = channelID;
	pub->address.ip = fromIP;
	pub->address.port = port;

	pubRecord->type = RT_PUBLISHER;
	pubRecord->object = pub;

	uint64 pubID = GetRecordID(directory, pubRecord);

	channel->publishers.push_back(pub);

	//NOTE(martin): send each subscriber to the publishers' notification port

	osc_element subMsg;
	const int buffSize = 512;
	char buffer[buffSize];

	platform_socket* subSock = SocketOpen(SOCK_TCP);
	SocketConnect(subSock, &pub->address);

	for(sub_vec::iterator it = channel->subscribers.begin();
	    it != channel->subscribers.end();
	    it++)
	{
		socket_address& subAddress = (*it)->address;

		OscFormat(&subMsg, buffSize, buffer, "/sub", "ii",
			  NetToHostIP(subAddress.ip),
			  NetToHostPort(subAddress.port));

		SendTCPOscPacket(subSock, &subMsg);
	}
	SocketClose(subSock);

	//NOTE(martin): send success reply
	SuccessReplyWithID(pubID, sock);
}

void HandleUnregisterPublisher(service_directory* directory,
		       osc_msg* msg,
		       platform_socket* sock,
		       net_ip fromIP)
{
	//NOTE(martin): check and extract arguments

	if(strcmp(OscTypeTags(msg),"h") != 0)
	{
		ERROR_PRINTF("Invalid type string\n");
		FailureReply(SDERR_INVALID_TYPES, sock);
		return;
	}
	osc_arg_iterator arg = OscArgumentsBegin(msg);
	uint64 pubID = OscAsInt64Unchecked(&arg);

	//NOTE(martin): get the publisher

	sd_err err = SDERR_NONE;
	sd_record* pubRecord = CheckRecordWithExpectedType(directory, pubID, RT_PUBLISHER, err);
	if(!pubRecord)
	{
		FailureReply(err, sock);
		return;
	}
	DEBUG_ASSERT(pubRecord->object);
	publisher_info* pub = (publisher_info*)pubRecord->object;

	//NOTE(martin): get the channel

	uint64 channelID = pub->channelID;	//NOTE(martin): we need this later
	sd_record* chanRecord = CheckRecordWithExpectedType(directory, channelID, RT_CHANNEL, err);
	if(!chanRecord)
	{
		FailureReply(err, sock);
		return;
	}
	DEBUG_ASSERT(chanRecord->object);
	channel_info* channel = (channel_info*)chanRecord->object;

	//NOTE(martin): remove the publisher

	for(pub_vec::iterator it = channel->publishers.begin();
	    it != channel->publishers.end();
	    it++)
	{
		if((*it) == pub)
		{
			channel->publishers.erase(it);
			break;
		}
	}

	RecordInvalidate(directory, pubRecord);
	delete pub;

	//NOTE(martin): remove the channel if there's no more pub/sub on it

	if(  !channel->publishers.size()
	  && !channel->subscribers.size())
	{
		PathMapRemoveID(directory, channelID);
		RecordInvalidate(directory, chanRecord);
		delete channel;
	}

	//NOTE(martin): send success reply
	SuccessReply(sock);
}

void HandleSubscribe(service_directory* directory,
		       osc_msg* msg,
		       platform_socket* sock,
		       net_ip fromIP)
{
	//NOTE(martin): check and extract arguments

	if(strcmp(OscTypeTags(msg),"si") != 0)
	{
		PrintDirectoryError(SDERR_INVALID_TYPES);
		FailureReply(SDERR_INVALID_TYPES, sock);
		return;
	}
	osc_arg_iterator arg = OscArgumentsBegin(msg);
	const char* path = OscAsStringUnchecked(&arg); OscArgumentNext(&arg);
	net_port port = HostToNetPort((host_port)OscAsInt32Unchecked(&arg));

	//NOTE(martin): get or create channel

	sd_record* channelRecord = 0;
	channel_info* channel = 0;
	uint64 channelID = 0;

	sd_err err = GetOrCreateChannel(directory, path, channelRecord, channel, channelID);
	if(err != SDERR_NONE)
	{
		PrintDirectoryError(err);
		FailureReply(err, sock);
		return;
	}

	//NOTE(martin): create our subscriber and add it to the channel

	sd_record* subRecord = RecordAlloc(directory);
	if(!subRecord)
	{
		PrintDirectoryError(SDERR_RECORD_LIMIT);
		FailureReply(SDERR_RECORD_LIMIT, sock);
		return;
	}

	subscriber_info* sub = new subscriber_info;
	sub->channelID = channelID;
	sub->address.ip = fromIP;
	sub->address.port = port;

	subRecord->type = RT_SUBSCRIBER;
	subRecord->object = sub;

	uint64 subID = GetRecordID(directory, subRecord);

	channel->subscribers.push_back(sub);

	//NOTE(martin): send a subscription request to each publisher's notification port

	osc_element subMsg;
	const int buffSize = 512;
	char buffer[buffSize];
	OscFormat(&subMsg, buffSize, buffer, "/sub", "ii",
		  NetToHostIP(fromIP),
		  NetToHostPort(port));

	for(pub_vec::iterator it = channel->publishers.begin();
	    it != channel->publishers.end();
	    it++)
	{
		publisher_info* pub = *it;

		platform_socket* pubSock = SocketOpen(SOCK_TCP);
		SocketConnect(pubSock, &pub->address);
		SendTCPOscPacket(pubSock, &subMsg);
		SocketClose(pubSock);
	}

	//NOTE(martin): send success reply
	SuccessReplyWithID(subID, sock);
}

void HandleUnsubscribe(service_directory* directory,
		       osc_msg* msg,
		       platform_socket* sock,
		       net_ip fromIP)
{
	//NOTE(martin): check and extract arguments

	if(strcmp(OscTypeTags(msg),"h") != 0)
	{
		ERROR_PRINTF("Invalid type string\n");
		FailureReply(SDERR_INVALID_TYPES, sock);
		return;
	}
	osc_arg_iterator arg = OscArgumentsBegin(msg);
	uint64 subID = OscAsInt64Unchecked(&arg);

	//NOTE(martin): get the subscriber

	sd_err err = SDERR_NONE;
	sd_record* subRecord = CheckRecordWithExpectedType(directory, subID, RT_SUBSCRIBER, err);
	if(!subRecord)
	{
		FailureReply(err, sock);
		return;
	}
	DEBUG_ASSERT(subRecord->object);

	subscriber_info* sub = (subscriber_info*)subRecord->object;

	//NOTE(martin): get the channel
	uint64 channelID = sub->channelID;
	sd_record* chanRecord = CheckRecordWithExpectedType(directory, channelID, RT_CHANNEL, err);
	if(!chanRecord)
	{
		FailureReply(err, sock);
		return;
	}
	DEBUG_ASSERT(chanRecord->object);

	channel_info* channel = (channel_info*)chanRecord->object;

	//NOTE(martin): remove the subscriber

	for(sub_vec::iterator it = channel->subscribers.begin();
	    it != channel->subscribers.end();
	    it++)
	{
		if((*it) == sub)
		{
			channel->subscribers.erase(it);
			break;
		}
	}

	//NOTE(martin): send an unsubscribe request to all publishers

	osc_element subMsg;
	const int buffSize = 512;
	char buffer[buffSize];
	OscFormat(&subMsg, buffSize, buffer, "/unsub", "ii",
		  NetToHostIP(sub->address.ip),
		  NetToHostPort(sub->address.port));

	for(pub_vec::iterator it = channel->publishers.begin();
	    it != channel->publishers.end();
	    it++)
	{
		publisher_info* pub = *it;
		platform_socket* pubSock = SocketOpen(SOCK_TCP);
		SocketConnect(pubSock, &pub->address);
		SendTCPOscPacket(pubSock, &subMsg);
		SocketClose(pubSock);
	}

	//NOTE(martin): invalidate subscriber record and delete publisher object

	RecordInvalidate(directory, subRecord);
	delete sub;

	//NOTE(martin): remove the channel if there's no more pub/sub on it

	if(  !channel->publishers.size()
	  && !channel->subscribers.size())
	{
		PathMapRemoveID(directory, channelID);
		RecordInvalidate(directory, chanRecord);
		delete channel;
	}

	//NOTE(martin): send success reply
	SuccessReply(sock);
}

void HandleRegisterRPC(service_directory* directory,
		       osc_msg* msg,
		       platform_socket* sock,
		       net_ip fromIP)
{
	//NOTE(martin): check and extract arguments
	if(strcmp(OscTypeTags(msg),"si") != 0)
	{
		ERROR_PRINTF("Invalid type string\n");
		FailureReply(SDERR_INVALID_TYPES, sock);
		return;
	}
	osc_arg_iterator arg = OscArgumentsBegin(msg);
	const char* path = OscAsStringUnchecked(&arg); OscArgumentNext(&arg);
	net_port port = HostToNetPort(OscAsInt32Unchecked(&arg));

	//NOTE(martin): verify that the rpc service doesn't exist yet

	uint64 ID = PathMapFind(directory, path);
	if(ID)
	{
		//error
		PrintDirectoryError(SDERR_PATH_USED);
		FailureReply(SDERR_PATH_USED, sock);
		return;
	}

	//NOTE(martin): create the rpc service

	rpc_info* rpc = new rpc_info;
	rpc->address.ip = fromIP;
	rpc->address.port = port;

	sd_record* record = RecordAlloc(directory);
	if(!record)
	{
		PrintDirectoryError(SDERR_RECORD_LIMIT);
		FailureReply(SDERR_RECORD_LIMIT, sock);
	}
	record->type = RT_RPC;
	record->object = rpc;

	ID = GetRecordID(directory, record);
	PathMapInsert(directory, path, ID);

	//NOTE(martin): send success reply
	SuccessReplyWithID(ID, sock);
}

void HandleUnregisterRPC(service_directory* directory,
			 osc_msg* msg,
			 platform_socket* sock,
			 net_ip fromIP)
{
	//NOTE(martin): check and extract arguments
	if(strcmp(OscTypeTags(msg),"h") != 0)
	{
		ERROR_PRINTF("Invalid type string\n");
		FailureReply(SDERR_INVALID_TYPES, sock);
		return;
	}
	osc_arg_iterator arg = OscArgumentsBegin(msg);
	uint64 ID = OscAsInt64Unchecked(&arg);

	//NOTE(martin): get the rpc service

	if(!ID)
	{
		WARNING_PRINTF("ID not found for rpc unregister request\n");
		SuccessReply(sock);
		return;
	}
	sd_err err = SDERR_NONE;
	sd_record* record = CheckRecordWithExpectedType(directory, ID, RT_RPC, err);
	if(!record)
	{
		FailureReply(err, sock);
	}
	DEBUG_ASSERT(record->object);

	rpc_info* rpc = (rpc_info*)record->object;

	//NOTE(martin): unmap and invalidate rpc record, and delete the rpc object

	PathMapRemoveID(directory, ID);
	RecordInvalidate(directory, record);
	delete rpc;

	//NOTE(martin): reply

	SuccessReply(sock);
}

void HandleGetRPC(service_directory* directory,
		  osc_msg* msg,
		  platform_socket* sock,
		  net_ip fromIP)
{
	//NOTE(martin): check and extract arguments

	if(strcmp(OscTypeTags(msg),"s") != 0)
	{
		ERROR_PRINTF("Invalid type string\n");
		FailureReply(SDERR_INVALID_TYPES, sock);
		return;
	}
	osc_arg_iterator arg = OscArgumentsBegin(msg);
	const char* path = OscAsStringUnchecked(&arg);

	//NOTE(martin): get the rpc service

	uint64 ID = PathMapFind(directory, path);
	if(!ID)
	{
		PrintDirectoryError(SDERR_INVALID_PATH);
		FailureReply(SDERR_INVALID_PATH, sock);
		return;
	}
	sd_err err = SDERR_NONE;
	sd_record* record = CheckRecordWithExpectedType(directory, ID, RT_RPC, err);
	if(!record)
	{
		FailureReply(err, sock);
		return;
	}
	DEBUG_ASSERT(record->object);

	rpc_info* rpc = (rpc_info*)record->object;

	SuccessReplyWithAddress(&rpc->address, sock);
}

void ServiceDirectoryReceiveOscPacket(service_directory* directory,
                                      platform_socket* sock,
				      net_ip fromIP,
				      const char* buffer,
				      uint32 oscSize)
{
	osc_msg msg;
	OscErrCode err = OscParseMessage(&msg, oscSize, buffer);
	if(err != OSC_OK)
	{
		ERROR_PRINTF("Invalid OSC packet (%i) : %s\n", err, OscGetErrorMessage(err));
		return;
	}
	else
	{
		DEBUG_PRINTF("Received an OSC message :\n\t");
		DEBUG_OSC_MSG_PRINT(&msg);
	}

	//NOTE(martin): dispatch message

	const char* address = OscAddressPattern(&msg);

	if(!strcmp(address, "/regpub"))
	{
		HandleRegisterPublisher(directory, &msg, sock, fromIP);
	}
	else if(!strcmp(address, "/unpub"))
	{
		HandleUnregisterPublisher(directory, &msg, sock, fromIP);
	}
	else if(!strcmp(address, "/sub"))
	{
		HandleSubscribe(directory, &msg, sock, fromIP);
	}
	else if(!strcmp(address, "/unsub"))
	{
		HandleUnsubscribe(directory, &msg, sock, fromIP);
	}
	else if(!strcmp(address, "/regrpc"))
	{
		HandleRegisterRPC(directory, &msg, sock, fromIP);
	}
	else if(!strcmp(address, "/unrpc"))
	{
		HandleUnregisterRPC(directory, &msg, sock, fromIP);
	}
	else if(!strcmp(address, "/getrpc"))
	{
		HandleGetRPC(directory, &msg, sock, fromIP);
	}
	else
	{
		FailureReply(SDERR_UNKNOWN_COMMAND, sock);
		//print error
	}
}
