/*
Copyright (c) 2019 Martin Fouilleul

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/************************************************************//**
*
*	@file: main.h
*	@author: Martin Fouilleul
*	@date: 18/03/2019
*	@revision:
*
*	Max external providing an interface to the Blitz bus
*
*****************************************************************/

#include "ext.h"		// standard Max include, always required
#include "ext_obex.h"		// required for new style Max object

#include"blitz.h"
#include"platform_thread.h"

//---------------------------------------------------------------------
// odot "fullPacket" macros
//---------------------------------------------------------------------

#define OMAX_UTIL_GET_LEN_AND_PTR					\
if(argc != 2){								\
object_error((t_object *)x, "expected 2 arguments but got %d", argc);	\
return;									\
	}								\
if(atom_gettype(argv) != A_LONG){					\
object_error((t_object *)x, "argument 1 should be an int");		\
return;									\
	}								\
if(atom_gettype(argv + 1) != A_LONG){					\
object_error((t_object *)x, "argument 2 should be an int");		\
return;									\
	}								\
long len = atom_getlong(argv);						\
char *ptr = (char *)atom_getlong(argv + 1);

//---------------------------------------------------------------------
// object struct
//---------------------------------------------------------------------

struct _blitz;

typedef void (*OpenFunction)(_blitz*, const char*);
typedef void (*CloseFunction)(_blitz*);
typedef void (*OscFunction)(_blitz*, osc_element*);
typedef void (*ConnectFunction)(_blitz*, net_ip, net_port);
typedef void (*DisconnectFunction)(_blitz*);
typedef void (*ListenFunction)(_blitz*, net_port);
typedef void (*DisconnectFunction)(_blitz*);

const int MAX_CLIENTS = 100;

enum {	MODE_RAW,
	MODE_RPC_LINK,
	MODE_RPC_SERVICE,
	MODE_PUBLISHER,
	MODE_SUBSCRIBER,
	MODE_UDP,
	MODE_TCP
 };

 enum
 {
	TCP_NONE,
	TCP_LISTEN,
	TCP_CONNECT
 };

typedef struct _blitz
{
	t_object	ob;
	void*		out;
	void*		outTime;
	void*		outPeer;

	OpenFunction	open;
	CloseFunction	close;
	OscFunction	osc;
	ConnectFunction connect;
	DisconnectFunction disconnect;
	ListenFunction  listen;

	int mode;

	blitz_context*  context;
	bool		connected;

	rpc_link*	rpcLink;
	rpc_service*	rpcService;
	rpc_client*	clients[MAX_CLIENTS];

	publisher_link* publisher;
	subscriber_link* subscriber;

	platform_thread* listener;

	socket_address sendTo;
	socket_address recvFrom;

	platform_socket* sock;
	platform_socket* client;
	int tcpRole;

	uint64 stamp;

} t_blitz;

//---------------------------------------------------------------------
// global class pointer variable
//---------------------------------------------------------------------

t_class *blitz_class;

//---------------------------------------------------------------------
// utility functions
//---------------------------------------------------------------------

int OscFromMax(osc_element* elt, int buffSize, char* buffer, int argc, t_atom* argv)
{
	if(argc<1)
	{
		error("Osc message must have at least an address pattern");
		return(-1);
	}

	if(atom_gettype(argv) != A_SYM)
	{
		error("OSC first atom must be a symbol representing the address pattern");
		return(-1);
	}

	OscBeginPacket(elt, buffSize, buffer);
	OscBeginMessage(elt, atom_getsym(argv)->s_name);

	if(argc > 1)
	{
		if(atom_gettype(argv+1) != A_SYM)
		{
			error("OSC second atom must be a symbol representing the type string");
			return(-1);
		}
		auto handle_err = []() -> int {error("Bad type in osc message arguments"); return(-1);};

		const char* tag = atom_getsym(argv+1)->s_name;
		t_atom* end = argv + argc;
		argv += 2;

		while(*tag != '\0' && argv < end)
		{
			switch(*tag)
			{
				case TRUE_TYPE_TAG:
					if(OscPushBool(elt, true) != OSC_OK) { return(handle_err());}
					break;
				case FALSE_TYPE_TAG:
					if(OscPushBool(elt, false) != OSC_OK) { return(handle_err());}
					break;
				case NIL_TYPE_TAG:
					if(OscPushNil(elt) != OSC_OK) { return(handle_err());}
					break;
				case INFINITUM_TYPE_TAG:
					if(OscPushInfinity(elt) != OSC_OK) { return(handle_err());}
					break;
				case ARRAY_BEGIN_TYPE_TAG:
					if(OscArrayBegin(elt) != OSC_OK) { return(handle_err());}
					break;
				case ARRAY_END_TYPE_TAG:
					if(OscArrayEnd(elt) != OSC_OK) { return(handle_err());}
					break;

				case INT16_TYPE_TAG:
					if(atom_gettype(argv) != A_LONG
					  || OscPushInt16(elt, (int16)atom_getlong(argv++)) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case INT32_TYPE_TAG:
					if(atom_gettype(argv) != A_LONG
					  || OscPushInt32(elt, (int32)atom_getlong(argv++)) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case FLOAT_TYPE_TAG:
					if(atom_gettype(argv) != A_FLOAT
					  || OscPushFloat(elt, atom_getfloat(argv++)) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case CHAR_TYPE_TAG:
					if(atom_gettype(argv) != A_SYM
					  || OscPushChar(elt, atom_getsym(argv++)->s_name[0]) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case RGBA_COLOR_TYPE_TAG:
					if(atom_gettype(argv) != A_LONG
					  || OscPushRgbaColor(elt, atom_getlong(argv++)) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case MIDI_MESSAGE_TYPE_TAG:
					if(atom_gettype(argv) != A_LONG
					  || OscPushMidiMessage(elt, atom_getlong(argv++)) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case INT64_TYPE_TAG:
					if(atom_gettype(argv) != A_LONG
					  || OscPushInt64(elt, atom_getlong(argv++)) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case TIME_TAG_TYPE_TAG:
					if(atom_gettype(argv) != A_LONG
					  || OscPushTimeTag(elt, atom_getlong(argv++)) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case DOUBLE_TYPE_TAG:
					if(atom_gettype(argv) != A_FLOAT
					  || OscPushDouble(elt, atom_getfloat(argv++)) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case STRING_TYPE_TAG:
					if(atom_gettype(argv) != A_SYM
					  || OscPushString(elt, atom_getsym(argv++)->s_name) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				case SYMBOL_TYPE_TAG:
					if(atom_gettype(argv) != A_SYM
					  || OscPushSymbol(elt, atom_getsym(argv++)->s_name) != OSC_OK)
					{
						return(handle_err());
					}
					break;

				default:
					ERROR_PRINTF("Unkown or unsupported type tag\n");
					return(handle_err());
			}
			tag++;
		}
		if(*tag != '\0' || argv != end)
		{
			error("error parsing osc arguments");
			return(-1);
		}

	}
	OscEndMessage(elt);
	OscEndPacket(elt);
	return(0);
}

void OutputOscMessage(t_blitz* x, osc_msg* msg)
{
	// reserve space for arg count + address + tags
	t_atom* atomBuff = (t_atom*)alloca(sizeof(t_atom) * (OscArgumentsCount(msg) + 2));

	atom_setsym(atomBuff, gensym(OscAddressPattern(msg)));
	const char* tags = OscTypeTags(msg);

	t_atom* atom = atomBuff+1;
	if(tags)
	{
		atom_setsym(atom++, gensym(OscTypeTags(msg)));

		for(osc_arg_iterator arg = OscArgumentsBegin(msg);
		    arg != OscArgumentsEnd(msg);
		    OscArgumentNext(&arg))
		{
			switch(OscArgumentType(&arg))
			{
				case TRUE_TYPE_TAG:
				case FALSE_TYPE_TAG:
				case NIL_TYPE_TAG:
				case INFINITUM_TYPE_TAG:
				case ARRAY_BEGIN_TYPE_TAG:
				case ARRAY_END_TYPE_TAG:

				case INT16_TYPE_TAG:
					atom_setlong(atom++, OscAsInt16Unchecked(&arg));
					break;

				case INT32_TYPE_TAG:
					atom_setlong(atom++, OscAsInt32Unchecked(&arg));
					break;

				case FLOAT_TYPE_TAG:
					atom_setfloat(atom++, OscAsFloatUnchecked(&arg));
					break;

				case CHAR_TYPE_TAG:
				{
					char c[2];
					c[0] = OscAsCharUnchecked(&arg);
					c[1] = '\0';
					atom_setsym(atom++, gensym(c));
				} break;

				case RGBA_COLOR_TYPE_TAG:
					atom_setlong(atom++, OscAsRgbaColorUnchecked(&arg));
					break;

				case MIDI_MESSAGE_TYPE_TAG:
					atom_setlong(atom++, OscAsMidiMessageUnchecked(&arg));
					break;

				case INT64_TYPE_TAG:
					atom_setlong(atom++, OscAsInt64Unchecked(&arg));
					break;

				case TIME_TAG_TYPE_TAG:
					atom_setlong(atom++, OscAsTimeTagUnchecked(&arg));
					break;

				case DOUBLE_TYPE_TAG:
					atom_setfloat(atom++, OscAsDoubleUnchecked(&arg));
					break;

				case STRING_TYPE_TAG:
					atom_setsym(atom++, gensym(OscAsStringUnchecked(&arg)));
					break;

				case SYMBOL_TYPE_TAG:
					atom_setsym(atom++, gensym(OscAsSymbolUnchecked(&arg)));
					break;

				default:
					error("Unknown type tag %c", *tags);
					ERROR_PRINTF("Unkown or unsupported type tag\n");
					return;
			}
		}
	}
	outlet_anything(x->out, gensym("osc"), atom - atomBuff, atomBuff);
}

void OutputOscBundle(t_blitz* x, osc_element* bundle)
{
	if(OscElementIsBundle(bundle))
	{
		for(osc_element it = OscElementsBegin(bundle);
		    it != OscElementsEnd(bundle);
		    it++)
		{
			OutputOscBundle(x, &it);
		}
	}
	else
	{
		osc_msg* msg = 0;
		if(OscElementAsMessage(bundle, &msg) == OSC_OK)
		{
			OutputOscMessage(x, msg);
		}
		// else error(...)
	}
}

//---------------------------------------------------------------------
// rpc_link functions
//---------------------------------------------------------------------

void OpenRPCLink(t_blitz* x, const char* path)
{
	if(x->rpcLink)
	{
		error("Link already opened");
		return;
	}
	if(!x->context)
	{
		error("No open context, use \"server\" message to setup a context first");
		return;
	}

	x->rpcLink = BlitzRPCOpenLink(x->context, path);
	if(!x->rpcLink)
	{
		error("Can't open link");
		return;
	}
}

void CloseRPCLink(t_blitz* x)
{
	if(!x->context)
	{
		error("No open context, use \"server\" message to setup a context first");
		return;
		//TODO(martin): maybe we should nullify the link anyway ??
	}

	if(!x->rpcLink)
	{
		error("Link already closed");
		return;
	}
	BlitzRPCCloseLink(x->context, x->rpcLink);
	x->rpcLink = 0;
}

void OscRPCLink(t_blitz* x, osc_element* bundle)
{
	if(!x->rpcLink)
	{
		error("Link is not open, can't make an rpc call");
		return;
	}
	const int buffSize = 4096;
	char buffer[buffSize];
	osc_element reply;

	if(BlitzRPCCall(x->rpcLink, bundle, &reply, buffSize, buffer) == RPC_STATUS_OK)
	{
		OutputOscBundle(x, &reply);
	}
	else
	{
		error("RPC Call failed");
	}
}

//---------------------------------------------------------------------
// rpc_service functions
//---------------------------------------------------------------------

int RPCCallback(rpc_client* client, osc_element* request, void* user)
{
	t_blitz* x = (t_blitz*)user;

	//NOTE(martin): store client, associate it with an id, output the request along with the id...

	for(int i=0; i<MAX_CLIENTS; i++)
	{
		if(x->clients[i] == 0)
		{
			x->clients[i] = client;
			outlet_int(x->outPeer, i);
			OutputOscBundle(x, request);
			return(0);
		}
	}
	object_error((object*)x, "MAX_CLIENTS limit reached for RPC requests");
	return(-1);
}

void OpenRPCService(t_blitz* x, const char* path)
{
	if(x->rpcService)
	{
		error("Service already opened");
		return;
	}
	if(!x->context)
	{
		error("No open context, use \"server\" message to setup a context first");
		return;
	}

	x->rpcService = BlitzRPCOpenService(x->context, path, RPCCallback, x);
	if(!x->rpcService)
	{
		error("Can't open service");
		return;
	}
}

void CloseRPCService(t_blitz* x)
{
	if(!x->context)
	{
		error("No open context, use \"server\" message to setup a context first");
		return;
		//TODO(martin): maybe we should nullify the link anyway ??
	}

	if(!x->rpcService)
	{
		error("Service already closed");
		return;
	}
	BlitzRPCCloseService(x->context, x->rpcService);
	x->rpcService = 0;
}

//---------------------------------------------------------------------
// publisher functions
//---------------------------------------------------------------------

void OpenPublisher(t_blitz* x, const char* path)
{
	if(x->publisher)
	{
		error("publisher already opened");
		return;
	}
	if(!x->context)
	{
		error("No open context, use \"server\" message to setup a context first");
		return;
	}

	x->publisher = BlitzOpenPublisher(x->context, path);
	if(!x->publisher)
	{
		error("Can't open link");
		return;
	}
}

void ClosePublisher(t_blitz* x)
{
	if(!x->context)
	{
		error("No open context, use \"server\" message to setup a context first");
		return;
		//TODO(martin): maybe we should nullify the link anyway ??
	}

	if(!x->publisher)
	{
		error("publisher already closed");
		return;
	}
	BlitzClosePublisher(x->context, x->publisher);
	x->publisher = 0;
}

void OscPublisher(t_blitz* x, osc_element* bundle)
{
	if(!x->publisher)
	{
		error("Link is not open, can't make an rpc call");
		return;
	}
	BlitzPublishElement(x->publisher, x->stamp, bundle);
}

//---------------------------------------------------------------------
// subscriber functions
//---------------------------------------------------------------------


void ScheduledOutput(t_blitz *x, t_symbol *s, short argc, t_atom *argv)
{
	// used to output arguments in a scheduled way
	osc_element* bundle = (osc_element*)atom_getobj(argv);
	OutputOscBundle(x, bundle);
}

void SubscriberCallback(const char* channel, uint64 timetag, osc_element* bundle, void* user)
{
	t_blitz* x = (t_blitz*)user;

#if 0
	OutputOscBundle(x, bundle);
#else
	int argc = 1;
	t_atom argv;
	atom_setobj(&argv, bundle);

	float delay = 1000 * SignedTimestampToSeconds(timetag - BlitzTimestamp(x->context));
	if(delay <= 0)
	{
		OutputOscBundle(x, bundle);
	}
	else
	{
		schedule_delay(x, (method)ScheduledOutput, delay, gensym(""), argc, &argv);
	}

//	post("scheduled function in %f ms", delay);
#endif
}



void OpenSubscriber(t_blitz* x, const char* path)
{
	if(x->subscriber)
	{
		error("subscriber already opened");
		return;
	}
	if(!x->context)
	{
		error("No open context, use \"server\" message to setup a context first");
		return;
	}

	x->subscriber = BlitzSubscribe(x->context, path, SubscriberCallback, x);
	if(!x->subscriber)
	{
		error("Can't open link");
		return;
	}
}

void CloseSubscriber(t_blitz* x)
{
	if(!x->context)
	{
		error("No open context, use \"server\" message to setup a context first");
		return;
		//TODO(martin): maybe we should nullify the link anyway ??
	}

	if(!x->subscriber)
	{
		error("subscriber already closed");
		return;
	}
	BlitzUnsubscribe(x->context, x->subscriber);
	x->subscriber = 0;
}


//---------------------------------------------------------------------
// udp mode functions
//---------------------------------------------------------------------


void* UdpListener(void* user)
{
	t_blitz* x = (t_blitz*)user;

	const int buffSize = 4096;
	char buffer[buffSize];

	while(true)
	{
		int size = SocketReceiveFrom(x->sock, buffer, buffSize, 0, &x->recvFrom);

		if(size > 0)
		{
			osc_element bundle;
			if(OscParsePacket(&bundle, size, buffer) != 0)
			{
				object_error((object*)x, "Unrecognized packet");
				continue;
			}

			t_atom atoms[2];
			atom_setsym(atoms, gensym(NetIPToString(x->recvFrom.ip)));
			atom_setlong(atoms+1, NetToHostPort(x->recvFrom.port));
			outlet_anything(x->outPeer, gensym("from"), 2, atoms);

			OutputOscBundle(x, &bundle);
		}
		else
		{
			if(SocketGetLastError() != SOCK_ERR_BADF)
			{
				object_error((object*)x,
				             "Error in SocketReceive() : %s",
					     SocketGetLastErrorMessage());
			}
			//NOTE(martin): else, this is us closing the socket from the main thread
			break;
		}
	}
	return(0);
}

void ConnectUdp(t_blitz* x, net_ip ip, net_port port)
{
	x->sendTo.ip = ip;
	x->sendTo.port = port;
}

void ListenUdp(t_blitz* x, net_port port)
{
	if(x->listener)
	{
		SocketClose(x->sock);
		void* r;
		ThreadJoin(x->listener, &r);
	}
	socket_address addr;
	addr.ip = SOCK_IP_ANY;
	addr.port = port;

	x->sock = SocketOpen(SOCK_UDP);

	if(SocketBind(x->sock, &addr) != 0)
	{
		object_error((object*)x, "Can't bind to port %i : %s", port, SocketGetLastErrorMessage());
		SocketClose(x->sock);
		x->sock = 0;
		return;
	}
	x->listener = ThreadCreate(UdpListener, x);
}

void OscUdp(t_blitz* x, osc_element* bundle)
{
	//todo: verify that socket is open etc.
	if(SocketSendTo(x->sock, (void*)OscElementData(bundle), OscElementSize(bundle), 0, &x->sendTo) < 0)
	{
		object_error((object*)x, "Couldn't send message to %s:%hu",
				NetIPToString(x->sendTo.ip),
				NetToHostPort(x->sendTo.port));
	}
}

//---------------------------------------------------------------------
// TCP mode functions
//---------------------------------------------------------------------

int CheckConnectionErrors(t_blitz* x, platform_socket** psock, ssize_t size, ssize_t expectedSize)
{
	if(size < expectedSize)
	{
		int lastErr = 0;
		if(size == 0)
		{
			object_post((object*)x, "%s : Peer closed connection", *psock == x->sock ? "receiver" : "listener");
		}
		else if(size == -1)
		{
			lastErr = SocketGetLastError();
			if(lastErr != SOCK_ERR_BADF)
			{
				//NOTE(martin): this is a socket error
				object_error((object*)x,
					     "Socket error on peer %s:%hu : %s\n",
					     NetIPToString(x->recvFrom.ip),
					     NetToHostPort(x->recvFrom.port),
					     SocketGetLastErrorMessage());
			}
		}
		else
		{
			//NOTE(martin): this is a client error
			object_error((object*)x,
				     "Protocol error on peer %s:%hu : short count\n",
				     NetIPToString(x->recvFrom.ip),
				     NetToHostPort(x->recvFrom.port));
		}
		if(lastErr != SOCK_ERR_BADF)
		{
			SocketClose(*psock);
			*psock = 0;
			x->connected = false;
		}
		return(-1);
	}
	else
	{
		return(0);
	}
}

int ReceiveOscFromTCPSocket(t_blitz* x, platform_socket** psock)
{
	const int buffSize = 4096;
	char buffer[buffSize];

	platform_socket* sock = *psock;

	//NOTE(martin): we extract the first int of the stream, which gives us the OSC message length
	char oscSizeBuff[4];
	ssize_t size  = SocketReceive(sock, oscSizeBuff, sizeof(uint32), 0);

	if(CheckConnectionErrors(x, psock, size, sizeof(uint32)) != 0)
	{
		return(-1);
	}

	uint32 oscSize = OscToInt32(oscSizeBuff);
	if(oscSize == 0)
	{
		//NOTE(martin): this means the peer wants us to close the connection on our side
		object_post((object*)x,
			    "Received EOF, closing socket");
		SocketClose(sock);
		*psock = 0;
		return(-1);
	}

	if(oscSize >= buffSize)
	{
		object_error((object*)x,
			     "Protocol error on client %s:%hu : size exceeds receive buffer\n",
			     NetIPToString(x->recvFrom.ip),
			     NetToHostPort(x->recvFrom.port));

		//TODO(martin): handle incorrect type (ie purge the stream ?)
		//		for now we just treat it as an error
		SocketClose(sock);
		*psock = 0;
		return(-1);
	}

	//NOTE(martin): now we receive the osc packet itself
	size = SocketReceive(sock, buffer, oscSize, 0);

	if(CheckConnectionErrors(x, psock, size, oscSize) != 0)
	{
		return(-1);
	}

	osc_element bundle;
	if(OscParsePacket(&bundle, size, buffer) != 0)
	{
		object_error((object*)x, "Can't parse Osc packet");
		SocketClose(sock);
		*psock = 0;
		return(-1);
	}
	OutputOscBundle(x, &bundle);
	return(0);
}

void* TcpReceiver(void* user)
{
	//NOTE(martin): this is the receiver thread used when we initiated the connection

	t_blitz* x = (t_blitz*)user;
	platform_socket* sock = x->sock;

	while(true)
	{
		if(ReceiveOscFromTCPSocket(x, &x->sock) != 0)
		{
			break;
		}
	}
	x->connected = false;
	return(0);
}

void* TcpListener(void* user)
{
	//NOTE(martin): Listener thread used when we are listening to incomming connections

	post("tcp listener start");

	t_blitz* x = (t_blitz*)user;

	while(true)
	{
		x->client = SocketAccept(x->sock, &x->recvFrom);
		if(!x->client)
		{
			if(SocketGetLastError() != SOCK_ERR_ABORT && SocketGetLastError() != SOCK_ERR_BADF)
			{
				object_error((object*)x,
				             "Error in SocketAccept() : %s",
					     SocketGetLastErrorMessage());
			}
			//NOTE(martin): else, this is just us closing the socket from the main thread
			break;
		}
		x->sendTo = x->recvFrom;

		t_atom atoms[2];
		atom_setsym(atoms, gensym(NetIPToString(x->recvFrom.ip)));
		atom_setlong(atoms+1, NetToHostPort(x->recvFrom.port));
		outlet_anything(x->outPeer, gensym("connected"), 2, atoms);

		while(true)
		{
			if(ReceiveOscFromTCPSocket(x, &x->client) != 0)
			{
				break;
			}
		}
		outlet_anything(x->outPeer, gensym("disconnected"), 0, 0);
	}
	return(0);
}

void ConnectTcp(t_blitz* x, net_ip ip, net_port port)
{
	if(x->listener)
	{
		SocketClose(x->sock);
		void* r;
		ThreadJoin(x->listener, &r);
	}

	x->sendTo.ip = ip;
	x->sendTo.port = port;
	x->recvFrom = x->sendTo;

	x->tcpRole = TCP_CONNECT;

	x->sock = SocketOpen(SOCK_TCP);
	if(!x->sock)
	{
		object_error((object*)x, "Can't create socket : %s", SocketGetLastErrorMessage());
		return;
	}
	if(SocketConnect(x->sock, &x->sendTo) != 0)
	{
		object_error((object*)x, "Can't connect socket : %s", SocketGetLastErrorMessage());
		return;
	}
	x->connected = true;
	x->listener = ThreadCreate(TcpReceiver, x);
}

void DisconnectTcp(t_blitz* x)
{
	DEBUG_ASSERT(x->sock);

	SocketClose(x->sock);
	x->sock = 0;
	x->connected = false;
	void* r;
	ThreadJoin(x->listener, &r);
	x->listener = 0;
}

void ListenTcp(t_blitz* x, net_port port)
{
	if(x->listener)
	{
		SocketClose(x->sock);
		if(x->client)
		{
			int eof = 0;
			SocketSend(x->client, &eof, sizeof(int), 0);
		}
		void* r;
		ThreadJoin(x->listener, &r);
	}
	socket_address addr;
	addr.ip = SOCK_IP_ANY;
	addr.port = port;

	x->sock = SocketOpen(SOCK_TCP);

	if(SocketBind(x->sock, &addr) != 0)
	{
		object_error((object*)x, "Can't bind to port %i : %s", port, SocketGetLastErrorMessage());
		SocketClose(x->sock);
		x->sock = 0;
		return;
	}
	if(SocketListen(x->sock, 100) != 0)
	{
		object_error((object*)x, "Failed listening on socket\n");
		SocketClose(x->sock);
		x->sock = 0;
		return;
	}
	x->tcpRole = TCP_LISTEN;
	x->listener = ThreadCreate(TcpListener, x);
}

void OscTcp(t_blitz* x, osc_element* bundle)
{
	platform_socket* sd = 0;
	if(x->tcpRole == TCP_NONE)
	{
		return;
	}
	else if(x->tcpRole == TCP_LISTEN)
	{
		sd = x->client;
	}
	else
	{
		sd = x->sock;
	}

	if(!sd)
	{
		return;
	}
	//TODO(martin): there is a possible race condition here : we could take the value of the socket just before it is
	//		closed and deallocated from the listener thread, and the call SocketSend() on deallocated memory...
	//		SocketSend() AND SocketClose() calls should be synchronized with a lock !!!

	//todo: verify that socket is open, connected etc.
	char sizeBuff[4];
	OscFromInt32(sizeBuff, OscElementSize(bundle));
	if(SocketSend(sd, (void*)sizeBuff, sizeof(int32), 0) != sizeof(int32))
	{
		object_error((object*)x, "Couldn't send message to %s:%hu",
				NetIPToString(x->sendTo.ip),
				NetToHostPort(x->sendTo.port));
		return;
	}
	if(SocketSend(sd, (void*)OscElementData(bundle), OscElementSize(bundle), 0) < 0)
	{
		object_error((object*)x, "Couldn't send message to %s:%hu",
				NetIPToString(x->sendTo.ip),
				NetToHostPort(x->sendTo.port));
	}
}

//---------------------------------------------------------------------
// Max message handling functions
//---------------------------------------------------------------------

void HandleFullPacketMsg(t_blitz *x, t_symbol *msg, int argc, t_atom *argv)
{
	OMAX_UTIL_GET_LEN_AND_PTR

	osc_element bundle;
	OscParsePacket(&bundle, len, ptr);
	if(x->osc)
	{
		x->osc(x, &bundle);
	}
}

void HandleFormatMsg(t_blitz *x, t_symbol *msg, int argc, t_atom *argv)
{
	const int buffSize = 4096;
	char buffer[buffSize];
	osc_element elt;

	if(OscFromMax(&elt, buffSize, buffer, argc, argv) != 0)
	{
		object_error((object*)x, "error parsing OSC message");
		return;
	}

	if(x->osc)
	{
		x->osc(x, &elt);
	}
}

void HandleRefreshServerMsg(t_blitz* x)
{
	if(x->context)
	{
		if(x->connected)
		{
			BlitzDisconnect(x->context);
			x->connected = false;
		}
		BlitzContextDestroy(x->context);
	}
	x->context = BlitzContextCreate();
}

void HandleOpenMsg(t_blitz* x, t_symbol* sym)
{
	if(x->open)
	{
		const char* path = sym->s_name;
		x->open(x, path);
	}
}

void HandleCloseMsg(t_blitz* x)
{
	if(x->close)
	{
		x->close(x);
	}
}

void HandleConnectMsg(t_blitz* x, t_symbol *msg, int argc, t_atom *argv)
{
	if(x->connected)
	{
		//TODO(martin): could disconnect/reconnect instead ?
		error("Already connected");
		return;
	}
	if(x->mode == MODE_TCP || x->mode == MODE_UDP)
	{
		if(argc != 2 || atom_gettype(argv) != A_SYM || atom_gettype(argv+1) != A_LONG)
		{
			object_error((object*)x,
			             "Connect message must have an ip address and a port arguments when in udp or tcp mode");
			return;
		}
		x->connect(x, StringToNetIP(atom_getsym(argv)->s_name), HostToNetPort(atom_getlong(argv+1)));
	}
	else
	{
		object_error((object*)x, "Connect message only recognized in \"udp\" or \"tcp\" modes");
	}
}

void HandleDisconnectMsg(t_blitz* x)
{
	if(!x->connected)
	{
		error("Already disconnected");
		return;
	}
	if(x->mode == MODE_RAW || x->mode == MODE_TCP)
	{
		x->disconnect(x);
	}
	else
	{
		object_error((object*)x, "Disconnect message only recognized in \"tcp\" mode");
	}
}

void HandleListenMsg(t_blitz* x, t_int port)
{
	if(x->listen)
	{
		x->listen(x, HostToNetPort(port));
	}
	else
	{
		object_error((object*)x, "'listen' message is only supported in udp or tcp mode");
	}
}

void HandleReplyMsg(t_blitz *x, t_symbol *msg, int argc, t_atom *argv)
{
	if(x->mode != MODE_RPC_SERVICE)
	{
		object_error((object*)x, "reply message is only recognized in \"rpc_service\" mode");
		return;
	}
	if(argc < 1 || atom_gettype(argv) != A_LONG)
	{
		object_error((object*)x, "First argument to reply message must be a int identifying the client");
		return;
	}
	int clientIndex = atom_getlong(argv);
	if(clientIndex < 0 || clientIndex >= MAX_CLIENTS || !x->clients[clientIndex])
	{
		object_error((object*)x, "Invalid client index");
		return;
	}
	argv++;
	argc--;

	const int buffSize = 4096;
	char buffer[buffSize];
	osc_element elt;

	if(OscFromMax(&elt, buffSize, buffer, argc, argv) != 0)
	{
		object_error((object*)x, "error parsing OSC message");
		return;
	}

	if(!x->rpcService)
	{
		error("Service is not open, can't reply to an rpc call");
		return;
	}

	if(clientIndex < 0 || clientIndex >= MAX_CLIENTS || !x->clients[clientIndex])
	{
		object_error((object*)x, "invalid client index");
		return;
	}
	BlitzRPCReply(x->clients[clientIndex], &elt);
	x->clients[clientIndex] = 0;
}

void HandleNowMsg(t_blitz *x)
{
	outlet_int(x->outTime, BlitzTimestamp(x->context));
}

void HandleStampMsg(t_blitz *x, uint64 stamp)
{
	x->stamp = stamp;
}

//---------------------------------------------------------------------
// Max external ctor/dtor methods
//---------------------------------------------------------------------

void *blitz_new(t_symbol *s, long argc, t_atom *argv)
{
	t_blitz *x = NULL;

	if((x = (t_blitz *)object_alloc(blitz_class)))
	{
		x->context = BlitzContextCreate();
		x->connected = false;

		x->open = 0;
		x->close = 0;
		x->osc = 0;
		x->connect = 0;
		x->disconnect = 0;
		x->listen = 0;

		x->rpcLink = 0;
		x->rpcService = 0;
		x->publisher = 0;
		x->subscriber = 0;
		x->sock = 0;
		x->client = 0;
		x->stamp = 0;

		memset(x->clients, 0, sizeof(rpc_client*) * MAX_CLIENTS);

		if(argc)
		{
			if(atom_gettype(argv) != A_SYM)
			{
				error("First argument must be a symbol");
				object_free(x);
				return(0);
			}
			const char* role = atom_getsym(argv)->s_name;
			if(!strcmp(role, "rpc_link"))
			{
				x->mode = MODE_RPC_LINK;
				x->open = OpenRPCLink;
				x->close = CloseRPCLink;
				x->osc = OscRPCLink;
			}
			else if(!strcmp(role, "rpc_service"))
			{
				x->mode = MODE_RPC_SERVICE;
				x->open = OpenRPCService;
				x->close = CloseRPCService;

				x->outPeer = outlet_new(x, NULL);
			}
			else if(!strcmp(role, "publisher"))
			{
				x->mode = MODE_PUBLISHER;
				x->open = OpenPublisher;
				x->close = ClosePublisher;
				x->osc = OscPublisher;
			}
			else if(!strcmp(role, "subscriber"))
			{
				x->mode = MODE_SUBSCRIBER;
				x->open = OpenSubscriber;
				x->close = CloseSubscriber;
			}
			else if(!strcmp(role, "udp"))
			{
				x->mode = MODE_UDP;

				x->sock = SocketOpen(SOCK_UDP);
				if(!x->sock)
				{
					object_error((object*)x,
					             "Can't create socket : %s",
						     SocketGetLastErrorMessage());
				}
				else
				{
					x->sendTo.ip = SOCK_IP_ANY;
					x->sendTo.port = SOCK_PORT_ANY;
					x->recvFrom.ip = SOCK_IP_ANY;
					x->recvFrom.port = SOCK_PORT_ANY;
					x->osc = OscUdp;
					x->connect = ConnectUdp;
					x->listen = ListenUdp;

					if(SocketBind(x->sock, &x->recvFrom) != 0)
					{
						object_error((object*)x, "Can't bind to port %i : %s",
								x->recvFrom.port, SocketGetLastErrorMessage());
						SocketClose(x->sock);
						x->sock = 0;
					}
					else
					{
						x->listener = ThreadCreate(UdpListener, x);
					}
				}
				x->outPeer = outlet_new(x, NULL);
			}
			else if(!strcmp(role, "tcp"))
			{
				x->mode = MODE_TCP;
				x->outPeer = outlet_new(x, NULL);
				x->osc = OscTcp;
				x->connect = ConnectTcp;
				x->disconnect = DisconnectTcp;
				x->listen = ListenTcp;
			}
			else
			{
				error("Unrecognized first argument");
				object_free(x);
				return(0);
			}
		}
		else
		{
			error("Unrecognized first argument");
			object_free(x);
			return(0);
		}
		x->outTime = outlet_new(x, NULL);
		x->out = outlet_new(x, NULL);
	}
	return (x);
}

void blitz_free(t_blitz *x)
{
	//TODO(martin): switch mode, close associated resources...
}


//---------------------------------------------------------------------
// module's main()
//---------------------------------------------------------------------

int main(void)
{
	t_class *c;

	c = class_new("blitz", (method)blitz_new, (method)blitz_free, (long)sizeof(t_blitz), 0L, A_GIMME, 0);

	class_addmethod(c, (method)HandleFullPacketMsg, "FullPacket", A_GIMME, 0);
	class_addmethod(c, (method)HandleFormatMsg, "osc", A_GIMME, 0);
	class_addmethod(c, (method)HandleRefreshServerMsg, "refresh_server", 0);
	class_addmethod(c, (method)HandleConnectMsg, "connect", A_GIMME, 0);
	class_addmethod(c, (method)HandleListenMsg, "listen", A_LONG, 0);
	class_addmethod(c, (method)HandleDisconnectMsg, "disconnect", 0);
	class_addmethod(c, (method)HandleOpenMsg, "open", A_DEFSYM, 0);
	class_addmethod(c, (method)HandleCloseMsg, "close", 0);
	class_addmethod(c, (method)HandleReplyMsg, "reply", A_GIMME, 0);
	class_addmethod(c, (method)HandleNowMsg, "now", 0);
	class_addmethod(c, (method)HandleStampMsg, "stamp", A_DEFLONG, 0);

	class_register(CLASS_BOX, c);
	blitz_class = c;
	return(0);
}
